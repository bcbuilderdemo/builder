//import { ProjectAndBuilderInfo } from './project-and-builder-info';

export interface GetBidsBySubproject {

    id:                     string;
    additionalDetails:      string;
    builderID:              string;
    projectID:              string;
    amount:                 number;
    subprojectID:           string;
    comments?:              string;
    is_bid_out:             string;

    //subcontractorInfo:      ProjectAndBuilderInfo;

    //API link
    //{apiURL}/api/v1/{subproject_id}/bids
}
