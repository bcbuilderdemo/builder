export interface GetAllBidsByProject{
  id:             string;
  projectID:      string;
  is_bid_out:     number;
  amount:         string;
  is_filled?:     string;
  comments?:      string;
  descriptions?:  string;
  is_available:   string;
  stepNum:        string;
  stepName:       string;
  status:         string;
}
