

export interface AccountingInterface {

  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;

  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;

  totalBalanceOwedForAllProjects?:            number;
  totalBalancePaidForAllProjects?:            number;
  totalBalanceRemainingForAllProjects?:       number;
  totalProjectsBalanceIsPaidInFull?:          number;
  totalProjectsBalanceIsOutstanding?:         number;

  data:                 Array<AccountingDataInterface>;
}

export interface AccountingOutPaginatorInterface {

  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;

  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;
  data:                 Array<AccountingOutInterface>;

}

export interface AccountingDataInterface {

  id:                       number;
  projectID:                number;
  scID:                     number;
  builderID:                number;
  chatUUID:                 string;

  projectAddress:           string;
  projectAddress2:          string;
  projectCity:              string;
  projectPostalCode:        string;
  projectProvince:          string;
  projectBuildValue:        string;
  projectBuildingSize:      string;
  projectCountry:           string;
  projectCreated_at:        string;
  projectDescription:       string;
  projectDetails:           string;
  projectLandType:          string;
  projectFirstName:         string;
  projectLastName:          string;
  projectLotSize:           string;
  projectProjectType:       string;
  projectUpdated_at:        string;
  projectZoning:            string;
  projectPhoneNumber:       string;
  projectPhoneNumber2:      string;
  projectNumUnits:          string;

  sci_company:              string;
  sci_address:              string;
  sci_address2:             string;
  sci_phoneNumber:          string;
  sci_postalCode:           string;
  sci_city:                 string;
  sci_province:             string;
  sci_companySlug:          string;

  bci_company:              string;
  bci_address:              string;
  bci_address2:             string;
  bci_phoneNumber:          string;
  bci_postalCode:           string;
  bci_city:                 string;
  bci_province:             string;
  bci_companySlug:          string;

  totalAmount:              string;
  accountingID:             string;
  accountingDeleted:        number;
  amountPaidSoFar:          string;
  amountRemaining:          string;
  lastPaymentMadeOn:        string;
  accounting_created_at:    string;

  totalBalanceOwedForProject:                number;
  totalBalancePaidForProject:                number;
  totalBalanceRemainingForProject:           number;
  totalBalanceOwedForAllProjects:            number;
  projectBalanceIsOutstanding:               number;

/*  apmAmountPaid:            string;
  paymentMadeOn:            string;
  apmID:                    number;*/

}

export interface AccountingOutInterface {

  projectAddress:           string;
  projectAddress2:          string;
  projectCity:              string;
  projectPostalCode:        string;
  projectProvince:          string;
  companyName:              string;
  companySlug:              string;
  chatUUID:                 string;


  totalAmount:              string;
  accountingID:             string;
  accountingDeleted:        number;
  amountPaidSoFar:          string;
  amountRemaining:          string;

}



