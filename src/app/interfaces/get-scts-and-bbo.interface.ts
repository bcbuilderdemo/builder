export interface GetSctsAndBboInterface {


  id:               number;
  tradeName:        string;
  description:      string;
  image:            URL;
  company?:         string;
  phoneNumber?:     string;
  scUserID?:        number;
  scID?:            number;
  checked:          any;
  bidSentToSC:      number;

}
