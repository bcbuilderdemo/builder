import {UserAndCompanyInfo} from './user-and-company-info';
//import { SubprojectsDataInterface } from './subprojects.interface';


export interface GetUserInfoAndProjects {

  projectInfo:      GetProjectsInterface;
  userInfo:         UserAndCompanyInfo;
}

export interface GetProjectsInterface {

  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;
  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;
  data:                 Array<ProjectDataInterface>;  
}

export interface  ProjectDataInterface {
  id:             string;
  address:        string;
  address2:       string;
  buildValue:     string;
  buildingSize:   string;
  city:           string;
  country:        string;
  created_at:     string;
  description:    string;
  details:        string;
  landType:       string;
  firstName:      string;
  lastName:       string;
  lotSize:        string;
  postalCode:     string;
  projectType:    string;
  province:       string;
  updated_at:     string;
  userID:         string;
  zoning:         string;
  phoneNumber:    string;
  phoneNumber2:   string;
  numUnits:       string;
  isOther:        boolean;
  
  bidID?:           number;
  bboID?:           number;
  accountingID?:    number;

  bidAdditionalDetails?: string;
}
