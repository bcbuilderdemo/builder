export interface GetProfileInfoInterface {
  profileInfo:        ProfileInfoInterface;
  profileUploads:     ProfileUploadsInterface;
}

export interface ProfileInfoInterface {

  uuid?:                    string;
  company:                  string;
  companySlug?:             string;
  contactFirstName:         string;
  contactLastName:          string;
  address:                  string;
  address2:                 string;
  postalCode:               string;
  province:                 string;
  city:                     string;
  country:                  string;
  email:                    string;
  phoneNumber:              string;
  phoneNumber2:             string;
  aboutUs?:                 string;
  youtubeURL?:              string;
  twitterURL?:              string;
  facebookURL?:             string;
  website?:                 string;
  instagramURL?:            string;
  googlePlusURL?:           string;
  pinterestURL?:            string;
  user_registered_on?:      string;
  userID?:                  string;
  profileID?:               string;
  id?:                      number;
}

export interface ProfileUploadsInterface {

  fileID?:                string;
  fileName:               string;
  fileType:               string;
  mimeType:               string;
  filePath:               string;
  fileURL:                string;
  id:                     string;
  bidID:                  number;
  updated_at:             string;
  created_at:             string;
  profileID:              string;
  userID:                 string;
  isLogo:                 string;
  iconURL:                string;
  fileUUID?:              string;

}

