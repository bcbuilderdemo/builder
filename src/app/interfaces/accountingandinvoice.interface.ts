import {AccountingDataInterface} from './accounting-data.interface';
import {InvoiceInterface} from './invoice.interface';
import {SCBOAndSCBOLIInterfaceForInvoice} from './sc-bid-form';

export interface AccountingAndInvoiceDataInterface {

  accounting:       AccountingDataInterface;
  apm:              InvoiceInterface;
  scbo:             SCBOAndSCBOLIInterfaceForInvoice;
}
