export interface SubcontractorPaginatorInterface {

  page:               number;
  total:              number;
  data:               Array<SubcontractorInterface>;
  prev_page_url:      string;
  next_page_url:      string;

  tradeName?:         string;
  description?:       string;
  aboutUs?:           string;
}

export interface SubcontractorInterface {

  id:                       number;

  company?:                 string;
  phoneNumber?:             string;
  scUserID?:                number;
  scID?:                    number;
  checked:                  any;
  sentToSC:                 number;
  bbo_created_at:           string;
  bboSCResponded?:          number;
  bbo_accepted?:            number;
  bidAcceptedSCBOID?:       number;
  bboID?:                   number;
  scboID?:                  number;
  scboCreatedAt:            string;
  scboAccountingID?:        number;
  projectID?:               number;
  scboBuilderAcceptedAt?:   string;
  bidID?:                   number;
  city?:                    string;
  province?:                string;
  address?:                 string;
  address2?:                string;
  postalCode?:              string;
  phoneNumber2?:            string;
  companySlug:              string;
  logoURL?:                 string;

  chatUUID?:                string;

  /* Subcontractor trades */

  tradeName:                string;
  description:              string;
  imageURL:                 string;
  imageType:                string;
  sctNameSlug:              string;

  website?:                 string;
  aboutUs?:                 string;
  bidExists:                boolean;
}
