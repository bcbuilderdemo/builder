export interface NewBidInterface {
  additionalDetails:  string;
  files:              Array<File>;
}
