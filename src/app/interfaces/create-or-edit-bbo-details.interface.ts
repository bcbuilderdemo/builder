import {SubcontractorInterface} from './subcontractor.interface';
import {BuilderInfoAliasInterface, ProjectInfoAliasInterface} from './combinedAliasNames.interface';

export interface CreateOrEditOrViewBboDetailsInterface {

  projectInfo:          ProjectInfoAliasInterface & BuilderInfoAliasInterface;
  sctInfo:              SubcontractorInterface;
}

