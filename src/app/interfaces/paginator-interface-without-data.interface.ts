export interface PaginatorInterfaceWithoutDataInterface {

  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;

  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;
}
