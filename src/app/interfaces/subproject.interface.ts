export interface SubprojectDataInterface {

  id:             string;
  project_id:     string;
  hbg_id:         string;
  is_filled:      number;
  comments?:       string;
  description?:    string;
  is_available:   number;

}
