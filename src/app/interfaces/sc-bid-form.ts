export interface ScBidFormInterface {

  subTotal:                     any;
  taxTotal:                     any;
  grandTotal:                   any;
  comments:                     string;
  lineData:                     LineDataInterface[];
  scbo_created_at?:             string;

  address?:                     string;
  address2?:                    string;
  company?:                     string;
  postalCode?:                  string;
  city?:                        string;
  province?:                    string;
  phoneNumber?:                 string;
  scboID?:                      any;

  builder_viewed_scbo?:         number;
  builder_viewed_at_scbo?:      string;

  sc_responded_bbo?:            number;
  sc_viewed_bbo?:               number;
  sc_viewed_bbo_at?:            string;

  projectID?:                   number;
  bidID?:                       number;

sc_read?:                       number;
sc_read_at?:                    string;
}


export interface LineDataInterface {
  index?:               number;
  name:                 string;
  description:          string;
  quantity:             number;
  uom:                  string;
  tax:                  string;
  taxRate?:             string;
  price:                number;
  lineTaxTotal:         number;
  lineSubTotal:         number;
  lineTotal:            number;
  deleted:              number;
  builder_accepted_at?: string;
  builderAccepted?:     number;
  builder_rejected_at?: number;
}

export interface SCBOAndSCBOLIInterfaceForInvoice {

  scbo_subTotal:                any;
  scbo_taxTotal:                any;
  scbo_grandTotal:              any;
  scbo_comments:                string;
  scbo_created_at?:             string;

  scbo_address?:               string;
  scbo_address2?:              string;
  scbo_company?:               string;
  scbo_postalCode?:            string;
  scbo_city?:                  string;
  scbo_province?:              string;
  scbo_phoneNumber?:           string;
  scbo_scboID?:                any;
  scboli_index?:               number;
  scboli_name:                 string;
  scboli_description:          string;
  scboli_quantity:             number;
  scboli_uom:                  string;
  scboli_tax:                  string;
  scboli_price:                number;
  scboli_lineTaxTotal:         number;
  scboli_lineSubTotal:         number;
  scboli_lineTotal:            number;
  scboli_deleted:              number;
  scboli_builder_accepted_at?: string;
  scboli_builderAccepted?:     number;
  scboli_builder_rejected_at?: number;
}


