import {ProjectDataInterface} from './get-project.interface';
import {UserAndCompanyInfo} from './user-and-company-info';

export interface ProjectAndBuilderInfo {

  projectInfo:      ProjectDataInterface;
  builderInfo:      UserAndCompanyInfo;
}
