export interface NewProjectInterface {
  firstName:            string;
  lastName:             string;
  address:              string;
  address2:             string;
  postalCode:           string;
  province:             string;
  country:              string;
  description:          string;
  landType:             string;
  projectType:          string;
  lotSize:              string;
  buildingSize:         string;
  zoning:               string;
  buildValue:           string;
  details:              string;
  buildingType:         string;
  uploadPdf:            string;
}
