import {BidsInInterface} from './get-bids-by-trade.interface';

export interface GetScBidsPaginatorInterface {
  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;

  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;

  data:                 Array<BidsInInterface>;
}
