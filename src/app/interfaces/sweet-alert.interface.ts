export interface  SweetAlertTypes {
  confirm:        String;
  prompt:         String;
  alert:          String;
  question:       String;
  success:        String;
  warning:        String;
  error:          String;
  info:           String;
}
