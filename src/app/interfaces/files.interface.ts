export interface FilesInterface {

  fileID:       string;
  fileName:     string;
  fileType:     string;
  mimeType:     string;
  filePath:     string;
  iconURL:      string;
  id:           string;
  bidID:        number;
  uploadsID:    number;
  deleted:      number;
  projectID?:   number;
  updated_at:   string;
  created_at:   string;

}
