import {FilesInterface} from './files.interface';
import {SubcontractorInterface} from './subcontractor.interface';

export interface GetBBOResolverData {

  bbo:             BBOAndProjectData;
  files?:          FilesInterface;
  sctInfo?:        SubcontractorInterface;
}

export interface BBOAndProjectData {
  id:                       number;
  projectID:                number;
  scID:                     number;
  builderID:                number;
  bboID:                    number;
  scTradeID:                number;
  accountingID:             number;

  bid_additionalDetails:    string;

  bbo_accepted:             number;

  projectAddress:           string;
  projectAddress2:          string;
  projectCity:              string;
  projectPostalCode:        string;
  projectProvince:          string;
  projectBuildValue:        string;
  projectBuildingSize:      string;
  projectCountry:           string;
  projectCreated_at:        string;
  projectDescription:       string;
  projectDetails:           string;
  projectLandType:          string;
  projectFirstName:         string;
  projectLastName:          string;
  projectLotSize:           string;
  projectProjectType:       string;
  projectUpdated_at:        string;
  projectZoning:            string;
  projectPhoneNumber:       string;
  projectPhoneNumber2:      string;
  projectNumUnits:          string;

  scbo_builderRejectedAt:   string;
  scbo_builderAcceptedAt:   string;
  scbo_builderAccepted:     number;
  scbo_builderRejected:     number;

  sci_company:              string;
  sci_address:              string;
  sci_address2:             string;
  sci_phoneNumber:          string;
  sci_postalCode:           string;
  sci_city:                 string;
  sci_province:             string;

  bci_company:              string;
  bci_address:              string;
  bci_address2:             string;
  bci_phoneNumber:          string;
  bci_postalCode:           string;
  bci_city:                 string;
  bci_province:             string;

  bbo_sc_viewed:            number;
  bbo_sc_viewed_at:         string;
}

