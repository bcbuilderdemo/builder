import { Injectable } from '@angular/core';
import { isDevMode } from '@angular/core';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { MasksInterface } from '../interfaces/masks.interface';

@Injectable()
export class GlobalConstants {
  readonly APIURL: string;
  readonly STORAGEURL: string;
  readonly FEURL: string;
  isLocal: boolean;
  masks: MasksInterface;
  regexPatterns: any;

  constructor() {

    // NOTE: IF YOU HAVE PRODUCTION ENVIRONMENT PROBLEMS IT MAY BE isDevMode()
    //changed this to allow access to neil brars site in local
    //zentomic
    //if (this.getMode() === true) {

    if (this.getMode() !== true) {
      console.log("local");
      this.APIURL = 'http://bcBuilder.test/api/v1';
      this.STORAGEURL = 'https://bcBuilder.test/storage';
      this.FEURL = "https://localhost:4200";
    }
    else {
      console.log("production");
      // this.APIURL     = 'https://neilbrar.com/bcbAPI/api/v1';
      // this.STORAGEURL = 'https://neilbrar.com/bcbAPI/public/storage';
      // this.FEURL        = "https://neilbrar.com/bcb";
      // this.APIURL     = 'https://api.bcbuilder.ca/api/v1';
      // this.STORAGEURL = 'https://api.bcbuilder.ca/public/storage';
      // this.FEURL        = "http://bcbuilder.ca";
      this.APIURL = 'http://localhost:8000/api/v1';
      this.STORAGEURL = 'http://api.bcbuilder.ca/public/storage';
      this.FEURL = "http://localhost:8000";
    }

    this.initMasks();
    this.initRegex();
  }

  private getMode() {
    this.isLocal = isDevMode();
    console.log("constants mode isLocal", this.isLocal);
    return this.isLocal;
  }

  private initMasks() {
    const buildValueMask = createNumberMask({
      prefix: '$',
      allowDecimal: true,
    });

    const sizeMask = createNumberMask({
      prefix: '',
    });

    this.masks = {
      buildValue: buildValueMask,
      sizeMask: sizeMask,
      phoneNumber: [/[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
      postalCode: [/[A-Za-z]/, /\d/, /[A-Za-z]/, ' ', /\d/, /[A-Za-z]/, /\d/]
    };
  }

  private initRegex() {

    const postalCode = /^[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]/;
    //const phoneNumber = "/[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/";
    const phoneNumber = /^[1-9]\d\d \d\d\d \d\d\d\d/;
    const URL = "((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[A-Za-z0-9.-]+)((?:\\/[\\+~%\\/.\\w-_]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)";


    this.regexPatterns = {
      postalCode: postalCode,
      phoneNumber: phoneNumber,
      URL: URL
    };
  }
}
