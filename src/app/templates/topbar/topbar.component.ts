import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../interfaces/profile-info.interface';
import { ProfileResolverService } from '../../services/resolvers/profile-resolver/profile-resolver.service';
import { GlobalConstants } from '../../constants/global.constants';

export class DataService {
  public itemAdded$: EventEmitter<boolean>;

  constructor() {
    this.itemAdded$ = new EventEmitter();
  }

  public putInfo(): void {
    this.itemAdded$.emit();
  }
}

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css'],
})
export class topbarComponent implements OnInit {

  public profileInfo: ProfileInfoInterface = null;
  public app: any;
  public headerThemes: any;
  public changeHeader: any;
  public sidenavThemes: any;
  public changeSidenav: any;
  public headerSelected: any;
  public sidenavSelected: any;
  profileStorageURL: string;

  @Output()
  change: EventEmitter<number> = new EventEmitter<number>();

  isLoggedIn;
  showlineNav = true;
  constructor
    (
    private profileResolver: ProfileResolverService,
    private route: ActivatedRoute,
    private auth: AuthService,
    private router: Router,
    private constants: GlobalConstants,
    private dataservice: DataService
    ) {
    this.app = {
      layout: {
        sidePanelOpen: false,
        isMenuCollapsed: false,
        themeConfigOpen: false,
        rtlActived: false
      }
    };

    this.headerThemes = ['header-default', 'header-primary', 'header-info', 'header-success', 'header-danger', 'header-dark'];
    this.changeHeader = changeHeader;

    function changeHeader(headerTheme) {
      this.headerSelected = headerTheme;
    }

    this.sidenavThemes = ['sidenav-default', 'side-nav-dark'];
    this.changeSidenav = changeSidenav;

    function changeSidenav(sidenavTheme) {
      this.sidenavSelected = sidenavTheme;
    }
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;
  }

  sidemenushow() {
    this.app.layout.isMenuCollapsed = !this.app.layout.isMenuCollapsed;
    this.dataservice.putInfo();
  }

  ngOnInit() {
    this.profileResolver
      .resolve(this.route.snapshot)
      .subscribe(value => {
        this.profileInfo = value.profileInfo;
      });

    this.auth.authenticationNotifier().subscribe(authd => {
      this.isLoggedIn = authd;
    });
  }

  onLogout() {
    this.auth.logout();
  }
}
