import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NewProjectComponent } from '../../pages/project-pages/new-project/new-project.component';
import { DataService } from '../topbar/topbar.component';
import { GetProjectsInterface, ProjectDataInterface } from '../../interfaces/get-project.interface';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../interfaces/profile-info.interface';
import { ProfileResolverService } from '../../services/resolvers/profile-resolver/profile-resolver.service';
import { GetDashboardDataResolverServiceService } from '../../services/resolvers/get-dashboard-resolver-service/get-dashboard-data-resolver-service.service';
import { GlobalConstants } from '../../constants/global.constants';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {

  showStyle: false;
  bsModalRef: BsModalRef;
  public activateClass: any;
  public profileInfo: ProfileInfoInterface = null;
  public projects: GetProjectsInterface;
  public app: any;
  public headerThemes: any;
  public changeHeader: any;
  public sidenavThemes: any;
  public changeSidenav: any;
  public headerSelected: any;
  public sidenavSelected: any;
  profileStorageURL: string;

  constructor(
    private dataresolverService: GetDashboardDataResolverServiceService,
    private profileResolver: ProfileResolverService,
    private route: ActivatedRoute,
    private auth: AuthService,
    private modalService: BsModalService,
    private constants: GlobalConstants,
    private dataService: DataService) {
    this.app = {
      layout: {
        sidePanelOpen: true,
        isMenuCollapsed: false,
        themeConfigOpen: false,
        rtlActived: false
      }
    };
    this.headerThemes = ['header-default', 'header-primary', 'header-info', 'header-success', 'header-danger', 'header-dark'];
    this.changeHeader = changeHeader;

    function changeHeader(headerTheme) {
      this.headerSelected = headerTheme;
    }

    this.sidenavThemes = ['sidenav-default', 'side-nav-dark'];
    this.changeSidenav = changeSidenav;

    function changeSidenav(sidenavTheme) {
      this.sidenavSelected = sidenavTheme;
    }

    dataService.itemAdded$.subscribe(() => this.onGetSidebarInfo());
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;

    
  }

  private onGetSidebarInfo(): void {
    // do something with added item
    this.app.layout.isMenuCollapsed = !this.app.layout.isMenuCollapsed;
  }

  setActivateClass(attribute) {
    this.activateClass = attribute;
  }

  ngOnInit() {

    this.dataresolverService
      .resolve(this.route.snapshot)
      .subscribe(value => {
        this.projects = value.projectInfo;
      });

    this.profileResolver
      .resolve(this.route.snapshot)
      .subscribe(value => {
        this.profileInfo = value.profileInfo;
      });

    if (this.auth.hasPermission('builder')) {
      if (!this.auth.checkIfUserHasCreatedAProject()) {
      this.bsModalRef = this.modalService.show(NewProjectComponent, { class: 'modal-dialog modal-md' });
      }
    }
    
    this.activateClass = "dashboard";
  }
  openNewProjectModal() {
    this.bsModalRef = this.modalService.show(NewProjectComponent, { class: 'modal-dialog modal-md' });
    // this.NewProjectRef.showModal();
  }
}
