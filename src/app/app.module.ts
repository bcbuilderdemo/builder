import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './templates/navbar/navbar.component';
import { SidebarComponent } from './templates/Sidebar/sidebar.component';
import { topbarComponent } from './templates/topbar/topbar.component';
import { DataService } from './templates/topbar/topbar.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { NewProjectComponent } from './pages/project-pages/new-project/new-project.component';
import { ProjectDetailsComponent } from './pages/project-pages/project-details/project-details.component';
import { ContractorsComponent } from './pages/services/contractors/contractors.component';
import { ViewComponent } from './pages/services/all-subcontractors-by-trade/view.component';
import { RegisterComponent } from './pages/signup-login/register/register.component';
import { LoginComponent } from './pages/signup-login/login/login.component';
import { InvoiceSendComponent } from './pages/bids/invoice/invoice-send.component';
import { RequestBidComponent } from './pages/bids/request-bid/request-bid.component';
import {
  AccordionModule, AlertModule, CarouselModule, ModalModule, PaginationModule,
  TooltipModule
} from 'ngx-bootstrap';



import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BackendService } from './services/backend.service';
import { AlertsComponent } from './templates/alerts/alerts.component';
import { AlertService } from './services/alerts/alert.service';
import { ModalComponent } from './templates/modal/modal.component';
import { Ng2Webstorage } from 'ngx-webstorage';
import { AuthService } from './services/auth/auth.service';
import { AuthModule } from './services/auth/auth.module';
import { ForgotPasswordComponent } from './pages/password/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/password/reset-password/reset-password.component';
// import { NgxBootstrapProductTourModule } from 'ngx-bootstrap-product-tour';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { TextMaskModule } from 'angular2-text-mask';
import { GlobalConstants } from './constants/global.constants';
import { ViewBidsComponent } from './pages/bids/view-bids/view-bids.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ProjectRouteGuard } from './services/route-guard/route-guard.service';
import { ProjectResolverService } from './services/resolvers/project-resolver/project-resolver.service';
import { NewOrCreateBidResolverService } from './services/resolvers/new-or-create-bid/new-or-create-bid.service';
import { GetSCByTradeResolver } from './services/resolvers/get-scby-trade/get-sc-by-trade.resolver.service';
import { APP_BASE_HREF, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { GetSctsOrProjectsOrBidTypeResolverService } from './services/resolvers/get-scts-or-projects/get-scts-or-projects-resolver.service';
import { GetBidsResolverService } from './services/resolvers/get-bids/get-bids-resolver.service';
import { GetBidsForSubprojectFromSubcontractorResolverService } from './services/resolvers/get-bids-for-subproject-from-subcontractor-resolver/get-bids-for-subproject-from-subcontractor-resolver.service';
import { GetPermissionsService } from './services/resolvers/get-permissions/get-permissions.service';
import { NgxPermissionsModule } from 'ngx-permissions';
import { GetScboDataService } from './services/resolvers/get-scbo-data/get-scbo-data.service';
import { CanViewOrManageBidGuard } from './services/guards/can-view-or-manage-bid/can-view-or-manage-bid.guard.guard';
import { MomentModule } from 'angular2-moment';
import { ViewUploadsComponent } from './pages/uploads/view-uploads/view-uploads.component';
import { GetUploadsForProjectService } from './services/resolvers/get-uploads-for-project/get-uploads-for-project.service';
import { ChangelogComponent } from './pages/temporary/changelog/changelog.component';
import { GetFilesForProjectOrBidService } from './services/resolvers/get-files-for-project-or-bid/get-files-for-project-or-bid.service';
import { GetBidForProjectForScResolverService } from './services/resolvers/get-bid-for-project-for-sc-resolver/get-bid-for-project-for-sc-resolver.service';
import { GetAccountingDataResolverService } from './services/resolvers/get-accounting-data-resolver/get-accounting-data-resolver.service';
import { GetInvoiceDataResolverService } from './services/resolvers/get-invoice-data-resolver/get-invoice-data-resolver.service';
import { ViewInvoiceComponent } from './pages/bids/view-invoice/view-invoice.component';
import { GetBuilderBidOutDataResolverService } from './services/resolvers/get-builder-bid-out-data-resolver/get-builder-bid-out-data-resolver.service';
import { GetCreateOrEditBbodetailsResolverService } from './services/resolvers/get-create-or-edit-bbodetails-resolver/get-create-or-edit-bbodetails-resolver.service';
import { GetBidsLineItemsResolverService } from './services/resolvers/get-bids-line-items/get-bids-line-items-resolver.service'
import { CheckIfBboExistsResolverService } from './services/resolvers/check-if-bbo-exists/check-if-bbo-exists-resolver.service';
import { ProfileComponent } from './pages/profile/profile/profile.component';
import { ProfileResolverService } from './services/resolvers/profile-resolver/profile-resolver.service';
import { ViewProfileComponent } from './pages/profile/view-profile/view-profile.component';
import { ViewProfilePhotoComponent } from './pages/profile/view-profile/photo.component';
import { ChatResolverService } from './services/resolvers/chat-resolver/chat-resolver.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { Autosize } from 'ng-autosize';
import { GetDashboardDataResolverServiceService } from './services/resolvers/get-dashboard-resolver-service/get-dashboard-data-resolver-service.service';
import { GetAllNotificationsResolverService } from './services/resolvers/get-all-notifications/get-all-notifications-resolver.service';
import { ViewAllMessagesResolverService } from './services/resolvers/view-all-messages-resolver/view-all-messages-resolver.service';
import { KeyObjectArrayPipePipe } from './pipes/key-object-array-pipe/key-object-array.pipe.pipe';
import { GetAllSubcontractorsResolverService } from './services/resolvers/get-all-scsresolver/get-all-subcontractors-resolver.service';
import { GetHomeBuildersGuideResolverService } from './services/resolvers/get-hbgresolver/get-home-builders-guide-resolver.service';
import { LineNavbarComponent } from './templates/line-navbar/line-navbar.component';
import { SocialProfileComponent } from './pages/social/social.component';
import { SocialProfilePhotoComponent } from './pages/social/photo.component';
import { SubprojectResolverService } from './services/resolvers/subproject-resolver/subproject-resolver.service';
import { BidsBySubprojectResolverService } from './services/resolvers/get-bids-by-subproject-resolver/get-bids-by-subproject-resolver.service';
import { GetAllBidsByProjectResolverService } from './services/resolvers/get-all-bids-by-project/get-all-bids-by-project-resolver.service';
import { ProjectPostComponent } from './pages/services/subproject-activation/project-post.component';
import { NouisliderModule } from 'ng2-nouislider/src/nouislider';

//Layout Modules
import { CommonLayoutComponent } from './common/common-layout.component';
import { AuthenticationLayoutComponent } from './common/authentication-layout.component';

//Directives
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Sidebar_Directives } from './shared/directives/side-nav.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NguiMapModule } from '@ngui/map';
import { GoogleMapComponent } from './pages/maps/google-map/google-map.component';

export function authServiceFactory(auth: AuthService): Function {
  return () => auth.onStartupRefresh();
}
const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    // zs-dashboard-route
    path: 'dashboard',
    component: HomePageComponent,
    resolve: {
      permissions: GetPermissionsService,
      data: GetDashboardDataResolverServiceService,
      nots: GetAllNotificationsResolverService,
      profile: ProfileResolverService,
      SCOrProjectData: GetSctsOrProjectsOrBidTypeResolverService,
      bidData: GetBidsResolverService,
    }
  },
  {
    // zs-dashboard-route
    path: 'maps/google-map',
    component: GoogleMapComponent,
    resolve: {
      permissions: GetPermissionsService,
      data: GetDashboardDataResolverServiceService,
      nots: GetAllNotificationsResolverService,
      profile: ProfileResolverService,
    }
  },
  {
    path: 'changelog',
    component: ChangelogComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    // zs-profile-route
    path: 'profile',
    component: SocialProfileComponent,
    resolve: { data: ProfileResolverService }
  },
  {
    // zs-profile-uploads-route
    path: 'profile-photos',
    component: SocialProfilePhotoComponent,
    resolve: { data: ProfileResolverService }
  },
  {
    // zs-edit-profile-route
    path: 'editprofile',
    component: ProfileComponent,
    resolve: { data: ProfileResolverService },
  },

  {
    path: 'profile/:companySlug',
    component: ViewProfileComponent,
    resolve: { data: ProfileResolverService },
  },
  {
    path: 'profile/photo/:companySlug',
    component: ViewProfilePhotoComponent,
    resolve: { data: ProfileResolverService },
  },
  {
    path: 'projects/:id',
    canActivate: [ProjectRouteGuard],
    children:
      [
        {
          path: 'new-project',
          component: NewProjectComponent,
        },
        {
          // zs-project-details-route
          path: 'project-details',
          component: ProjectDetailsComponent,
          resolve: {
            projectData: ProjectResolverService,
            nots: GetAllNotificationsResolverService
          }
        },
        /***********************************************
                        SUBCONTRACTORS
         ************************************************/
        {
          // zs-subcontractors-route
          path: 'subcontractors',
          component: ContractorsComponent,
          resolve: {
            scs: GetAllSubcontractorsResolverService,
            hbg: GetHomeBuildersGuideResolverService,
            subprojects: SubprojectResolverService,
          }
        },
        {
          // zs-home-builders-guide-route
          path: 'subcontractors/view/:scTradeID/:hbg',
          component: ViewComponent,
          resolve: {
            data: GetSCByTradeResolver,
            subprojectService: SubprojectResolverService,
          }
        },
        {
          // zs-request-bid-route
          path: 'subcontractors/view/:scTradeID/:hbg/:subprojectID/activeproject',
          component: ProjectPostComponent,
          resolve: {
            bidInfo: GetBidsLineItemsResolverService,
            profile: ProfileResolverService,
            projectData: ProjectResolverService,
            profileResolver: ProfileResolverService,
            subprojectService: SubprojectResolverService,
          },
        },
        {
          // zs-bid-request-route
          path: 'subcontractors/view/:scTradeID/:hbg/:subcontractorID/:subprojectID/bidrequest',
          component: InvoiceSendComponent,
          resolve: {
            bidInfo: GetBidsForSubprojectFromSubcontractorResolverService,
            profile: ProfileResolverService,
            projectData: ProjectResolverService,
            profileResolver: ProfileResolverService,
            allSubprojects: SubprojectResolverService
          },
        },

        /***********************************************
                          BIDS
         ************************************************/

        {
          // zs-bid-route
          path: 'bids',
          component: ViewBidsComponent,
          resolve: {
            getAllBids: GetAllBidsByProjectResolverService,
          },
        },
        {
          path: 'bids/:subprojectID/:bidID/invoice',
          component: ViewInvoiceComponent,
          resolve: {
            bidInfo: GetBidsLineItemsResolverService,
            profile: ProfileResolverService,
            projectData: ProjectResolverService,
          }
        },

        /***********************************************
                              UPLOADS
         ************************************************/
        {
          path: 'uploads',
          component: ViewUploadsComponent,
          resolve: {
            projectFiles: GetFilesForProjectOrBidService
          },
        },
        {
          path: 'uploads/:bidID',
          component: ViewUploadsComponent,
          resolve: {
            data: GetFilesForProjectOrBidService
          }
        },
        /***********************************************
                            ACCOUNTING
         ************************************************/


        /***********************************************
                            CHAT
         ************************************************/

      ]
  },
  {
    path: 'password/forgot',
    component: ForgotPasswordComponent,
  },
  {
    path: 'password/reset',
    component: ResetPasswordComponent
  }];

@NgModule({
  imports: [
    BrowserModule,
    AuthModule,
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyDFtMYszj1GiNJONfmK_y4bs6joGb7URGo' }),
    HttpModule,
    FormsModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    Ng2Webstorage,
    TextMaskModule,
    NouisliderModule,
    MomentModule,
    BsDatepickerModule.forRoot(),
    SweetAlert2Module.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    PaginationModule.forRoot(),
    AccordionModule.forRoot(),
    // NgxBootstrapProductTourModule.forRoot(),
    RouterModule.forRoot(
      routes,
      {
        // Tell the router to use the HashLocationStrategy.
        useHash: true
      },
    ),
    NgxPermissionsModule.forRoot(),
    CarouselModule.forRoot(),
    InfiniteScrollModule
  ],
  declarations: [
    AppComponent,
    NavbarComponent,
    Sidebar_Directives,
    SidebarComponent,
    topbarComponent,
    CommonLayoutComponent,
    HomePageComponent,
    //dashboardComponent,
    NewProjectComponent,
    ProjectDetailsComponent,
    ContractorsComponent,
    ViewComponent,
    //BidsViewHBGComponent,
    RegisterComponent,
    LoginComponent,
    //NewBidComponent,
    RequestBidComponent,
    AlertsComponent,
    ModalComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ViewBidsComponent,
    ViewUploadsComponent,
    ChangelogComponent,
    ViewInvoiceComponent,
    ProfileComponent,
    SocialProfileComponent,
    SocialProfilePhotoComponent,
    ViewProfileComponent,
    ViewProfilePhotoComponent,
    Autosize,
    KeyObjectArrayPipePipe,
    LineNavbarComponent,
    GoogleMapComponent,
    InvoiceSendComponent,
    ProjectPostComponent,


  ],

  providers: [
    { provide: LOCALE_ID, useValue: 'en-CA' },
    BackendService,
    GlobalConstants,
    AlertService,
    {
      // Provider for APP_INITIALIZER
      provide: APP_INITIALIZER,
      useFactory: authServiceFactory,
      deps: [AuthService],
      multi: true
    },
    AuthService,
    ProjectRouteGuard,
    ProjectResolverService,
    NewOrCreateBidResolverService,
    GetBidsLineItemsResolverService,
    GetSCByTradeResolver,
    GetSctsOrProjectsOrBidTypeResolverService,
    GetBidsResolverService,
    GetBidsForSubprojectFromSubcontractorResolverService,
    GetPermissionsService,
    GetScboDataService,
    CanViewOrManageBidGuard,
    GetUploadsForProjectService,
    GetFilesForProjectOrBidService,
    GetAccountingDataResolverService,
    GetInvoiceDataResolverService,
    GetBuilderBidOutDataResolverService,
    CheckIfBboExistsResolverService,
    GetCreateOrEditBbodetailsResolverService,
    ProfileResolverService,
    ChatResolverService,
    GetDashboardDataResolverServiceService,
    GetAllNotificationsResolverService,
    ViewAllMessagesResolverService,
    GetAllSubcontractorsResolverService,
    GetHomeBuildersGuideResolverService,
    DataService,
    // {provide: APP_BASE_HREF, useValue: '/test'}
    NguiMapModule,
    SubprojectResolverService,
    BidsBySubprojectResolverService,
    GetAllBidsByProjectResolverService,
    BsDatepickerModule
  ],
  entryComponents: [
    RequestBidComponent,
  ],
  bootstrap
    :
    [AppComponent]
})

export class AppModule {
}
