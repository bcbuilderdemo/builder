import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {Alert, AlertType} from '../../interfaces/alert';
import {SweetAlertTypes} from '../../interfaces/sweet-alert.interface';
import swal from 'sweetalert2';
import 'rxjs/add/observable/timer';

@Injectable()
export class AlertService {
  private subject = new Subject<Alert>();
  private keepAfterRouteChange = false;


  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterRouteChange) {
          // only keep for a single route change
          this.keepAfterRouteChange = false;
        } else {
          // clear alert messages
          this.clear();
        }
      }
    });
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  success(messageType: string, message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Success, message, messageType, keepAfterRouteChange);
  }

  error(messageType: string, message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Error, message, messageType, keepAfterRouteChange);
  }

  info(messageType: string, message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Info, message, messageType, keepAfterRouteChange);
  }

  warn(messageType: string, message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Warning, message, messageType, keepAfterRouteChange);
  }

  alert(type: AlertType, message: string, messageType: string, keepAfterRouteChange = false) {
    this.clear();
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.subject.next(<Alert>{ type: type, message: message, messageType });
  }

  swalSuccess(title: string, text: string, showConfirmButton: boolean, confirmButtonText = 'Ok', timer = null, navigateAfter = false, route = '', reloadLocation = false) {
    swal({
      type: 'success',
      title: title,
      text: text,
      showConfirmButton: showConfirmButton,
      confirmButtonText: confirmButtonText,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
      timer: timer,
    }).then(() => {
        if (navigateAfter === true && timer != null) {
          console.log("in if");
          const t = Observable.timer(timer);
          t.subscribe(() => this.router.navigate([`${route}`]));
        } else if (navigateAfter === true && timer > 0) {
          console.log("in else");
          const t = Observable.timer(timer);
          t.subscribe(() => this.router.navigate([`${route}`]));
        }
    }, (dismiss) => {
      console.log("DISMISS", dismiss);
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (navigateAfter === true && timer != null) {
        console.log("in if");
        this.router.navigate([`${route}`]);
      }
      if (dismiss === 'timer' && reloadLocation) {
        console.log("in swal sucess dismiss");
        window.location.reload();
      }
      else if (dismiss === 'timer') {
        //this.router.navigate([`/${route}`]);
      }
    });
  }

  swalError(title: string, text: string, showConfirmButton: boolean, confirmButtonText = 'Ok', timer = null, navigateAfter = false, route = '') {
    swal({
      type: 'error',
      title: title,
      text: text,
      showConfirmButton: showConfirmButton,
      confirmButtonText: confirmButtonText,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
      timer: timer,
    }). then(() => {
      if (navigateAfter) {
        this.router.navigate([`/${route}`]);
      }
    },  (dismiss) => {
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (dismiss === 'timer') {
        this.router.navigate([`/${route}`]);
      }
    });
  }


  swalConfirm(title: string, text: string, showConfirmButton: boolean, confirmButtonText = 'Ok', showCancelButton: boolean, cancelButtonText = 'Cancel', timer = null, navigateAfter = false, route = '') {
    swal({
      title: title,
      text: text,
      type: 'warning',
      buttonsStyling: false,
      showConfirmButton: showConfirmButton,
      confirmButtonText: confirmButtonText,
      confirmButtonClass: 'btn btn-outline-success btn-lg mr-1',
      showCancelButton: showCancelButton,
      cancelButtonText: cancelButtonText,
      cancelButtonClass: 'btn btn-outline-danger btn-lg',
      timer: timer,
    }).then(() => {
      if (navigateAfter) {
        this.router.navigate([`/${route}`]);
      }
    }, (dismiss) => {
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (dismiss === 'cancel') {
      }
    });
  }

  clear() {
    // clear alerts
    this.subject.next();
  }
}
