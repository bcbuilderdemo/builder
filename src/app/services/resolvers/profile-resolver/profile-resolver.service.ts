// zs-dashboard-resolver ProfileResolverService
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { BackendService } from '../../backend.service';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { Observable } from 'rxjs/Observable';
import { GetProfileInfoInterface } from '../../../interfaces/profile-info.interface';


@Injectable()
export class ProfileResolverService implements Resolve<any>  {
  constructor(
    private API: BackendService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetProfileInfoInterface> {
    if (!route.routeConfig) {
      return this.API.getProfileInfo();
    }
    const path = route.routeConfig.path;

    if (path === 'dashboardSampleLayout') {
      return this.API.getProfileInfoForCurrentUser();
    }
    if (path === 'maps/google-map') {
      return this.API.getProfileInfoForCurrentUser();
    }
    if (path === 'dashboard') {
      return this.API.getProfileInfoForCurrentUser();
    }
    else if (path === 'editprofile' || path === 'profile' || path === 'profile-photos') {
      return this.API.getProfileInfo();
    }
    else if (path === 'invoice/project_id/:project_id/subproject_id/:subproject_id') {
      return this.API.getProfileInfo();
    }
    else if (path === 'subcontractors/bids/:subprojectID/invoice') {
      return this.API.getProfileInfo();
    }
    else if (path === 'subcontractors/view/:scTradeID/:hbg/:subcontractorID/:subprojectID/bidrequest') {
      return this.API.getProfileInfoForCurrentUser();
    }
    else if (path === 'subcontractors/view/:scTradeID/:hbg/:subprojectID/activeproject') {
      return this.API.getProfileInfoForCurrentUser();
    }
    else if (path === 'bids/:subprojectID/:bidID/invoice') {
      return this.API.getProfileInfoForCurrentUser();
    }
    else if (path === 'profile/:companySlug') {
      // LAZY. Change this so it fetches for current user w/o using params
      if (route.params.companySlug === 'view') {
        return this.API.getProfileInfoForCurrentUser();
      }
      return this.API.getProfileInfoForCompany(route.params.companySlug);
    }
    else if (path === 'profile/photo/:companySlug') {
      return this.API.getProfileInfoForCompany(route.params.companySlug);
    }
  }
}
