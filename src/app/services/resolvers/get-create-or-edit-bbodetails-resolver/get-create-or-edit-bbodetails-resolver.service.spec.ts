import { TestBed, inject } from '@angular/core/testing';

import { GetCreateOrEditBbodetailsResolverService } from './get-create-or-edit-bbodetails-resolver.service';

describe('GetCreateOrEditBbodetailsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetCreateOrEditBbodetailsResolverService]
    });
  });

  it('should be created', inject([GetCreateOrEditBbodetailsResolverService], (service: GetCreateOrEditBbodetailsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
