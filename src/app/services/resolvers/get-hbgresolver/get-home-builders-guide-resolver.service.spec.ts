import { TestBed, inject } from '@angular/core/testing';

import { GetHomeBuildersGuideResolverService } from './get-home-builders-guide-resolver.service';

describe('GetHomeBuildersGuideResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetHomeBuildersGuideResolverService]
    });
  });

  it('should be created', inject([GetHomeBuildersGuideResolverService], (service: GetHomeBuildersGuideResolverService) => {
    expect(service).toBeTruthy();
  }));
});
