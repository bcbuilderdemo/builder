import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BackendService} from '../../backend.service';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {GetHBGInterface} from '../../../interfaces/get-hbg.interface';

@Injectable()
export class GetHomeBuildersGuideResolverService implements Resolve<any>
{


  constructor(
    private API: BackendService,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetHBGInterface>
  {
      return this.API.getHomeBuildersGuide();
  }

}
