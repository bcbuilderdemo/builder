import { TestBed, inject } from '@angular/core/testing';

import { BidsBySubprojectResolverService } from './get-bids-by-subproject-resolver.service';

describe('BidsBySubprojectResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BidsBySubprojectResolverService]
    });
  });

  it('should be created', inject([BidsBySubprojectResolverService], (service: BidsBySubprojectResolverService) => {
    expect(service).toBeTruthy();
  }));
});
