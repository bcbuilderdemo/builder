import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { GetBidsBySubproject } from '../../../interfaces/get-bids-by-subproject.interface';
import { Observable } from 'rxjs/Observable';
import { SessionStorageService } from 'ngx-webstorage';
import { BackendService } from '../../backend.service';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class BidsBySubprojectResolverService implements Resolve<GetBidsBySubproject[]>  {

  gbbs: GetBidsBySubproject;

  constructor(
    private session: SessionStorageService,
    private API: BackendService,
    private router: Router,
  )
  {
    //Intentionally left empty
  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetBidsBySubproject[]>
  {
    const routeID = route.params.id;
    const subprojectID = route.params.subprojectID;
    
    return this.API.getBidsBySubproject(subprojectID);
  }
}
