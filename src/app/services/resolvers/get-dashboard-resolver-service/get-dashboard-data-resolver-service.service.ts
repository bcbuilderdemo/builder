// zs-dashboard-resolver
import { Injectable } from '@angular/core';
import {BackendService} from '../../backend.service';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { GetUserInfoAndProjects} from '../../../interfaces/get-project.interface';

@Injectable()
export class GetDashboardDataResolverServiceService implements Resolve<any>  {



  constructor(
    private API: BackendService,

  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetUserInfoAndProjects>
  {

    return this.API.getProjects();

  }

}
