import { TestBed, inject } from '@angular/core/testing';

import { NewOrCreateBidResolverService } from './new-or-create-bid.service';

describe('NewOrCreateBidResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewOrCreateBidResolverService]
    });
  });

  it('should be created', inject([NewOrCreateBidResolverService], (service: NewOrCreateBidResolverService) => {
    expect(service).toBeTruthy();
  }));
});
