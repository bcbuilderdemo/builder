import { TestBed, inject } from '@angular/core/testing';

import { GetInvoiceDataResolverService } from './get-invoice-data-resolver.service';

describe('GetInvoiceDataResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetInvoiceDataResolverService]
    });
  });

  it('should be created', inject([GetInvoiceDataResolverService], (service: GetInvoiceDataResolverService) => {
    expect(service).toBeTruthy();
  }));
});
