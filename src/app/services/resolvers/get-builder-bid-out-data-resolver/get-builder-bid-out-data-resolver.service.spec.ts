import { TestBed, inject } from '@angular/core/testing';

import { GetBuilderBidOutDataResolverService } from './get-builder-bid-out-data-resolver.service';

describe('GetBuilderBidOutDataResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetBuilderBidOutDataResolverService]
    });
  });

  it('should be created', inject([GetBuilderBidOutDataResolverService], (service: GetBuilderBidOutDataResolverService) => {
    expect(service).toBeTruthy();
  }));
});
