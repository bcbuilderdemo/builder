import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';
import {Observable} from 'rxjs/Observable';
import {GetBBOResolverData} from '../../../interfaces/get-bbo-resolver-data.interface';
import {CreateOrEditOrViewBboDetailsInterface} from '../../../interfaces/create-or-edit-bbo-details.interface';

@Injectable()
export class GetBuilderBidOutDataResolverService implements Resolve<any>
{

  constructor(private API: BackendService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetBBOResolverData>
                                        | Observable<CreateOrEditOrViewBboDetailsInterface>
  {


    console.log("getting bbo", route);
    const path = route.routeConfig.path;
    if (path === 'bid/:type/:sctID') {
      const type          = route.params.type;
      if (type === 'create') {
        const projectID = route.params.id;
        const sctID = route.params.sctID;
        
        return this.API.getProjectInfoForBBO(projectID, type, sctID);
      }
      if (type === 'view') {
        const projectID = route.params.id;
        const sctID     = route.params.sctID;
        return this.API.getProjectInfoForBBO(projectID, type, sctID);
      }
    }

    else {

      const bidID    = route.params.bidID;
      const bboID    = route.params.bboID;
      return this.API.getBBO(bidID, bboID);
    }
    }

}
