import { Injectable } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {Observable} from 'rxjs/Observable';
import {SessionStorageService} from 'ngx-webstorage';
import {BackendService} from '../../backend.service';
import {Observer} from 'rxjs/Observer';

@Injectable()
export class ProjectResolverService implements Resolve<ProjectDataInterface>  {

  projectData: ProjectDataInterface;

  constructor(
    private session: SessionStorageService,
    private API: BackendService,
    private router: Router,
  )
  {

    this.getProjectData();
  }

  resolve(route: ActivatedRouteSnapshot): Observable<ProjectDataInterface>
  {
    const routeID = route.params.id;
    let projectID = '';

    if (!!this.projectData) {
      projectID = this.projectData.id;
      if (projectID == routeID) {
        return Observable.of(this.projectData);
      } else {
        return this.API.getProjectInfo(routeID);
      }
    } else {
      return this.API.getProjectInfo(routeID);
    }
  }

/*    return new Observable<any>((observer) =>
    {
      const projectID = this.project.id;
      const id = this.route.snapshot.paramMap.get('id');


      // if (this.project) {
      //   observer.complete();
	  //
      //   return this.project;
      // } else {
      //   this.API.getProjectsInfo(projectID).subscribe((data => {
      //     data = this.project ;
      //   })
      // }
  });
    }

  */

  setProjectData(data: ProjectDataInterface) {
    if (!this.projectData || (this.projectData.id !== data.id)) {
      this.projectData = data;
      this.session.store('project', this.projectData);
    }
  }

  getProjectData() {
    this.projectData = this.session.retrieve('project');
    return this.projectData;
  }

}
