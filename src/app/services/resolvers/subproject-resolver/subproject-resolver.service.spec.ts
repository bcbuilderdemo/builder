import { TestBed, inject } from '@angular/core/testing';

import { SubprojectResolverService } from './subproject-resolver.service';

describe('ProjectResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubprojectResolverService]
    });
  });

  it('should be created', inject([SubprojectResolverService], (service: SubprojectResolverService) => {
    expect(service).toBeTruthy();
  }));
});
