import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { SubprojectDataInterface } from '../../../interfaces/subproject.interface';
import { Observable } from 'rxjs/Observable';
import { SessionStorageService } from 'ngx-webstorage';
import { BackendService } from '../../backend.service';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class SubprojectResolverService implements Resolve<SubprojectDataInterface>  {

  subprojectData: Array<any>;

  constructor(
    private session: SessionStorageService,
    private API: BackendService,
    private router: Router,
  ) {
    //Intentionally left empty
  }

  resolve(route: ActivatedRouteSnapshot): Observable<SubprojectDataInterface> {
    const routeID = route.params.id;
    console.log(routeID);
    return this.API.getAllSubprojects(routeID);
  }
}
