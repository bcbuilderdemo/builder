import { TestBed, inject } from '@angular/core/testing';

import { GetScboDataService } from './get-scbo-data.service';

describe('GetScboDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetScboDataService]
    });
  });

  it('should be created', inject([GetScboDataService], (service: GetScboDataService) => {
    expect(service).toBeTruthy();
  }));
});
