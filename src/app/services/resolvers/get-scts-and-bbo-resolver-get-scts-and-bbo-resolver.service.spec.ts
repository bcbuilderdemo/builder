import { TestBed, inject } from '@angular/core/testing';

import { GetSctsAndBboResolverGetSctsAndBboResolverService } from './get-scts-and-bbo-resolver-get-scts-and-bbo-resolver.service';

describe('GetSctsAndBboResolverGetSctsAndBboResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetSctsAndBboResolverGetSctsAndBboResolverService]
    });
  });

  it('should be created', inject([GetSctsAndBboResolverGetSctsAndBboResolverService], (service: GetSctsAndBboResolverGetSctsAndBboResolverService) => {
    expect(service).toBeTruthy();
  }));
});
