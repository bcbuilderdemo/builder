import { Injectable } from '@angular/core';
import { BackendService } from '../../backend.service';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class GetBidsForSubprojectFromSubcontractorResolverService implements Resolve<any>
{

    constructor(private API: BackendService,
        private auth: AuthService,
    ) {

    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.API.getBidsForSubprojectFromSubcontractor(route.params);
    }
}