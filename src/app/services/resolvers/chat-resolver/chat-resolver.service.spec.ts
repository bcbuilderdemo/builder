import { TestBed, inject } from '@angular/core/testing';

import { ChatResolverService } from './chat-resolver.service';

describe('ChatResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatResolverService]
    });
  });

  it('should be created', inject([ChatResolverService], (service: ChatResolverService) => {
    expect(service).toBeTruthy();
  }));
});
