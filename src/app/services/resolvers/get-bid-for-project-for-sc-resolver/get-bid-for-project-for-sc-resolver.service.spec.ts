import { TestBed, inject } from '@angular/core/testing';

import { GetBidForProjectForScResolverService } from './get-bid-for-project-for-sc-resolver.service';

describe('GetBidForProjectForScResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetBidForProjectForScResolverService]
    });
  });

  it('should be created', inject([GetBidForProjectForScResolverService], (service: GetBidForProjectForScResolverService) => {
    expect(service).toBeTruthy();
  }));
});
