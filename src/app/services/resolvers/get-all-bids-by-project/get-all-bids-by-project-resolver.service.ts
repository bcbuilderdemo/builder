import { Injectable } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {GetAllBidsByProject} from '../../../interfaces/get-all-bids-by-project.interface';
import {Observable} from 'rxjs/Observable';
import {SessionStorageService} from 'ngx-webstorage';
import {BackendService} from '../../backend.service';
import {Observer} from 'rxjs/Observer';

@Injectable()
export class GetAllBidsByProjectResolverService implements Resolve<GetAllBidsByProject>  {

  allBidsByProject: GetAllBidsByProject;

  constructor(
    private session: SessionStorageService,
    private API: BackendService,
    private router: Router,
  )
  {
    //Intentionally left empty
  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetAllBidsByProject>
  {
    const routeID = route.params.id;
    return this.API.getAllBidsByProject(routeID);
  }


}
