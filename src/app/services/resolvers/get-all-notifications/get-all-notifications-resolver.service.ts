// zs-dashboard-resolver
import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {BackendService} from '../../backend.service';
import {NotificationsInterface} from '../../../interfaces/notifications.interface';

@Injectable()
export class GetAllNotificationsResolverService implements Resolve<any>  {



  constructor(
    private API: BackendService,

  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<NotificationsInterface>
  {
    if (route.routeConfig.path === 'dashboard') {
      return this.API.getAllNotifictions();
    }
    else {
      return this.API.getAllNotifictionsForProject(route.params.id);
    }

  }

}
