// zs-dashboard-resolver
import { TestBed, inject } from '@angular/core/testing';

import { GetAllNotificationsResolverService } from './get-all-notifications-resolver.service';

describe('GetAllNotificationsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetAllNotificationsResolverService]
    });
  });

  it('should be created', inject([GetAllNotificationsResolverService], (service: GetAllNotificationsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
