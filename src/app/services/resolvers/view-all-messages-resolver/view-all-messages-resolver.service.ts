import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {GetProfileInfoInterface} from '../../../interfaces/profile-info.interface';
import {BackendService} from '../../backend.service';
import {ViewAllMessagesInterface} from '../../../interfaces/view-all-messages.interface';

@Injectable()
export class ViewAllMessagesResolverService implements Resolve<any>  {



  constructor(
    private API: BackendService,

  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<ViewAllMessagesInterface>
  {
    const path = route.routeConfig.path;
    console.log(path);

    if (path === 'messages') {
      return this.API.getAllMessages();
    }
    else if (path === 'projects/:id/messages') {
      return this.API.getAllMessagesForProject(route.params.id);
    }


  }

}
