import { TestBed, inject } from '@angular/core/testing';

import { GetPermissionsService } from './get-permissions.service';

describe('GetPermissionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetPermissionsService]
    });
  });

  it('should be created', inject([GetPermissionsService], (service: GetPermissionsService) => {
    expect(service).toBeTruthy();
  }));
});
