// zs-dashboard-resolver GetPermissionsService
import { Injectable } from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class GetPermissionsService implements Resolve<any>  {


  constructor(
    private auth: AuthService,
  )
  {
  }

  resolve(): Observable<any>
  {
    
    return this.auth.getPermissionsFromAPI();

  }

}
