import { TestBed, inject } from '@angular/core/testing';

import { GetAccountingDataResolverService } from './get-accounting-data-resolver.service';

describe('GetAccountingDataResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetAccountingDataResolverService]
    });
  });

  it('should be created', inject([GetAccountingDataResolverService], (service: GetAccountingDataResolverService) => {
    expect(service).toBeTruthy();
  }));
});
