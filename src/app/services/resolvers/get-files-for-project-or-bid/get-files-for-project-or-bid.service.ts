import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { BackendService } from '../../backend.service';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../auth/auth.service';
import { FilesInterface } from '../../../interfaces/files.interface';

@Injectable()
export class GetFilesForProjectOrBidService implements Resolve<FilesInterface>
{


  constructor
    (
    private API: BackendService,
    private auth: AuthService,
    private router: Router
    ) {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<FilesInterface> {
    const projectID = route.params.id;
    const bidID = route.params.bidID;

    console.log("BIDID", bidID);

    if (this.auth.hasPermission('builder')) {
      return this.API.getAllFilesForProject(projectID);
    }
    else if (this.auth.hasPermission('subcontractor')) {
      return this.API.getAllFilesForProject(projectID, bidID);
    }
    else {
      this.router.navigate(['/dashboard']);
    }
  }

}
