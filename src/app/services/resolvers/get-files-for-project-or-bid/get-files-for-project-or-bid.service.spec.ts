import { TestBed, inject } from '@angular/core/testing';

import { GetFilesForProjectOrBidService } from './get-files-for-project-or-bid.service';

describe('GetFilesForProjectOrBidService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetFilesForProjectOrBidService]
    });
  });

  it('should be created', inject([GetFilesForProjectOrBidService], (service: GetFilesForProjectOrBidService) => {
    expect(service).toBeTruthy();
  }));
});
