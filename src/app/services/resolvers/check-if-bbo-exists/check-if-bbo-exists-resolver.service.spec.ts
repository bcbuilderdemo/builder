import { TestBed, inject } from '@angular/core/testing';

import { CheckIfBboExistsResolverService } from './check-if-bbo-exists-resolver.service';

describe('CheckIfBboExistsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckIfBboExistsResolverService]
    });
  });

  it('should be created', inject([CheckIfBboExistsResolverService], (service: CheckIfBboExistsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
