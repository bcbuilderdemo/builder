// zs-dashboard-resolver GetSctsOrProjectsOrBidTypeResolverService
import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {BackendService} from '../../backend.service';
import {Observable} from 'rxjs/Observable';
import {ScTradesPaginatorInterface} from '../../../interfaces/sctrades-paginator.interface';
import {AuthService} from '../../auth/auth.service';
import {GetProjectsInterface} from '../../../interfaces/get-project.interface';
import {
  BidsInInterface, BidsOutInterface,
  GetBidsByTradeInterface
} from '../../../interfaces/get-bids-by-trade.interface';
import {GetScBidsPaginatorInterface} from '../../../interfaces/get-sc-bids-paginator.interface';

@Injectable()
export class GetSctsOrProjectsOrBidTypeResolverService implements Resolve<any>
{

  constructor(private API: BackendService,
              private auth: AuthService,
              private router: Router
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot):   Observable<ScTradesPaginatorInterface>
                                          | Observable<GetProjectsInterface>
                                          | Observable<GetBidsByTradeInterface>
                                          | Observable<BidsInInterface>
                                          | Observable<BidsOutInterface>
                                          | Observable<GetScBidsPaginatorInterface>
  {
    const url = route.url[0].path
    let bidType = route.params.type;
    const projectID = route.params.id;
    if (url === 'dashboard') {
      bidType = 'all';
    }

    if (this.auth.hasPermission('subcontractor')) {
      if (bidType === 'all') {
        return this.API.getSCBidType('all');
      } else if (projectID && bidType === 'for-project') {
        return this.API.getSCBidType('all', projectID);
      }
    }
    else if (this.auth.hasPermission('builder')) {
      if (bidType === 'all') {
        return this.API.getSubContractorTradesForAllBids();
      }
      return this.API.getSubContractorTradesForProject(projectID);
    }
    else
      {
      this.router.navigate(['/dashboard']);
    }

  }

}
