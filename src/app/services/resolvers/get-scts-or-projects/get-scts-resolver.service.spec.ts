import { TestBed, inject } from '@angular/core/testing';

import { GetSctsOrProjectsOrBidTypeResolverService } from './get-scts-or-projects-resolver.service';

describe('GetSctsOrProjectsOrBidTypeResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetSctsOrProjectsOrBidTypeResolverService]
    });
  });

  it('should be created', inject([GetSctsOrProjectsOrBidTypeResolverService], (service: GetSctsOrProjectsOrBidTypeResolverService) => {
    expect(service).toBeTruthy();
  }));
});


