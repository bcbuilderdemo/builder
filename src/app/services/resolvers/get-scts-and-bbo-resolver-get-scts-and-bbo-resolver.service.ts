import { Injectable } from '@angular/core';
import {BackendService} from '../backend.service';
import {ActivatedRouteSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {SubcontractorPaginatorInterface} from '../../interfaces/subcontractor.interface';

@Injectable()
export class GetSctsAndBboResolverGetSctsAndBboResolverService
{

  constructor(private API: BackendService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<SubcontractorPaginatorInterface>
  {
    const scTradeID = route.params.scTradeID;
    const projectID = route.params.id;
    return this.API.getSCTsAndBBO(projectID, scTradeID);
  }
}
