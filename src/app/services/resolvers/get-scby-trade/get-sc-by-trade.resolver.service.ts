import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { Observable } from 'rxjs/Observable';
import { SessionStorageService } from 'ngx-webstorage';
import { BackendService } from '../../backend.service';
import { SubcontractorPaginatorInterface } from '../../../interfaces/subcontractor.interface';
import { GetSctsAndBboInterface } from '../../../interfaces/get-scts-and-bbo.interface';
@Injectable()
export class GetSCByTradeResolver implements Resolve<any>
{
  constructor(
    private API: BackendService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<SubcontractorPaginatorInterface> | Observable<GetSctsAndBboInterface> {

    const scTradeID = route.params.scTradeID;
    const projectID = route.params.id;
    if (projectID) {
      return this.API.getSCTsAndBBO(projectID, scTradeID);
    } else {
      return this.API.getSubcontractorByTrade(scTradeID);
    }
  }
}
