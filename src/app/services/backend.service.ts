import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions } from '@angular/http';
import { RegisterInterface } from '../interfaces/register-interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { NewProjectInterface } from '../interfaces/new-project-interface';
import { AuthService } from './auth/auth.service';
import { AuthHttp } from 'angular2-jwt';
import { NewBidInterface } from '../interfaces/new-bid.interface';
import { GetProjectsInterface, GetUserInfoAndProjects, ProjectDataInterface } from '../interfaces/get-project.interface';
import { SubcontractorInterface, SubcontractorPaginatorInterface } from '../interfaces/subcontractor.interface';
import { GetBidFormInterface } from '../interfaces/get-bid-form-interface';
import { GlobalConstants } from '../constants/global.constants';
import { ScTradesPaginatorInterface } from '../interfaces/sctrades-paginator.interface';
import { BidsInInterface, BidsOutInterface, GetBidsByTradeInterface } from '../interfaces/get-bids-by-trade.interface';
import { ScBidFormInterface } from '../interfaces/sc-bid-form';
import { GetSctsAndBboInterface } from '../interfaces/get-scts-and-bbo.interface';
import { GetScBidsPaginatorInterface } from '../interfaces/get-sc-bids-paginator.interface';
import { ProjectAndBuilderInfo } from '../interfaces/project-and-builder-info';
import { AccountingInterface } from '../interfaces/accounting-data.interface';
import { AccountingAndInvoiceDataInterface } from '../interfaces/accountingandinvoice.interface';
import { GetBBOResolverData } from '../interfaces/get-bbo-resolver-data.interface';
import { CreateOrEditOrViewBboDetailsInterface } from '../interfaces/create-or-edit-bbo-details.interface';
import { GetProfileInfoInterface } from '../interfaces/profile-info.interface';
import { GetChatAndUploadsInterface } from '../interfaces/chatpaginator.interface';
import { ViewAllMessagesInterface } from '../interfaces/view-all-messages.interface';
import { NotificationsInterface } from '../interfaces/notifications.interface';
import { GetHBGInterface } from '../interfaces/get-hbg.interface';
import { SubprojectDataInterface } from '../interfaces/subproject.interface';
import { GetAllBidsByProject } from '../interfaces/get-all-bids-by-project.interface';
import { GetBidsBySubproject } from '../interfaces/get-bids-by-subproject.interface';

//Nadim Feb 13 added library for angular upgrade from 4 to 5
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class BackendService {

  // private readonly API = 'http://bcBuilder.app/api/v1';
  // private readonly API = 'http://neilbrar.com//api/v1';

  API: string;

  headers: Headers = new Headers({ 'Content-Type': 'application/json' });
  options: RequestOptions = new RequestOptions({ headers: this.headers });

  //readonly headers: Headers = new Headers({ 'Content-Type': 'application/json' });
  //readonly options: RequestOptions = new RequestOptions({ headers: this.options });

  constructor
    (
    private http: Http,
    private authHttp: AuthHttp,
    private auth: AuthService,
    private constants: GlobalConstants,
  ) {
    this.API = this.constants.APIURL;
    this.register = this.register.bind(this);
  }

  register(data: RegisterInterface): Observable<any> {
    const URL = `${this.API}/register`;
    return this.http.post(URL, data, this.options)
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }


  newProject(data: NewProjectInterface): Observable<any> {
    const URL = `${this.API}/projects/new`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  uploadProjectFiles(data: FormData): Observable<any> {
    const URL = `${this.API}/projects/upload/files`;
    const headers = new Headers();
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  submitBidRequest(data: SubprojectDataInterface): Observable<any> {
    const URL = `${this.API}/subproject/${data.id}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }


  deleteFile(projectID: string, bidID: number, scTradeID: any, fileID: number) {
    const URL = `${this.API}/projects/${projectID}/bid/${bidID}/${scTradeID}/uploads/${fileID}/delete/`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, '', options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  forgotPassword(input): Observable<any> {
    console.log("input", input);
    const URL = `${this.API}/password/forgot`;
    return this.authHttp.post(URL, input, this.options)
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getProjects(): Observable<GetUserInfoAndProjects> {
    const URL = `${this.API}/projects/user`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getProjectsByID(input): Observable<any> {
    const URL = `${this.API}/projects/${input}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }


  getSubProjects(): Observable<any> {
    let URL = `${this.API}/scs/scTradeID`;
    return this.authHttp.get(URL, this.options)
      .flatMap((response: Response) => {
        let scTradeID = response.json().scTradeID;
        URL = `${this.API}/projects/subprojectsBySct/` + scTradeID;
        return this.authHttp.get(URL, this.options)
      })
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getBidsForSubprojectFromSubcontractor(params) {
    let URL = `${this.API}/subproject/` + params.subprojectID + `/bids/` + params.subcontractorID;

    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }
  getAllBidsLineItemsFromBuilder(params) {

    let URL = `${this.API}/subproject/` + params.subprojectID + `/bids`;
    return this.authHttp.get(URL, this.options)
      .flatMap((response: Response) => {
        let subprojectBid = response.json();
        let bidId = -1;
        let bidIn = null;
        let bidOut = null;
        if (subprojectBid.length !== 0) {
          subprojectBid.forEach(bid => {
            if (bidId === -1 && parseInt(bid.is_bid_out) === 0) {
              bidId = bid.id
              bidIn = bid
            }
            if (parseInt(bid.is_bid_out) === 1 && !bidOut) {
              bidOut = bid
            }
          });

        }
        URL = `${this.API}/bids/` + bidId + `/bids-line-items`;
        return this.authHttp.get(URL, this.options)
          .map((res: Response) => {
            let resJson = res.json();
            return {
              bid: bidIn,
              bidOut: bidOut,
              bidLineItems: resJson
            }
          })
          .share()
          .catch((err) => this.handleError(err, this));
      });
  }

  updateBid(bid) {
    let URL = `${this.API}/bids/updateBid`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, bid, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getSCBidType(type: string, projectID?): Observable<GetScBidsPaginatorInterface> {
    const URL = (projectID) ? `${this.API}/bids/get-bids-for-sc/${type}/${projectID}` : `${this.API}/bids/get-bids-for-sc/${type}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getBidsProjectForSC(projectID): Observable<any> {
    const URL = `${this.API}/${projectID}/sc/get-bids-for-project`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }



  changePassword(input): Observable<any> {
    const URL = `${this.API}/password/change`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, input, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  changeEmail(input): Observable<any> {
    const URL = `${this.API}/email/change`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, input, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /*checkProjectOwnership(params: URLSearchParams): Observable<any> {
    const URL = `${this.API}/projects/gpo?id=${params}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }*/

  getProjectInfo(id: string): Observable<ProjectDataInterface> {
    const URL = `${this.API}/projects/${id}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /******************************************************
                           BIDS
   *******************************************************/

  getAllBidsByProject(project_id: any): Observable<any> {
    let URL = `${this.API}/bids/${project_id}/all`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getBidsBySubproject(subproject_id: number): Observable<GetBidsBySubproject[]> {
    const URL = `${this.API}/subproject/${subproject_id}/bids`;

    return this.auth.refreshToken()
      .flatMap(
        () =>
          this.authHttp.get(URL, this.options)
      )
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }


  getBuilderBidOutFormByTrade(projectID: number, scTradeID: number): Observable<GetBidFormInterface> {
    const URL = `${this.API}/projects/${projectID}/scs/${scTradeID}/bids`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  sendBidToSelectedSCS(data: Array<any>, projectID: number, scTradeID: number, bidID: number): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/scs/${scTradeID}/bid/${bidID}/send`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, { data: data }, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAllBidsByTrade(projectID: number, scTradeID: number, bidType: string): Observable<GetBidsByTradeInterface> {
    const URL = `${this.API}/projects/${projectID}/scs/${scTradeID}/bid/get/${bidType}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAllBidsForProject(projectID: number): Observable<GetBidsByTradeInterface> {
    const URL = `${this.API}/projects/${projectID}/bids/all`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  setBidsLineItems(params) {
    let URL = `${this.API}/request/bid/out`;

    console.log(params);
    return this.auth.refreshToken()
      .flatMap(() => {
        let val = this.authHttp.post(URL, params, this.options);
        return val;
      })
      .map((response: Response) => {
        return response.json();
      })
      .share()
      .catch((err) => this.handleError(err, this));
  }

  newBid(data, projectID: string, scTradeID: number): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/bid/create-or-update/${scTradeID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  editBid(data, projectID: string, scTradeID: number): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/bid/create-or-update/${scTradeID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getFilesAndProjectDetailsForBid(bidID: number, projectID: string): Observable<any> {
    const URL = `${this.API}/bids/${bidID}/files-and-proj-data/get/${projectID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getProjectAndBuilderInfoForSCBidOut(bidID: number, projectID: string): Observable<ProjectAndBuilderInfo> {
    const URL = `${this.API}/bids/${bidID}/builder-and-proj-data/get/${projectID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getBBO(bidID, bboID): Observable<GetBBOResolverData> {
    const URL = `${this.API}/bids/${bidID}/bbo/${bboID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getProjectInfoForBBO(projectID, type, scTradeID): Observable<CreateOrEditOrViewBboDetailsInterface> {
    const URL = `${this.API}/projects/${projectID}/bids/bbo/${type}/sct/${scTradeID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }


  SCSendBidOut(data: ScBidFormInterface, projectID: number, bidID: number): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/sc/bid/${bidID}/respond`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, { data: data }, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getSCBidOut(projectID: number, bidID: number): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/sc/bid/${bidID}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  acceptBid(bidID: number): Observable<any> {
    const URL = `${this.API}/bids/${bidID}/accept`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, '', this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  rejectBid(bidID: number): Observable<any> {
    const URL = `${this.API}/bids/${bidID}/reject`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, '', this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  checkIfBidFormExistsForTrade(projectID, sctID) {
    const URL = `${this.API}/projects/${projectID}/bids/trade/${sctID}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }


  /******************************************************
                    SUBCONCTRACTORS
   *******************************************************/


  getSubcontractorForProject(id: number, projectID: number) {
    const URL = `${this.API}/scs/${id}/project/${projectID}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getSubcontractorByTrade(id: number): Observable<SubcontractorPaginatorInterface> {
    const URL = `${this.API}/scs/get/${id}/all`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }


  getSCTsAndBBO(projectID, id: number): Observable<SubcontractorPaginatorInterface> {
    const URL = `${this.API}/projects/${projectID}/bids/get-bbo-by-sct-trade/${id}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }



  getAllSubprojects(projectID: number): Observable<SubprojectDataInterface> {
    const URL = `${this.API}/projects/subprojectsByprojectID/${projectID}`;

    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));

    // const x = this.auth.refreshToken()
    //   .flatMap(() => this.authHttp.get(URL, this.options))
    //   .map((response: Response) => response.json())
    //   .share()
    //   .catch((err) => this.handleError(err, this));
    //var response = this.authHttp.get(URL, this.options).map((response: Response) => response.json());
    //console.log('logging \n' + JSON.stringify(response));
    //return x;
  }

  getAllSubContractors(): Observable<SubcontractorPaginatorInterface> {
    const URL = `${this.API}/scs/get`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  // For register select
  getSubContractors(): Observable<SubcontractorInterface> {
    const URL = `${this.API}/trades`;
    return this.http.get(URL, this.options)
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getSubContractorTrades(): Observable<ScTradesPaginatorInterface> {
    const URL = `${this.API}/scs/trades`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  // zs-dashboard-service getSubContractorTradesForProject
  getSubContractorTradesForProject(projectID): Observable<ScTradesPaginatorInterface> {
    const URL = `${this.API}/projects/${projectID}/scs/trades`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  // zs-dashboard-service getSubContractorTradesForAllBids
  getSubContractorTradesForAllBids(): Observable<ScTradesPaginatorInterface> {
    const URL = `${this.API}/bids/scs/trades`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getPaginatorPage(URL: string): Observable<any> {
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getPaginatorPageNEW(URL: string): Observable<any> {
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /****************************************************
                      UPLOADS
   ****************************************************/

  getAllFilesForBid(projectID: string, bidID: number) {
    const URL = `${this.API}/projects/${projectID}/bid/${bidID}/uploads/get`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }


  getAllFilesForProject(projectID, bidID?) {
    let URL = `${this.API}/projects/${projectID}/uploads/get-files-for-project`;
    if (bidID) { URL = `${URL}/${bidID}`; }
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().projectFiles)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getFile(fileID, bidID?): string {
    let URL = '';
    this.auth.refreshToken().subscribe(() => {
      if (this.auth.hasPermission('builder')) {
        console.log("bu");
        URL = `${this.API}/uploads/get-file/${fileID}?token=${this.auth.token}`;
      }
      else if (this.auth.hasPermission('subcontractor')) {
        console.log("sub");
        URL = `${this.API}/uploads/get-file/${fileID}?token=${this.auth.token}`;
      }
    });
    return URL;
  }

  getProjectFile(fileID): string {
    let URL = '';
    this.auth.refreshToken().subscribe(() => {
      if (this.auth.hasPermission('builder')) {
        URL = `${this.API}/projects/get-file/${fileID}?token=${this.auth.token}`;
      }
      else if (this.auth.hasPermission('subcontractor')) {
        URL = `${this.API}/projects/get-file/${fileID}?token=${this.auth.token}`;
      }
    });
    return URL;
  }

  deleteProjectFile(fileID) {
    let URL = `${this.API}/projects/delete-file/${fileID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /*****************************************************
                     Accounting
   *****************************************************/

  getAllAccounting(type: string): Observable<AccountingInterface> {
    const URL = `${this.API}/accounting/${type}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAccountingForProject(projectID, type: string): Observable<AccountingInterface> {
    const URL = `${this.API}/projects/${projectID}/accounting/${type}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getSCAccountingForProject(projectID, type): Observable<AccountingInterface> {
    const URL = `${this.API}/projects/${projectID}/accounting/${type}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAccountingAndInvoiceForProject(projectID, accountingID): Observable<AccountingAndInvoiceDataInterface> {
    const URL = `${this.API}/projects/${projectID}/accounting/${accountingID}/invoice`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAccountingReportsForAllProjects(): Observable<AccountingInterface> {
    const URL = `${this.API}/accounting/reports/all-projects`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAccountingReportForProject(projectID): Observable<AccountingInterface> {
    const URL = `${this.API}/projects/${projectID}/accounting/reports/for-project`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, options))
      .map((response: Response) => response.json().data)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  sendPayment(input, projectID, accountingID): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/accounting/${accountingID}/make-payment`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, input, options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  changePaymentStatus(input, projectID, accountingID, apmID): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/sc/accounting/${accountingID}/payment/${apmID}`;
    const headers = new Headers({ 'Accept': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, { status: input }, options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }


  /****************************************************
                    PERMISSIONS
   ****************************************************/

  checkProjectOwnership(params: URLSearchParams): Observable<any> {
    const URL = `${this.API}/projects/gpo?id=${params}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  checkCanViewOrManageBidSCBidOut(bidID: number, params: URLSearchParams) {
    const URL = `${this.API}/permissions/bids/${bidID}/can-view-or-manage-bid?${params}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /******************************************************
                         PROFILE
   *******************************************************/

  getProfileInfo(): Observable<GetProfileInfoInterface> {
    const URL = `${this.API}/profile`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getProfileInfoForCurrentUser(): Observable<GetProfileInfoInterface> {
    const URL = `${this.API}/profile/user/current`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }


  getProfileInfoForCompany(slug): Observable<GetProfileInfoInterface> {
    const URL = `${this.API}/profile/${slug}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  updateProfile(data): Observable<any> {
    const URL = `${this.API}/profile/update`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  updateProfileImage(data: FormData): Observable<any> {
    const URL = `${this.API}/profile/update/image`;
    const headers = new Headers();
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  deleteProfileImage(data: FormData): Observable<any> {
    const URL = `${this.API}/profile/delete/image`;
    const headers = new Headers();
    const options = new RequestOptions({ headers: headers });
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /******************************************************
                          CHAT
   *******************************************************/

  getChat(projectID, uuid): Observable<GetChatAndUploadsInterface> {
    const URL = `${this.API}/projects/${projectID}/chat/${uuid}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  sendMessage(data, projectID, uuid): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/chat/${uuid}`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  uploadFileForChat(data, projectID, uuid): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/chat/${uuid}/upload`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, data, this.options))
      .map((response: Response) => response.json().message)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getUploadForChat(projectID, uuid, fileUUID): string {
    let URL = '';
    this.auth.refreshToken().subscribe(() => {
      URL = `${this.API}/projects/${projectID}/chat/${uuid}/get-file/${fileUUID}?token=${this.auth.token}`;
    });
    return URL;
  }

  getAllMessages(): Observable<ViewAllMessagesInterface> {
    const URL = `${this.API}/messages`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  getAllMessagesForProject(projectID): Observable<ViewAllMessagesInterface> {
    const URL = `${this.API}/projects/${projectID}/messages`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }
  /******************************************************
                         NOTIFICATIONS
   *******************************************************/

  getAllNotifictions(): Observable<NotificationsInterface> {
    const URL = `${this.API}/notifications/all`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  // zs-dashboard-service getAllNotifictionsForProject
  getAllNotifictionsForProject(projectID): Observable<NotificationsInterface> {
    const URL = `${this.API}/projects/${projectID}/notifications`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  markBBOAsReadBySC(projectID, bidID, bboID): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/notifications/bids/${bidID}/bbo/${bboID}/read`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, '', this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  markSCBOAsReadByBuilder(projectID, bidID, scboID): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/notifications/bids/${bidID}/scbo/${scboID}/read`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, '', this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  markSCBOAsReadBySCAfterStatusChange(projectID, bidID, scboID): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/notifications/bids/${bidID}/scbo/${scboID}/read-by-sc`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, '', this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  markMessagesAsRead(projectID, chatUUID): Observable<any> {
    const URL = `${this.API}/projects/${projectID}/notifications/messages/${chatUUID}/read`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.put(URL, '', this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }
  /*****************************************************
                 HOME BUILDERS GUIDE
   *****************************************************/

  getHomeBuildersGuide(): Observable<GetHBGInterface> {
    const URL = `${this.API}/hbg`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => response.json().hbg)
      .share()
      .catch((err) => this.handleError(err, this));
  }

  /*****************************************************
                 POSTS
   *****************************************************/
  publishPost(postInfo): Observable<any> {
    const URL = `${this.API}/post`;
    return this.auth.refreshToken()
      .flatMap(() => this.authHttp.post(URL, postInfo, this.options))
      .map((response: Response) => response.json())
      .share()
      .catch((err) => this.handleError(err, this));
  }

  handleError(error: Response | any, self) {
    if ((!error.message && !error.status) || error.message === "No JWT present or has expired") {
      self.auth.logout();
      return;
    }
    const errorsResponses = error.json().message || error.error || error.json().error || error.error.json() || error.json()
      || error || error.message || error.message.json() || error.errorInfo || error.json().errorInfo || "Server Error.";

    console.log(errorsResponses);
    if (error.status === 422) {
      console.log(error);
      return Observable.throw(error.json().error);
    }
    return Observable.throw(errorsResponses);
  }
}
