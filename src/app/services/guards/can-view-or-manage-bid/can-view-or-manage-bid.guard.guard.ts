import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {BackendService} from '../../backend.service';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../../auth/auth.service';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Response} from '@angular/http';

@Injectable()
export class CanViewOrManageBidGuard implements CanActivate {

  constructor(
    // private API:    BackendService,
    private auth:   AuthService,
    private authHttp: AuthHttp,
    private router: Router

  ) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ):  boolean| Observable<boolean>  {

    console.log("guard view or manage bid");
    console.log('st', state);
    const path        = next.routeConfig.path;
    const params      = new URLSearchParams;
    const bidID       = next.params.bidID;
    const uType       = next.params.uType;
    const permission  = uType === 'sc' ? 'manage' : 'view';
    if (path === 'projects/:id/bids/:bidID/:uType/quote')
    {
      params.append('userType', uType);
      params.append('permission', permission);
      const URL = `${this.auth.API}/permissions/bids/${bidID}/can-view-or-manage-bid?${params}`;
      return this.auth.refreshToken()
        .flatMap(() => this.authHttp.get(URL, this.auth.options))
        .map((response: Response) => {
            const validated = response.json().validated;
            if ( ! validated) {
              //this.router.navigate(['/dashboard']);
              return validated;
            }
            return validated;
        })
        .share()
        .catch(err => this.auth.handleError(err, this.auth));
    }
    else
    {
        this.router.navigate(['/dashboard']);
        return false;
    }
  }
}
