import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {SessionStorageService} from 'ngx-webstorage';
import {BackendService} from '../backend.service';

@Injectable()
  export class ProjectRouteGuard implements CanActivate
{

  constructor
  (private router: Router,
   private session: SessionStorageService,
   private API: BackendService,
   )
  {

  }

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean
{
  const id = route.params.id;
  return new Observable<boolean>((observer) =>
  {
    if (this.checkIfIDInStorage(id)) {
      observer.next(true);
      observer.complete();
    }
    else {
      const params = new URLSearchParams();
      params.append('id', id);
        this.API.checkProjectOwnership(id).subscribe((data) =>
        {
          if (data.type === 'AL') {
            console.log("PROJ ROUTE GUARD", data.type);
            this.storeProjectID(id);
            observer.next(true);
            observer.complete();
          }
          else {
            this.router.navigate(['/dashboard']);
            observer.next(false);
            observer.complete();
            return false;
          }
        });
    }
  });
}

  storeProjectID(id: string)
  {
    this.session.store('id', id);
  }

  getProjectID()
  {
    return this.session.retrieve('id');
  }

  checkIfIDInStorage(id: string)
  {
    const storedID = this.getProjectID();
    return (id === storedID);
  }

}
