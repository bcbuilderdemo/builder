import { NgModule } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import {LocalStorageService} from 'ngx-webstorage';

export function authHttpServiceFactory(http: Http, options: RequestOptions, storage: LocalStorageService) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'token',
    tokenGetter: (() => storage.retrieve('token')),
    globalHeaders: [{ 'Accept': 'application/json' }],
  }), http, options);
}

@NgModule({
  providers: [
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions, LocalStorageService]
    }
  ]
})
export class AuthModule {}
