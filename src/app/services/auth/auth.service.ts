import { Injectable } from '@angular/core';
import { AuthHttp, JwtHelper } from 'angular2-jwt';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { GlobalConstants } from '../../constants/global.constants';
import { NgxPermissionsService } from 'ngx-permissions';

@Injectable()
export class AuthService {

  API: string;
  private jwtHelper: JwtHelper = new JwtHelper();
  private rootPage = 'login';
  token;
  refreshSubscription: any;

  authNotifier: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
  options = new RequestOptions({ headers: this.headers });

  constructor(public http: Http,
    private authHttp: AuthHttp,
    private storage: LocalStorageService,
    private constants: GlobalConstants,
    private session: SessionStorageService,
    private permissionsService: NgxPermissionsService
  ) {
    console.log("LOCAL STORAGE TOKEN", this.storage.retrieve('token'));
    this.API = this.constants.APIURL;
    console.log('Hello ProvidersAuthServiceProvider Provider');
    this.token = this.getToken();
  }

  login(input) {
    const URL = `${this.API}/login`;
    return this.http.post(URL, input, this.options)
      .map((response: Response) => response.json().token)
      .catch(err => this.handleError(err, this))
      .share();
  }


  verifyToken(token: string) {
    return !this.jwtHelper.isTokenExpired(token);
  }

  getPermissionsFromAPI(): Observable<any> {
    const URL = `${this.API}/permissions/get`;
    return this.refreshToken()
      .flatMap(() => this.authHttp.get(URL, this.options))
      .map((response: Response) => {
        const res = response.json().data;
        this.permissionsService.addPermission(res);
        return res;
      })
      .share()
      .catch(err => this.handleError(err, this));
  }

  getPermissions() {
    return this.permissionsService.getPermissions();
  }

  hasPermission(permission: string) {
    return this.permissionsService.getPermission(permission);
  }

  setPermissions(permissions) {
    console.log("setting permissions");
    this.permissionsService.loadPermissions(permissions);
    console.log("setting permissions are", this.permissionsService.getPermissions());
  }

  refreshToken(): Observable<any> {
    const URL = `${this.API}/refresh?token=${this.token}`;
    if (this.jwtHelper.isTokenExpired(this.token)) {
      return this.authHttp.get(URL)
        .map((rsp) => {
          this.setToken(rsp.json().token);
          this.authNotifier.next(true);
          this.getPermissionsFromAPI().subscribe();
          return rsp.json().token;
        },
          err => {
            this.authNotifier.next(false);
            this.logout();
            this.handleError(err, this);
          })
        .share();
    }
    else {
      return Observable.of(this.token);
    }
  }

  logout() {
    this.rootPage = 'login';
    this.permissionsService.flushPermissions();
    this.clearToken();
    this.authNotifier.next(false);
  }

  authenticationNotifier(): Observable<boolean> {
    return this.authNotifier;
  }


  checkToken() {
    if (this.token === '' || this.token === null || this.token === undefined) {
      this.authNotifier.next(false);
      this.logout();
      return false;
    }
    return true;
  }

  setToken(token: string) {
    this.storage.store('token', token);
    this.token = token;
    this.authNotifier.next(true);
  }

  getToken() {
    this.token = this.storage.retrieve('token');
    return this.token;
  }

  clearToken() {
    this.token = '';
    this.storage.clear();
    this.session.clear();
  }

  onStartupRefresh(): Promise<any> {
    console.log(this.token);
    // const URL = `${this.API}/refresh?token=${this.token}`;
    const URL = `${this.API}/refresh/startup?token=${this.token}`;
    if (this.checkToken()) {
      // TODO: CHANGE TO authhttp IF NO MORE PROBLEMS
      return this.http.get(URL)
        .map((res: Response) => {
          const resp = res.json();
          console.log("REFRESH permissions", resp.permissions);
          this.permissionsService.addPermission(resp.permissions);
          this.setToken(resp.token);
          return res;
        })
        .toPromise()
        .then((data: any) => {
          console.log("in then refreshed perms", this.permissionsService.getPermissions());
          /*          this.authHttp.get(URL, this.headers)
                      .map((response: Response) => {
                        const res2 = response.json().data;
                        this.permissionsService.addPermission(res2);
                        return res2;
                      });*/
          console.log("in then refreshed perms2", this.permissionsService.getPermissions());

          /*this.getPermissionsFromAPI().subscribe(() => {
            console.log("in sub refresh perms", this.permissionsService.getPermissions());
          });*/
          //this.setToken(data.token);
          //this.getPermissionsFromAPI().subscribe();
        })
        .catch((err: any) => {
          this.logout();
          this.authNotifier.next(false);
          console.log("REFRESH ERROR", err);
          // TODO: COMMENTED OUT BECAUSE OF STARTUP ERROR
          // this.handleError(err);
          // TODO: UNCOMMENT IF ERROR PERSIST
          // Promise.resolve();
        });
    }
  }

  setRootPage(page: string) {
    this.rootPage = page;
  }

  getRootPage(): string {
    return this.rootPage;
  }

  decodeToken() {
    this.jwtHelper.decodeToken(this.token);
    console.log(this.jwtHelper.decodeToken(this.token));
  }

  checkIfUserHasCreatedAProject() {
    return this.jwtHelper.decodeToken(this.token).hasProject;
  }


  handleError(error: Response | any, self) {
    if ((!error.message && !error.status) || error.message === "No JWT present or has expired") {
      self.logout();
      return;
    }
    const errorsResponses = error.json().message || error.error || error.json().error || error.error.json() || error.json()
      || error || error.message || error.message.json() || "Server Error.";

    console.log(errorsResponses);

    return Observable.throw(errorsResponses);
  }
}
