// import { Component, OnInit, ViewChild } from '@angular/core';
// import { BsModalRef, BsModalService, TabsetComponent } from 'ngx-bootstrap';
// import { GetProjectsInterface, ProjectDataInterface } from '../../../interfaces/get-project.interface';
// import { BackendService } from '../../../services/backend.service';
// import { SubcontractorInterface, SubcontractorPaginatorInterface } from '../../../interfaces/subcontractor.interface';
// import { ActivatedRoute, Router } from '@angular/router';
// import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
// import { ScTradesPaginatorInterface } from '../../../interfaces/sctrades-paginator.interface';
// import { AlertService } from '../../../services/alerts/alert.service';
// import {
//   BidsInInterface, BidsOutInterface,
//   GetBidsByTradeInterface
// } from '../../../interfaces/get-bids-by-trade.interface';
// import { NewBidComponent } from '../new-bid/new-bid.component';
// import { AuthService } from '../../../services/auth/auth.service';
// import { GetScBidsPaginatorInterface } from '../../../interfaces/get-sc-bids-paginator.interface';
// import { GlobalConstants } from '../../../constants/global.constants';
// import { Observable } from 'rxjs/Observable';
//
//
// @Component({
//   selector: 'app-view-bids',
//   templateUrl: './view-bids.component.html',
//   styleUrls: ['./view-bids.component.css']
// })
// export class ViewBidsComponent implements OnInit {
//
//   @ViewChild('staticTabs') staticTabs: TabsetComponent;
//   bsModalRef: BsModalRef;
//
//
//   scs: ScTradesPaginatorInterface;
//   projects: GetProjectsInterface;
//   bidsIn: BidsInInterface;
//   acceptedBids: BidsInInterface | BidsOutInterface;
//   bids: GetScBidsPaginatorInterface;
//   bidsOut: BidsInInterface;
//   projectData: ProjectDataInterface;
//
//   storageURL: string;
//
//   isSc: boolean;
//   isBuilder: boolean;
//   maxSize = 5;
//   totalItems: number;
//   currentPage: number;
//   projectID: number;
//   bidType: string;
//   title: string;
//   showWhichTab;
//   viewingBidsForProject: boolean;
//
//   type;
//
//   constructor
//     (
//     private router: Router,
//     private route: ActivatedRoute,
//     private API: BackendService,
//     private alerts: AlertService,
//     private modalService: BsModalService,
//     private projectService: ProjectResolverService,
//     private auth: AuthService,
//     private constants: GlobalConstants,
//   ) {
//     this.type = this.route.snapshot.params.type;
//     this.showWhichTab = this.route.snapshot.queryParams;
//
//     // If you are viewing bids for project
//     if (this.type === 'for-project') {
//       this.projectID = route.snapshot.params.id;
//       this.viewingBidsForProject = true;
//       this.projectData = this.projectService.getProjectData();
//     }
//     // if viewing project bids get data
//     else if (this.type !== 'all') {
//       this.projectData = this.projectService.getProjectData();
//     }
//     if (this.auth.hasPermission('subcontractor')) {
//       this.bids = this.route.snapshot.data['SCOrProjectData'];
//       this.isBuilder = false;
//       this.isSc = true;
//       /*this.totalItems     = this.projects.last_page;
//       this.currentPage    = this.projects.current_page;*/
//     }
//     else if (this.auth.hasPermission('builder')) {
//       this.scs = this.route.snapshot.data['SCOrProjectData'];
//       this.isBuilder = true;
//       this.isSc = false;
//       this.totalItems = this.scs.last_page;
//       this.currentPage = this.scs.current_page;
//
//       /*if (this.scs.data[0]) {
//       this.title = this.scs.data[0].tradeName;
//     }*/
//
//       this.title = "all trades";
//     }
//     this.bidsIn = this.route.snapshot.data.bidData.bidsIn;
//     this.bidsOut = this.route.snapshot.data.bidData.bidsOut;
//     this.acceptedBids = this.route.snapshot.data.bidData.acceptedBids;
//
//     /*this.route.params.subscribe(param => {
//   this.projectID = param.id;
//   this.bidType   = param.type ? param.type : 'in';
// });*/
//
//     this.projectID = route.snapshot.params.id;
//
//
//     this.bidType = route.snapshot.params.type ? route.snapshot.params.type : 'in';
//
//   }
//
//   ngOnInit() {
//     this.storageURL = `${this.constants.STORAGEURL}/`;
//
//     if (this.showWhichTab.tab != null) {
//       debugger
//       const whichTab = this.showWhichTab.tab;
//
//       switch (whichTab) {
//         case "new-bbo":
//           this.selectTab(1);
//           break;
//         case "builder-accepted":
//           this.onChangeBidType('accepted');
//           break;
//       }
//
//       if (this.auth.hasPermission('builder')) {
//         window.history.pushState("", "", `/projects/${this.projectID}/bids`);
//       }
//       else {
//         window.history.pushState("", "", `/bids/all`);
//       }
//
//
//     }
//
//   }
//
//   selectTab(index: number) {
//     // Find out why it doesnt work without timer b/c of ng-template
//     const timer = Observable.timer(0.0001);
//     timer.subscribe(t => this.staticTabs.tabs[index].active = true);
//   }
//   getAllBidsForProject() {
//     this.API.getAllBidsByTrade(this.projectID, 0, 'project-all')
//       .subscribe(data => {
//         this.bidsIn = data.bidsIn;
//         this.bidsOut = data.bidsOut;
//         this.acceptedBids = data.acceptedBids;
//         this.title = "all trades";
//       },
//         (err) => { this.alerts.swalError('Error', err, true, 'Ok'); });
//   }
//
//   onPageChanged(event: any): void {
//     if (this.isBuilder) {
//       const URL = `${this.scs.path}?page=${event.page}`;
//       this.API.getPaginatorPage(URL)
//         .subscribe(data => {
//           this.scs = data;
//           this.totalItems = this.scs.last_page;
//           this.currentPage = this.scs.current_page;
//         },
//           (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
//     } else if (this.isSc) {
//       const URL = `${this.projects.path}?page=${event.page}`;
//       this.API.getPaginatorPage(URL)
//         .subscribe(data => {
//           this.projects = data;
//           this.totalItems = this.projects.last_page;
//           this.currentPage = this.projects.current_page;
//         },
//           (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
//     }
//   }
//
//   onBidPageChanged(event: any): void {
//     const URL = `${this.projects.path}?page=${event.page}`;
//     this.API.getPaginatorPage(URL)
//       .subscribe(data => {
//         this.bids = data;
//         this.totalItems = this.bids.last_page;
//         this.currentPage = this.bids.current_page;
//       },
//         (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
//   }
//
//   onChangeSCT(sct) {
//     this.title = sct.tradeName;
//     this.API.getAllBidsByTrade(this.projectID, sct.id, this.bidType)
//       .subscribe(data => {
//         this.bidsIn = data.bidsIn;
//         this.bidsOut = data.bidsOut;
//         this.acceptedBids = data.acceptedBids;
//       },
//         (err) => this.alerts.swalError('Error', err, true, 'Ok'));
//   }
//
//   onViewBidDetails(bid) {
//     this.API.getFilesAndProjectDetailsForBid(bid.id, bid.projectID)
//       .subscribe(data => {
//         this.projectService.setProjectData(data.projectInfo);
//         this.bsModalRef = this.modalService.show(NewBidComponent);
//         this.bsModalRef.content.bidForm = bid;
//         this.bsModalRef.content.filesArray = data.files;
//         this.bsModalRef.content.title = "View your bid form";
//         this.bsModalRef.content.formType = "viewBid";
//       });
//   }
//
//   onChangeBidType(type: string) {
//     if (this.viewingBidsForProject) {
//       this.API.getSCBidType(type, this.projectID).subscribe(data => this.bids = data);
//     }
//     else {
//       this.API.getSCBidType(type).subscribe(data => this.bids = data);
//     }
//   }
//
//   /******************************************
//               SUBCONTRACTORS
//    ******************************************/
//
//
//   onGotoProject(project: ProjectDataInterface) {
//     this.projectService.setProjectData(project);
//   }
//
// }
