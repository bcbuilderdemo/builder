// import { Component, OnInit } from '@angular/core';
// import {ActivatedRoute, Router} from '@angular/router';
// import {ViewAllMessagesInterface} from '../../../interfaces/view-all-messages.interface';
// import {GlobalConstants} from '../../../constants/global.constants';
// import {BackendService} from '../../../services/backend.service';
// import {AlertService} from '../../../services/alerts/alert.service';
//
// @Component({
//   selector: 'app-view-messages',
//   templateUrl: './view-messages.component.html',
//   styleUrls: ['./view-messages.component.css']
// })
//
//
// export class ViewMessagesComponent implements OnInit {
//
//   msgs:       ViewAllMessagesInterface;
//   totalItems: number;
//   currentPage: number;
//   storageURL: string;
//   maxSize = 10;
//
//   constructor
//   (
//     private route: ActivatedRoute,
//     private constants: GlobalConstants,
//     private router: Router,
//     private API: BackendService,
//     private alerts: AlertService
//   )
//   {
//
//   }
//
//   ngOnInit() {
//
//     this.msgs         = this.route.snapshot.data.data.data;
//     this.storageURL   = this.constants.STORAGEURL;
//
//   }
//
//
//
//   onClickMessage(msg) {
//     this.router.navigate([`/projects/${msg.projectID}/chat/${msg.uuid}`]);
//   }
//
//   // TODO: NOT YET TESTED
//   onPageChanged(event: any): void {
//     const URL = `${this.msgs.path}?page=${event.page}`;
//     this.API.getPaginatorPage(URL)
//       .subscribe(data =>
//         {
//           this.msgs = data;
//           this.totalItems = this.msgs.last_page;
//           this.currentPage = this.msgs.current_page;
//         },
//         (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
//   }
//
// }
