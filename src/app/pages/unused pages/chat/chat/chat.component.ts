// import {Component, ElementRef, OnChanges, OnInit, ViewChild} from '@angular/core';
// import io from 'socket.io-client';
// import {ChatPaginatorInterface} from '../../../interfaces/chatpaginator.interface';
// import {ActivatedRoute} from '@angular/router';
// import Echo from 'laravel-echo';
// import {AuthService} from '../../../services/auth/auth.service';
// import {GlobalConstants} from '../../../constants/global.constants';
// import {BackendService} from '../../../services/backend.service';
// import {FormControl, FormGroup, Validators} from '@angular/forms';
// import {AlertService} from '../../../services/alerts/alert.service';
// import swal from "sweetalert2";
// import {ProfileUploadsInterface} from '../../../interfaces/profile-info.interface';
//
// @Component({
//   selector: 'app-chat',
//   templateUrl: './chat.component.html',
//   styleUrls: ['./chat.component.css']
// })
// export class ChatComponent implements OnInit, OnChanges
// {
//
//   @ViewChild('uploadInput')  uploadInput:    ElementRef;
//   @ViewChild('chatScroll')   chatScroll:     ElementRef;
//   loading: boolean;
//
//   readonly CHAT_MAX_LENGTH = 500;
//
//   messages:     ChatPaginatorInterface | any;
//   uploads:      ProfileUploadsInterface | any;
//   storageURL:   string;
//
//   chatUUID:         any;
//   projectID:        any;
//   submitAttempt:    boolean;
//
//   newMessageForm: FormGroup;
//
//   constructor
//   (
//     private route: ActivatedRoute,
//     private auth: AuthService,
//     private constants: GlobalConstants,
//     private API: BackendService,
//     private alerts: AlertService,
//   )
//   {
//
//     this.newMessageForm = new FormGroup({
//       'message': new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(250)])),
//     });
//
//   }
//
//   ngOnInit()
//   {
//
//     this.storageURL = `${this.constants.STORAGEURL}/`;
//
//     this.messages     = this.route.snapshot.data.data.chat;
//     this.uploads      = this.route.snapshot.data.data.uploads;
//     this.chatUUID     = this.route.snapshot.params.uuid;
//     this.projectID    = this.route.snapshot.params.id;
//
//     if (this.messages.data[0].message) {
//       this.sortMessages();
//       this.scrollToBottom();
//       this.markMessagesAsRead();
//     }
//
//     if (this.constants.isLocal) {
//       window['Echo'] = new Echo({
//         broadcaster: 'socket.io',
//         host: 'http://bcbuilder.test:6001',
//         auth:
//           {
//             headers:
//               {
//                 'Authorization': 'Bearer ' + this.auth.token
//               }
//           }
//       });
//     } else {
//       window['Echo'] = new Echo({
//         broadcaster: 'socket.io',
//         host: 'http://52.60.104.219:6001',
//         auth:
//           {
//             headers:
//               {
//                 'Authorization': 'Bearer ' + this.auth.token
//               }
//           }
//       });
//     }
//
//       /*window['Echo'] = new Echo({
//         broadcaster: 'socket.io',
//         host: 'http://bcbuilder.test:6001',
//         auth:
//         {
//           headers:
//           {
//             'Authorization': 'Bearer ' + this.auth.token
//           }
//         }
// 		});*/
//
//
//     window['Echo'].private(`chat-${this.chatUUID}`)
//       .listen(".new.message", (data) => {
//         console.log("e", data);
//         this.messages.data.push(data.message);
//         this.scrollToBottom();
//       });
//
//   }
//
//   onScrolledDown() {
//     console.log("down");
//   }
//
//   onScrolledUp() {
//     const height = this.chatScroll.nativeElement.scrollHeight;
//     console.log("height", height);
//     if (this.messages.next_page_url) {
//       this.loading = true;
//       this.API.getPaginatorPageNEW(this.messages.next_page_url).subscribe(data => {
//         let msgs = data.chat.data;
//         msgs = msgs.concat(msgs);
//         this.messages = data.chat;
//         this.messages.data = msgs;
//         this.messages  = data.chat;
//         this.loading = false;
//       },
//         (err => this.alerts.swalError(err, '', true, 'Ok')));
//     }
//   }
//
//   scrollToBottom(): void {
//     /*try {
//       this.chatScroll.nativeElement.scrollTop = this.chatScroll.nativeElement.scrollHeight;
//     } catch (err) {
//       console.log("scroll error", err);
//     }*/
//     setTimeout(() => { this.chatScroll.nativeElement.scrollTop = this.chatScroll.nativeElement.scrollHeight; }, 0);
//   }
//
//   onScroll (event) {
//     console.log(event);
//   }
//
//   onSendMessage(input) {
//     this.submitAttempt = false;
//     if (this.newMessageForm.invalid) {
//       this.submitAttempt = true;
//       return false;
//     }
//
//     this.API.sendMessage(input, this.projectID, this.chatUUID).subscribe(msg => {
//         this.newMessageForm.reset();
//       console.log(msg);
//     },
//       (err) => this.alerts.swalError(err, '', true, 'Ok'));
//   }
//   ngOnChanges()
//   {
//
//   }
//
//   onChooseFile(event) {
//
//     const file                            = event.target.files[0];
//     // const mime                         = file.type;
//     this.uploadInput.nativeElement.value  = "";
//
//     if ((file.size / 1024) > 100000) {
//       this.alerts.swalError("Image size cannot exceed 100 MB", '', true, 'Ok');
//       return false;
//     }
//
//     swal({
//       title: `Are you sure you want to upload ${file.name}?`,
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg',
//     }).then(() => {
//         const fd = new FormData();
//         fd.append('file', file);
//         this.API.uploadFileForChat(fd, this.projectID, this.chatUUID)
//           .subscribe(data =>
//             {
//               this.getMessagesAndUploads();
//               this.alerts.swalSuccess(data, '', false, '', 1500);
//             },
//             (err) => this.alerts.swalError(err, '', true, 'Ok')
//           );
//       },
//       (dismiss) =>
//       {
//         if (dismiss === 'cancel') {
//         }
//       });
//   }
//
//   getMessagesAndUploads() {
//     this.API.getChat(this.projectID, this.chatUUID)
//       .subscribe(data => {
//           this.messages     = data.chat;
//           this.uploads      = data.uploads;
//           this.sortMessages();
//           this.scrollToBottom();
//       });
//   }
//
//   onOpenFile(fileUUID) {
//     window.open(this.API.getUploadForChat(this.projectID, this.chatUUID, fileUUID));
//   }
//
//   sortMessages() {
//     this.messages.data.sort((a, b) => a.id > b.id);
//   }
//
//   markMessagesAsRead() {
//     this.API.markMessagesAsRead(this.projectID, this.chatUUID).subscribe();
//   }
//
// }
