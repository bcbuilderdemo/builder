// import { Component, OnInit } from '@angular/core';
// import {BackendService} from '../../../services/backend.service';
// import {SupplierPaginatorInterface} from '../../../interfaces/supplier.interface';
// import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
// import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
// import {ActivatedRoute} from '@angular/router';
// import {GetHBGInterface} from '../../../interfaces/get-hbg.interface';
// import {GlobalConstants} from '../../../constants/global.constants';
//
// @Component({
//   selector: 'app-supplier',
//   templateUrl: './supplier.component.html',
//   styleUrls: ['./supplier.component.css']
// })
// export class SupplierComponent implements OnInit {
//
//   scs:                SupplierPaginatorInterface;
//   hbg:                GetHBGInterface;
//   projectData:        ProjectDataInterface;
//   storageURL:         string;
//
//   constructor
//   (
//     private API: BackendService,
//     private projectService: ProjectResolverService,
//     private route: ActivatedRoute,
//     private constants: GlobalConstants
//   )
//   {
//     this.hbg = this.route.snapshot.data.hbg;
//     this.scs = this.route.snapshot.data.scs;
//
//     console.log(this.hbg);
//     console.log(this.scs);
//   }
//
//   ngOnInit() {
//     this.storageURL   = `${this.constants.STORAGEURL}/`;
//     this.projectData  = this.projectService.getProjectData();
//     //this.API.getAllSubContractors().subscribe((data) => this.scs = data);
//   }
//
// }
