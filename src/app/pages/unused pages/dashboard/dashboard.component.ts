// import {Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
// import {ModalComponent} from '../../templates/modal/modal.component';
// import {NewProjectComponent} from '../project-pages/new-project/new-project.component';
// import {BsModalRef, BsModalService} from 'ngx-bootstrap';
// import {BackendService} from '../../services/backend.service';
// import {GetProjectsInterface, ProjectDataInterface} from '../../interfaces/get-project.interface';
// import {SessionStorageService} from 'ngx-webstorage';
// import {ProjectResolverService} from '../../services/resolvers/project-resolver/project-resolver.service';
// import {AlertService} from '../../services/alerts/alert.service';
// import {AuthService} from '../../services/auth/auth.service';
// import {ActivatedRoute} from '@angular/router';
// import {UserAndCompanyInfo} from '../../interfaces/user-and-company-info';
// import {GlobalConstants} from '../../constants/global.constants';
// import {NotificationsInterface} from '../../interfaces/notifications.interface';
// import {ProfileInfoInterface, ProfileUploadsInterface} from '../../interfaces/profile-info.interface';
// import swal from "sweetalert2";
// import {forEach} from '@angular/router/src/utils/collection';
//
// import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
// import { ViewEncapsulation } from '@angular/core';
//
//
//
// const now = new Date();
//
// @Component({
//   selector: 'app-dashboard',
//   templateUrl: './dashboard.component.html',
//   styleUrls: ['./dashboard.component.css'],
// })
// export class dashboardComponent implements OnInit {
//
//   //Nadim added this from the original project feb 16
//
//    // @ViewChild('childModal') childModal: ModalComponent;
//   // @ViewChild('newProject') newProject: NewProjectComponent;
//   bsModalRef:   BsModalRef;
//   modalRef:     BsModalRef;
//   projects:     GetProjectsInterface;
//   // userInfo:     UserAndCompanyInfo;
//   nots:         NotificationsInterface | any;
//   hasNots       = false;
//   hasProjects   = false;
//   notKeys;
//   maxSize       = 5;
//   totalItems:   number;
//   currentPage:  number;
//   storageURL: string;
//
//   profileInfo:        ProfileInfoInterface;
//   logo:               ProfileUploadsInterface;
//   image:              ProfileUploadsInterface;
//   profileUploads =    [] as ProfileUploadsInterface[];
//   oldFile:            ProfileUploadsInterface;
//
//   @ViewChild('imageInput') imageInput: ElementRef;
//
//
//
//
//   constructor(  private viewContainerRef: ViewContainerRef,
//     private modalService: BsModalService,
//     private API: BackendService,
//     private alerts: AlertService,
//     private projectService: ProjectResolverService,
//     private auth: AuthService,
//     private route: ActivatedRoute,
//     private constants: GlobalConstants,
// ) {
//
//   }
//   singleSelectOptions: any = [
//     {
//         label: 'Angular',
//         value: 'angular',
//         code: 'NG'
//     }, {
//         label: 'ReactJS',
//         value: 'reactjs',
//         code: 'RJS'
//     }, {
//         label: 'Ember JS',
//         value: 'emberjs',
//         code: 'emjs'
//     }, {
//         label: 'Ruby on Rails',
//         value: 'ruby_on_rails',
//         code: 'ROR'
//     }
// ];
//
// singleSelectConfig: any = {
//     labelField: 'label',
//     valueField: 'value',
//     searchField: ['label']
// };
//
// singleSelectValue: string[] = ['reactjs'];
//
// multiSelectOptions: any = [
//     {
//         label: 'Angular',
//         value: 'angular',
//         code: 'NG'
//     }, {
//         label: 'ReactJS',
//         value: 'reactjs',
//         code: 'RJS'
//     }, {
//         label: 'Ember JS',
//         value: 'emberjs',
//         code: 'emjs'
//     }, {
//         label: 'Ruby on Rails',
//         value: 'ruby_on_rails',
//         code: 'ROR'
//     }
// ];
//
// multiSelectConfig: any = {
//     labelField: 'label',
//     valueField: 'value',
//     searchField: ['label'],
//     maxItems: 4
// };
//
// multiSelectValue: string[] = ['reactjs'];
// multiSelectValue2: string[] = ['reactjs', 'angular'];
//
//
// optGroupConfig: any = {
//     maxItems: 1,
//     optgroupField: 'class',
//       labelField: 'name',
//       searchField: ['name'],
//       render: {
//         optgroup_header: function(data, escape) {
//             return '<div class="optgroup-header">' +
//                     escape(data.label) +
//                     ' <span class="scientific">' +
//                     escape(data.label_scientific) +
//                     '</span></div>';
//         }
//     },
//     optgroups: [
//         {value: 'mammal', label: 'Mammal', label_scientific: 'Mammalia'},
//         {value: 'bird', label: 'Bird', label_scientific: 'Aves'},
//         {value: 'reptile', label: 'Reptile', label_scientific: 'Reptilia'}
//     ]
// };
//
// optGroupOptions: any = [
//     {class: 'mammal', value: 'dog', name: 'Dog' },
//     {class: 'mammal', value: 'cat', name: 'Cat' },
//     {class: 'mammal', value: 'horse', name: 'Horse' },
//     {class: 'mammal', value: 'kangaroo', name: 'Kangaroo' },
//     {class: 'bird', value: 'duck', name: 'Duck'},
//     {class: 'bird', value: 'chicken', name: 'Chicken'},
//     {class: 'bird', value: 'ostrich', name: 'Ostrich'},
//     {class: 'bird', value: 'seagull', name: 'Seagull'},
//     {class: 'reptile', value: 'snake', name: 'Snake'},
//     {class: 'reptile', value: 'lizard', name: 'Lizard'},
//     {class: 'reptile', value: 'alligator', name: 'Alligator'},
//     {class: 'reptile', value: 'turtle', name: 'Turtle'}
// ];
//
// optGroupValue: string[] = ['dog'];
//
//
// model;
// model2;
// model3;
//
// date: {year: number, month: number};
// displayMonths = 2;
//
// selectToday() {
//     this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
// }
//
//   ngOnInit(){
//
//         this.storageURL = `${this.constants.STORAGEURL}/`;
//
//         this.projects     = this.route.snapshot.data.data.projectInfo;
//         // this.userInfo     = this.route.snapshot.data.data.userInfo;
//         this.profileInfo  = this.route.snapshot.data.profile.profileInfo;
//         const uploads     = this.route.snapshot.data.profile.profileUploads;
//         this.nots         = this.route.snapshot.data.nots.nots;
//         this.totalItems   = this.projects.last_page;
//         this.currentPage  = this.projects.current_page;
//
//         for (const upload of uploads) {
//           if (!upload.isLogo)  { this.profileUploads.push(upload); }
//           else                 { this.logo = upload; }
//         }
//
//         this.hasNots      = this.nots;
//         this.hasProjects  = this.projects.total > 0;
//         console.log("prjects", this.projects);
//
//         /*if (this.nots) {
//           this.hasNots = true;
//
//         }
//         else {
//           this.hasNots = false;
//         }*/
//
//        if (this.auth.hasPermission('builder')) {
//          if ( ! this.auth.checkIfUserHasCreatedAProject()) {
//            this.bsModalRef = this.modalService.show(NewProjectComponent, {class: 'modal-dialog'});
//          }
//        }
//
//
//        console.log(this.projects);
//
//
//
//   }
//
//   openNewProjectModal() {
//     this.bsModalRef = this.modalService.show(NewProjectComponent, {class: 'modal-dialog'});
//     // this.NewProjectRef.showModal();
//   }
//   onGotoProject(project: ProjectDataInterface) {
//     console.log('onGottoProject');
//     this.projectService.setProjectData(project);
//   }
//
//   onPageChanged(event: any): void {
//     const URL = `${this.projects.path}?page=${event.page}`;
//     this.API.getPaginatorPage(URL)
//       .subscribe(data => {
//           this.projects       = data;
//           this.totalItems     = this.projects.last_page;
//           this.currentPage    = this.projects.current_page;
//         },
//         (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
//   }
//
//   onViewImage(template: TemplateRef<any>, file: ProfileUploadsInterface) {
//       this.image = file;
//       this.modalRef = this.modalService.show(template);
//   }
//
//   onEditImage(file) {
//     this.imageInput.nativeElement.click();
//     this.oldFile = file;
//   }
//
//   uploadImage(event) {
//
//     const image         = event.target.files[0];
//     const mime          = image.type;
//     const type          = mime.split('/');
//     const validFileExts  = ["jpg", "jpeg", "png", "svg", "bmp"];
//     this.imageInput.nativeElement.value = "";
//
//     if (type[0] !== 'image') {
//       this.alerts.swalError("You can only upload images", '', true, 'Ok');
//       // this.imageInput.nativeElement.value = "";
//       return false;
//     }
//     else if ( ! validFileExts.includes(type[1])) {
//       this.alerts.swalError("You can only upload the following formats", '.JPEG, .JPG, .PNG, .SVG, .BMP', true, 'Ok');
//       // this.imageInput.nativeElement.value = "";
//       return false;
//     }
//     else if ((image.size / 1024) > 10240) {
//       this.alerts.swalError("Image size cannot exceed 10 MB", '', true, 'Ok');
//       // this.imageInput.nativeElement.value = "";
//       return false;
//     }
//
//     const fd = new FormData();
//     fd.append('image', image);
//     fd.append('fileID', this.oldFile.fileID);
//     fd.append('profileID', this.oldFile.profileID);
//
//     this.API.updateProfileImage(fd)
//       .subscribe(data =>
//         {
//           const upload = data.profileUploads;
//           if ( ! this.oldFile.isLogo) {
//             const index = this.profileUploads.findIndex(f => f.fileID === upload.fileID);
//             this.profileUploads[index] = upload;
//           } else {
//             this.logo = upload;
//           }
//
//           this.alerts.swalSuccess(data.message, '', false, '', 2500);
//         },
//         (err) => this.alerts.swalError(err, '', true, 'Ok')
//       );
//   }
//
//   onDeleteImage(file: ProfileUploadsInterface) {
//     const fd = new FormData();
//     fd.append('fileID', file.fileID);
//     fd.append('profileID', file.profileID);
//
//     swal({
//       title: 'Are you sure you want to delete this image?',
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg',
//     }).then(() => {
//         this.API.deleteProfileImage(fd)
//           .subscribe(data =>
//             {
//               const upload = data.profileUploads;
//               const index = this.profileUploads.findIndex(f => f.fileID === upload.fileID);
//               this.profileUploads[index] = upload;
//               this.alerts.swalSuccess(data.message, '', false, '', 2500);
//             },
//             (err) => this.alerts.swalError(err, '', true, 'Ok')
//           );
//       },
//       (dismiss) =>
//       {
//         if (dismiss === 'cancel') {
//           console.log("cancel");
//         }
//       });
//
//   }
//
//
//   openURL(URL: string) {
//     if (URL) { window.open(URL); }
//   }
// }
