import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewComponent } from './bids_view_hbg.component';

describe('BidsViewHBGComponent', () => {
  let component: ViewComponent;
  let fixture: ComponentFixture<BidsViewHBGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidsViewHBGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidsViewHBGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
