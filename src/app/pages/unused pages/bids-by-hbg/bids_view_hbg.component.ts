// import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
// import { SubcontractorPaginatorInterface, SubcontractorInterface } from '../../../interfaces/subcontractor.interface';
// import { BidFormInterface, FilesInterface, GetBidFormInterface } from '../../../interfaces/get-bid-form-interface';
// import { BackendService } from '../../../services/backend.service';
// import { AlertService } from '../../../services/alerts/alert.service';
// import { GetBidsBySubproject } from '../../../interfaces/get-bids-by-subproject.interface';
// import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
// import { ProfileResolverService } from '../../../services/resolvers/profile-resolver/profile-resolver.service';
// import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
// import { BidsBySubprojectResolverService } from '../../../services/resolvers/get-bids-by-subproject-resolver/get-bids-by-subproject-resolver.service';
// import { GlobalConstants } from '../../../constants/global.constants';
// import { GetHBGInterface } from '../../../interfaces/get-hbg.interface';
//
// @Component({
//   selector: 'app-view',
//   templateUrl: './bids_view_hbg.component.html',
//   styleUrls: ['./bids_view_hbg.component.css']
// })
// export class BidsViewHBGComponent implements OnInit {
//   hbg: GetHBGInterface;
//   projectID: number;
//   subprojectID: number;
//   projectData: ProjectDataInterface;
//   gbbs: GetBidsBySubproject;
//   storageURL: string;
//
//   constructor
//     (
//     private route: ActivatedRoute,
//     private projectService: ProjectResolverService,
//     private constants: GlobalConstants,
//
//   ) {
//     this.projectID = this.route.snapshot.params.id;
//     this.subprojectID = this.route.snapshot.params.subprojectID;
//     this.hbg = this.route.snapshot.data.hbg;
//     this.gbbs = this.route.snapshot.data.gbbs;
//   }
//
//   ngOnInit() {
//     this.projectData = this.projectService.getProjectData();
//     this.storageURL = `${this.constants.STORAGEURL}/`;
//   }
//
//   onGotoWebsite(URL) {
//     if (URL) {
//       window.open(URL);
//     }
//   }
// }
