// import { Component, OnInit } from '@angular/core';
// import {ActivatedRoute, Route, Router} from '@angular/router';
// import {BBOAndProjectData} from '../../../interfaces/get-bbo-resolver-data.interface';
// import {FilesInterface} from '../../../interfaces/files.interface';
// import {AuthService} from '../../../services/auth/auth.service';
// import {BackendService} from '../../../services/backend.service';
// import {SubcontractorInterface} from '../../../interfaces/subcontractor.interface';
// import swal from "sweetalert2";
// import {FormControl, FormGroup, Validators} from '@angular/forms';
// import {NewBidInterface} from '../../../interfaces/new-bid.interface';
// import {AlertService} from '../../../services/alerts/alert.service';
// import {forEach} from '@angular/router/src/utils/collection';
//
// @Component({
//   selector: 'app-view-bid',
//   templateUrl: './view-bid.component.html',
//   styleUrls: ['./view-bid.component.css']
// })
// export class ViewBidComponent implements OnInit {
//
//   bboData:                BBOAndProjectData;
//   files:                  FilesInterface;
//   sctInfo:                SubcontractorInterface;
//   newBidForm:             FormGroup;
//   filesToUpload:          Array<File> = [];
//
//   viewing     =     false;
//   editing     =     false;
//   newBid      =     false;
//   viewingBBO  =     false;
//
//   type:           string;
//   bidID:          string;
//   bboID:          string;
//   projectID:      string;
//   sctID:          any;
//
//
//   bidsLineItems: any;
//   bid: any;
//   subprojectID: any;
//
//
//   date =          new Date();
//
//
//   constructor
//   (
//     private route: ActivatedRoute,
//     private auth: AuthService,
//     private API: BackendService,
//     private alerts: AlertService,
//     private router: Router,
//   )
//   {
//     const snapshot        = route.snapshot;
//     const path            = snapshot.routeConfig.path;
//     this.projectID        = snapshot.params.id;
//     this.type             = snapshot.params.type;
//     // this.subprojectID = snapshot.params.subproject_id;
//     // this.bid = snapshot.data.bidInfo.bid || {
//     //   id: 143890574,
//     //   comments: null,
//     //   amount: null
//     // };
//
//     if (this.auth.hasPermission('subcontractor')) {
//       this.bboData      = this.route.snapshot.data.data.bbo;
//       this.files        = this.route.snapshot.data.data.uploads;
//       this.bboID        = snapshot.params.bboID;
//     }
//     else if (this.auth.hasPermission('builder')) {
//       // View Bid Out to SC
//       if (path === "bids/:bidID/:type/:bboID") {
//         if (this.type === 'view') {
//           this.viewingBBO       = true;
//           this.bidID            = snapshot.params.bidID;
//           this.bboID            = snapshot.params.bboID;
//           this.bboData          = snapshot.data.data.bbo;
//           this.files            = snapshot.data.data.uploads;
//
//         }
//       }
//       // View BBO for trade Or create new bid
//       else if (path === 'bid/:type/:sctID') {
//         this.sctID            = snapshot.params.sctID;
//         if (this.type === 'create') {
//           this.newBidForm = new FormGroup({
//             'additionalDetails': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(250)])),
//
//           });
//           this.newBid       = true;
//           this.bboData      = snapshot.data.data.projectInfo;
//           this.sctInfo      =  snapshot.data.data.sctInfo;
//         }
//         else if (this.type === 'view') {
//           this.viewing          = true;
//           this.bboData          = snapshot.data.data.projectInfo;
//           this.files            = snapshot.data.data.uploads;
//           this.sctInfo          = snapshot.data.data.sctInfo;
//           this.files            = snapshot.data.data.files;
//         }
//       }
//     }
//
//     this.markAsRead();
//   }
//
//   ngOnInit() {
//   }
//
//   markAsRead() {
//     if (this.auth.hasPermission('subcontractor')) {
//       if (this.type === 'view' && this.bboData.bbo_sc_viewed !== 1) {
//         this.API.markBBOAsReadBySC(this.projectID, this.bidID, this.bboID).subscribe();
//       }
//     }
//   }
//
//   onViewFile(file: FilesInterface) {
//     if (this.auth.hasPermission('builder')) {
//       window.open(this.API.getFile(file.fileID));
//     }
//     else if (this.auth.hasPermission('subcontractor')) {
//       window.open(this.API.getFile(file.fileID, file.bidID));
//     }
//   }
//
//
//   /******************************************************************
//                                   NEW BID
//   ********************************************************************/
//
//   fileSelected(event) {
//     const i                 = this.filesToUpload.length;
//     const file              = event.target.files[0];
//     const exists            = this.filesToUpload.find(f => f.name === file.name);
//     if (exists) {
//       this.alerts.swalError(`You have already selected ${file.name}`, '', true, 'Ok');
//     }
//     else {
//       this.filesToUpload[i] = file;
//     }
//   }
//
//   deleteFile(index, fileName)
//   {
//     // this.alerts.swalConfirm(`Are you sure you want to delete ${fileName}?`, '', true, 'Yes', true, 'Cancel');
//     swal({
//       title: 'Are you sure you want to delete ' + fileName,
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg',
//     }).then(() => {
//         this.filesToUpload.splice(index, 1);
//       },
//       (dismiss) =>
//       {
//         // dismiss can be 'cancel', 'overlay',
//         // 'close', and 'timer'
//         if (dismiss === 'cancel') {
//           console.log("cancel");
//         }
//       });
//   }
//
//   onSubmit(input: NewBidInterface)
//   {
//     const files     = [];
//     const formData  = new FormData();
//     formData.append('additionalDetails', input.additionalDetails);
//     for (let i = 0; i < this.filesToUpload.length; i++) {
//       formData.append("files[]", this.filesToUpload[i], this.filesToUpload[i]['name']);
//     }
//     // let params = {
//     //   bid: this.bid,
//     //   bids_line_items: this.bidsLineItems,
//     //   project: this.projectData,
//     //   subproject_id: this.subprojectID
//     // }
//     //
//     // this.API.setBidsLineItems(params).subscribe(() => {
//     //   this.alerts.swalSuccess(data, '', false, '', 3000, true, `projects/${this.projectID}/subcontractors/view/${this.sctID}`, false);
//     // });
//
//     this.API.newBid(formData, this.projectID, this.sctID).subscribe(data =>
//     {
//       this.alerts.swalSuccess(data, '', false, '', 3000, true, `projects/${this.projectID}/subcontractors/view/${this.sctID}`, false);
//       //this.router.navigate(['projects', this.projectID, 'subcontractors', 'view', this.sctID]);
//     }, (err) =>
//     {
//       this.alerts.swalError(err, '', true, 'Ok', null, false);
//
//     });
//   }
//
// }
