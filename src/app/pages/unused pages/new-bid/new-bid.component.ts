// import {Component, OnInit, ViewChild} from '@angular/core';
// import {FormControl, FormGroup, Validators} from '@angular/forms';
// import {BsModalRef, BsModalService} from 'ngx-bootstrap';
// import {BackendService} from '../../../services/backend.service';
// import {NewBidInterface} from '../../../interfaces/new-bid.interface';
// import {ModalComponent} from '../../../templates/modal/modal.component';
// import { ModalDirective } from 'ngx-bootstrap/modal';
// import {AlertService} from '../../../services/alerts/alert.service';
// import {ActivatedRoute, Router} from '@angular/router';
// import {SessionStorageService} from 'ngx-webstorage';
// import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
// import swal from 'sweetalert2';
// import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
// import {FilesInterface} from '../../../interfaces/files.interface';
// import {AuthService} from '../../../services/auth/auth.service';
//
//
// @Component({
//   selector: 'app-new-bid',
//   templateUrl: './new-bid.component.html',
//   styleUrls: ['./new-bid.component.css']
// })
//
// export class NewBidComponent implements OnInit {
//   NewProjectComponent: any;
//
//   @ViewChild('childModal') childModal: ModalComponent;
//   @ViewChild(ModalDirective) public newBidModal: ModalDirective;
//
//   newBidForm: FormGroup;
//
//   message:      string;
//   project:      ProjectDataInterface;
//
//   bidForm:      any;
//   scTradeID:    any;
//   formType:     any;
//   title:        any;
//
//   files: Array<File> = [];
//   filesArray = [];
//
//   currentNumUploads     = 0;
//   readonly MAX_UPLOADS  = 10;
//
//   constructor
//   (
//     private API: BackendService,
//     public bsModalRef: BsModalRef,
//     private modalService: BsModalService,
//     private alerts: AlertService,
//     private route: ActivatedRoute,
//     private session: SessionStorageService,
//     private projectService: ProjectResolverService,
//     private auth: AuthService,
//     private router: Router,
//
//   ) {
//
//     this.project = this.session.retrieve('project');
//
//     console.log("f2", this.filesArray);
//
//   }
//
//       ngOnInit()
//       {
//         this.newBidForm = new FormGroup({
//           'additionalDetails': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(250)])),
//           'scTradeID':         new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
//           'formType':          new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
//           'bidID':             new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
//           'projectID':         new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
//         });
//       }
//
//   fileSelected(event) {
//     const i          = this.files.length;
//     this.files[i]    = event.target.files[0];
//     this.setTotalUploads();
//
//     console.log(this.files);
//   }
//
//   setTotalUploads() {
//     this.currentNumUploads = this.filesArray.length + this.files.length;
//     console.log(this.currentNumUploads);
//     console.log("FA L", this.filesArray.length);
//     console.log("F L", this.files.length);
//   }
//
//
//   deleteFile(index, fileName)
//   {
//     // this.alerts.swalConfirm(`Are you sure you want to delete ${fileName}?`, '', true, 'Yes', true, 'Cancel');
//     swal({
//       title: 'Are you sure you want to delete ' + fileName,
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg',
//     }).then(() => {
//       this.files.splice(index, 1);
//       this.setTotalUploads();
//
//       },
//       (dismiss) =>
//     {
//       // dismiss can be 'cancel', 'overlay',
//       // 'close', and 'timer'
//       if (dismiss === 'cancel') {
//         console.log("cancel");
//       }
//     });
//   }
//
//   editFileSelected(event, fileArray) {
//
//     this.filesArray                = fileArray;
//     const filesLength              = this.files.length;
//     this.files[filesLength]        = event.target.files[0];
//     this.filesArray.push({
//       fileName: event.target.files[0].name
//     });
//     this.setTotalUploads();
//   }
//
//   deleteUploadedFile(index, fileID, fileName, filesArray) {
//     this.filesArray = filesArray;
//     swal({
//       title: 'Are you sure you want to delete ' + fileName,
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg',
//     }).then(() => {
//         this.API.deleteFile(this.project.id, this.newBidForm.value.bidID, this.newBidForm.value.scTradeID, fileID)
//           .subscribe(data => {
//             this.alerts.swalSuccess(data, '', false, '', 2500, false, '', false);
//             this.filesArray.splice(index, 1);
//             this.setTotalUploads();
//           }, (err => {
//             this.alerts.swalError("An error has occurred", "Please try again", true, 'Ok', null, false);
//           }));
//       },
//       (dismiss) =>
//       {
//         // dismiss can be 'cancel', 'overlay',
//         // 'close', and 'timer'
//         if (dismiss === 'cancel') {
//           console.log("cancel");
//         }
//       });
//
//   }
//
//
//
//   onSubmit(input: NewBidInterface) {
//     const formData = new FormData();
//     formData.append('additionalDetails', input.additionalDetails);
//     for (let i = 0; i < this.files.length; i++) {
//       formData.append("files[]", this.files[i], this.files[i]['name']);
//       // formData.append("files[]", this.files[i], this.files[i]['name']);
//     }
//     this.API.newBid(formData, this.project.id, this.newBidForm.value.scTradeID).subscribe(data =>
//     {
//       this.bsModalRef.hide();
//       this.alerts.swalSuccess(data, '', false, '', 3000, false, null, true);
//       // this.API.getProjectInfo(this.project.id).subscribe(pData => this.project = pData);
//     }, (err) =>
//     {
//       this.alerts.swalError("An error has occurred", "Please try again", true, 'Ok', null, false);
//
//     });
//     /*if (this.newBidForm.value.formType === "newBid") {
//       this.API.newBid(formData, this.project.id, this.newBidForm.value.scTradeID).subscribe(data =>
//       {
//         this.bsModalRef.hide();
//         this.alerts.swalSuccess(data, '', false, '', 2500, true, `projects/${this.project.id}/subcontractors/view/${this.newBidForm.value.scTradeID}`, true);
//       }, (err) =>
//       {
//         this.alerts.swalError("An error has occurred", "Please try again", true, 'Ok', null, false);
//
//       });
//     } else {
//       this.API.editBid(formData, this.project.id, this.newBidForm.value.scTradeID).subscribe(data =>
//       {
//         this.bsModalRef.hide();
//         this.alerts.swalSuccess(data, '', false, '', 2500, true, `projects/${this.project.id}/subcontractors/view/${this.newBidForm.value.scTradeID}`, true);
//       }, (err) => this.alerts.swalError("An error has occurred", "Please try again", true, 'Ok', null, false));
//     }*/
//   }
//
//   onViewFile(file: FilesInterface) {
//     if (this.auth.hasPermission('builder')) {
//       window.open(this.API.getFile(file.id));
//     }
//     else if (this.auth.hasPermission('subcontractor')) {
//       window.open(this.API.getFile(file.id, file.bidID));
//     }
//   }
// }
