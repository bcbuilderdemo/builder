// import {Component, Input, OnInit, TemplateRef} from '@angular/core';
// import {FormControl, FormGroup, Validators} from '@angular/forms';
// import {LineDataInterface, ScBidFormInterface} from '../../../interfaces/sc-bid-form';
// import {BsModalRef, BsModalService} from 'ngx-bootstrap';
// import 'rxjs/add/operator/debounceTime';
// import 'rxjs/add/operator/filter';
// import {BackendService} from '../../../services/backend.service';
// import {ActivatedRoute} from '@angular/router';
// import {AlertService} from '../../../services/alerts/alert.service';
// import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
// import {AuthService} from '../../../services/auth/auth.service';
// import swal from 'sweetalert2';
// import {NgxPermissionsService} from 'ngx-permissions';
// import {FilesInterface} from '../../../interfaces/get-bid-form-interface';
// import {UserAndCompanyInfo} from '../../../interfaces/user-and-company-info';
// import { CurrencyPipe } from '@angular/common';
//
// const TAX_RATE = 0.12;
//
// @Component({
//   selector: 'app-bid-form',
//   templateUrl: './bid-form.component.html',
//   styleUrls: ['./bid-form.component.css']
// })
// export class BidFormComponent implements OnInit {
//
//   @Input() quantity: any;
//
//   bsModalRef: BsModalRef;
//   modalRef:   BsModalRef;
//
//   bidForm:        FormGroup;
//   submitAttempt   =   false;
//   taxRequired     =   false;
//
//
//   subTotal:         number;
//   taxTotal:         number;
//   grandTotal:       number;
//   lineSubTotal:     number;
//   lineTaxTotal:     number;
//   lineTotal:        number;
//   comments:         string;
//
//   index:            number;
//   title  =          "Add a Line Item";
//
//   scboExists = false;
//
//   bidID;
//   projectID;
//
//   taxRate;
//
//   bidData: ScBidFormInterface;
//   lineData =      [] as LineDataInterface[];
//   projectData:    ProjectDataInterface;
//   builderInfo: UserAndCompanyInfo;
//   files:    FilesInterface;
//   // lineData =  [];
//
//   editing       = false;
//   viewing       = false;
//   showInvoiceTotal = false;
//
//   permissions;
//   constructor
//   (
//     private modalService: BsModalService,
//     private API: BackendService,
//     private route: ActivatedRoute,
//     private alerts: AlertService,
//     private auth: AuthService,
//     private ngxPermissions: NgxPermissionsService
//   )
//   {
//
//     this.taxRate = TAX_RATE;
//      this.permissions = ngxPermissions.getPermissions();
//     this.route.params.subscribe(param => {
//       this.projectID  = param.id;
//       this.bidID      = param.bidID;
//     });
//
//     this.API.getProjectAndBuilderInfoForSCBidOut(this.bidID, this.projectID).subscribe(data => {
//       this.projectData =   data.projectInfo;
//       this.builderInfo  =   data.builderInfo;
//     });
//
//     this.bidData      = this.route.snapshot.data.data.data;
//
//     this.editing = this.route.snapshot.data.data.editing;
//     this.viewing = this.route.snapshot.data.data.viewing;
//     if (this.editing || this.viewing) {
//       if (this.route.snapshot.data.data.data[0].name !== null) {
//         this.lineData     = this.route.snapshot.data.data.data;
//         this.scboExists = true;
//       }
//
//       this.grandTotal   = parseFloat(this.bidData[0].grandTotal);
//       this.subTotal     = this.bidData[0].subTotal;
//       this.taxTotal     = this.bidData[0].taxTotal;
//       this.comments     = this.bidData[0].comments;
//     } else {
//       this.subTotal = 0;
//       this.grandTotal = 0;
//       this.taxTotal = 0;
//     }
//
//
//
//     this.bidForm = new FormGroup({
//       'name':                         new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(255)])),
//       'description':                  new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(255)])),
//       'price':                        new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(1), Validators.max(9999999999)])),
//       'quantity':                     new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10), Validators.min(1), Validators.max(9999999999)])),
//       'uom':                          new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(255)])),
//       'lineSubTotal':                 new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'lineTaxTotal':                 new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'tax':                          new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'taxRate':                      new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'lineTotal':                    new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//     });
//
//     this.bidForm.controls['price'].valueChanges.debounceTime(300).subscribe(data    => this.calculateLineTotal());
//     this.bidForm.controls['quantity'].valueChanges.debounceTime(300).subscribe(data => this.calculateLineTotal());
//     this.bidForm.controls['tax'].valueChanges.debounceTime(100).subscribe(data      => this.calculateLineTotal());
//   }
//
//   ngOnInit() {
//
//     this.markAsRead();
//   }
//
//   markAsRead() {
//     if (this.auth.hasPermission('builder')) {
//       if (this.bidData.builder_viewed_scbo !== 1) {
//         this.API.markSCBOAsReadByBuilder(this.bidData[0].projectID, this.bidData[0].bidID, this.bidData[0].scboID).subscribe();
//       }
//     }
//     if (this.auth.hasPermission('subcontractor')) {
//       if (this.bidData.sc_read !== 1 && this.bidData[0].scboID > 0) {
//         this.API.markSCBOAsReadBySCAfterStatusChange(this.bidData[0].projectID, this.bidData[0].bidID, this.bidData[0].scboID).subscribe();
//       }
//     }
//   }
//
//   calculateLineTotal() {
//     const qty       = this.bidForm.value.quantity;
//     const price     = this.bidForm.value.price;
//     const tax       = (this.bidForm.value.tax) ? this.bidForm.value.tax : this.taxRate;
//
//     console.log("TAX", tax);
//     if (qty > 0 && price > 0 && tax > 0) {
//       this.lineTaxTotal   = (qty * price) * tax;
//       this.lineSubTotal   = (qty * price);
//       this.lineTotal      = this.lineSubTotal + this.lineTaxTotal;
//     }
//     else {
//       this.lineTaxTotal   = null;
//       this.lineSubTotal   = null;
//       this.lineTotal      = null;
//     }
//
//     console.log(this.lineTotal);
//
//   }
//
//   calculateBidTotal() {
//     let taxTotal = 0;
//     let subTotal = 0;
//     let grandTotal = 0;
//     for (let i = 0; i < this.lineData.length; i++) {
//       if (this.lineData[i].deleted !== 1) {
//         subTotal = Number(subTotal) + Number(this.lineData[i].lineSubTotal);
//         taxTotal = Number(taxTotal) + Number(this.lineData[i].lineTaxTotal);
//         grandTotal = Number(grandTotal) + Number(this.lineData[i].lineTotal);
//       }
//     }
//
//     this.grandTotal   = grandTotal;
//     this.taxTotal     = taxTotal;
//     this.subTotal     = subTotal;
//
//     this.showInvoiceTotal =  (this.grandTotal > 0);
//   }
//
//   onShowLineItemModal(template: TemplateRef<any>) {
//     this.modalRef = this.modalService.show(template);
//
//   }
//
//   onShowEditItemModal(lineItem: LineDataInterface, template: TemplateRef<any>) {
//     const index   = this.lineData.findIndex(item => item === lineItem);
//     this.index    = index;
//     this.bidForm = new FormGroup({
//       'name':                         new FormControl(lineItem.name, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(255)])),
//       'description':                  new FormControl(lineItem.description, Validators.compose([Validators.minLength(1), Validators.maxLength(255)])),
//       'price':                        new FormControl(lineItem.price, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(1), Validators.max(9999999999)])),
//       'quantity':                     new FormControl(lineItem.quantity, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10), Validators.min(1), Validators.max(9999999999)])),
//       'uom':                          new FormControl(lineItem.uom, Validators.compose([Validators.minLength(1), Validators.maxLength(255)])),
//       'lineSubTotal':                 new FormControl(lineItem.lineSubTotal, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'lineTaxTotal':                 new FormControl(lineItem.lineTaxTotal, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'tax':                          new FormControl(lineItem.tax, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'taxRate':                      new FormControl(lineItem.tax, Validators.compose([Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//       'lineTotal':                    new FormControl(lineItem.lineTotal, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(0.00), Validators.max(9999999999)])),
//     });
//
//     this.bidForm.controls['price'].valueChanges.debounceTime(300).subscribe(data    => this.calculateLineTotal());
//     this.bidForm.controls['quantity'].valueChanges.debounceTime(300).subscribe(data => this.calculateLineTotal());
//     this.bidForm.controls['tax'].valueChanges.debounceTime(100).subscribe(data      => this.calculateLineTotal());
//
//     this.lineSubTotal   = lineItem.lineSubTotal;
//     this.lineTaxTotal   = lineItem.lineTaxTotal;
//     this.lineTotal      = lineItem.lineTotal;
//     this.modalRef       = this.modalService.show(template);
//   }
//
//
//   onEditLineItem(input: LineDataInterface) {
//
//     this.title = `Edit ${input.name}`;
//
//     console.log("edit Form invalid", this.bidForm.invalid);
//
//     const controls = this.bidForm.controls;
//     for (const name in controls) {
//       if (controls[name].invalid) {
//         console.log(name);
//       }
//     }
//
//
//     if (input.tax == null) {
//       this.taxRequired = true;
//       this.submitAttempt = true;
//       return false;
//     }
//     if (this.bidForm.invalid) {
//       this.submitAttempt  = true;
//       return false;
//     }
//
//     const lineItem = {
//       name:           input.name,
//       description:    input.description,
//       quantity:       input.quantity,
//       uom:            input.uom,
//       tax:            input.tax,
//       price:          input.price,
//       lineTaxTotal:   input.lineTaxTotal,
//       lineSubTotal:   input.lineSubTotal,
//       lineTotal:      input.lineTotal,
//       taxRate:        input.taxRate,
//       deleted:        input.deleted,
//     };
//
//     this.lineData[this.index] = lineItem;
//     this.calculateBidTotal();
//     //this.lineData.push(lineItem);
//     this.modalRef.hide();
//     this.bidForm.reset();
//     this.bidForm.patchValue({
//       tax: this.taxRate
//     });
//
//     this.taxRate = TAX_RATE;
//   }
//
//   onDeleteLineItem(lineItem: LineDataInterface)
//   {
//
//     const message = lineItem.deleted ? 'restore' : 'delete';
//     // this.alerts.swalConfirm(`Are you sure you want to delete ${fileName}?`, '', true, 'Yes', true, 'Cancel');
//     swal({
//       title: `Are you sure you want to ${message} ${lineItem.name}?`,
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn  btn-outline-success btn-lg',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg ml-2',
//     }).then(() => {
//         const li = this.lineData.find(item => item === lineItem);
//         li.deleted = (li.deleted) ? 0 : 1;
//         this.calculateBidTotal();
//       },
//       (dismiss) =>
//       {
//         // dismiss can be 'cancel', 'overlay',
//         // 'close', and 'timer'
//         if (dismiss === 'cancel') {
//           // console.log("cancel");
//         }
//       });
//   }
//
//   onSaveLineItem(input: LineDataInterface) {
//
//     this.title = "Add a new line item";
//
//     console.log("input", input);
//
//     if (input.tax == null) {
//       this.taxRequired    = true;
//       this.submitAttempt  = true;
//       return false;
//     }
//
//     if (this.bidForm.invalid) {
//       this.submitAttempt  = true;
//       return false;
//     }
//
//       const lineItem = {
//         name:           input.name,
//         description:    input.description,
//         quantity:       input.quantity,
//         uom:            input.uom,
//         tax:            input.tax,
//         price:          input.price,
//         lineTaxTotal:   input.lineTaxTotal,
//         lineSubTotal:   input.lineSubTotal,
//         lineTotal:      input.lineTotal,
//         taxRate:        input.taxRate,
//         deleted:        input.deleted
//       };
//
//     this.lineData.push(lineItem);
//     this.calculateBidTotal();
//     this.bidForm.reset();
//     this.modalRef.hide();
//       this.bidForm.patchValue({
//         tax: this.taxRate
//       });
//       console.log("LDD", this.lineData);
//     this.taxRate = TAX_RATE;
//
//     /*if (i) {
// 	  this.lineData[i] = lineItem;
// 	} else {
// 	  this.lineData.push(lineItem);
// 	}*/
//
//
//
//     }
//
//     onSubmitBid()
//     {
//       /*this.bidData.push({
//         grandTotal:       this.grandTotal,
//         subTotal:         this.subTotal,
//         taxTotal:         this.taxTotal,
//         lineData:         this.lineData
//       });*/
//
//       this.calculateBidTotal();
//       let bd: ScBidFormInterface;
//       bd = {
//         grandTotal: this.grandTotal,
//         subTotal: this.subTotal,
//         taxTotal: this.taxTotal,
//         lineData: this.lineData,
//         comments: this.comments,
//       };
//
//       if (this.scboExists) {
//         swal({
//           title: "You are editing an existing Quote. \n\n The Builder will have to accept this Bid again if they have already accepted it.\n",
//           text: "Are you sure you want to continue?",
//           type: 'question',
//           buttonsStyling: false,
//           showConfirmButton: true,
//           confirmButtonText: 'Yes   ',
//           confirmButtonClass: 'btn  btn-outline-success btn-lg',
//           showCancelButton: true,
//           cancelButtonText: ' Cancel',
//           cancelButtonClass: 'btn btn-outline-danger btn-lg ml-2',
//         }).then(() => {
//             this.API.SCSendBidOut(bd, this.projectID, this.bidID)
//               .subscribe(data =>
//                 {
//                   this.alerts.swalSuccess(data, '', false, '', 2500, false, '', false);
//                   this.getSCBO();
//                 },
//                 (err => this.alerts.swalError(err, '', true, 'Ok')));
//           },
//           (dismiss) =>
//           {
//             // dismiss can be 'cancel', 'overlay',
//             // 'close', and 'timer'
//             if (dismiss === 'cancel') {
//               // console.log("cancel");
//             }
//           });
//       }
//       else {
//         this.API.SCSendBidOut(bd, this.projectID, this.bidID)
//           .subscribe(data =>
//             {
//               this.alerts.swalSuccess(data, '', false, '', 2500, false, '', false);
//               this.getSCBO();
//             },
//             (err => this.alerts.swalError(err, '', true, 'Ok')));
//       }
//     }
//
//
//     onAcceptBid() {
//       this.API.acceptBid(this.bidID).subscribe(data => {
//         this.alerts.swalSuccess(data, '', false, '', 2500, false, '', false);
//         this.getSCBO();
//
//         },
//         (err => this.alerts.swalError(err, '', true, 'Ok')));
//     }
//
//     onRejectBid() {
//       this.API.rejectBid(this.bidID).subscribe(data => {
//         this.alerts.swalSuccess(data, '', false, '', 2500, false, '', false);
//         this.getSCBO();
//       },
//         (err => this.alerts.swalError(err, '', true, 'Ok')));
//     }
//
//     getSCBO() {
//       this.API.getSCBidOut(this.projectID, this.bidID).subscribe(data2 =>
//       {
//         this.lineData     = data2.data;
//         this.bidData      = data2.data;
//         this.editing      = data2.editing;
//         this.scboExists   = true;
//       });
//     }
//
// }
//
// export class ModalContentComponent {
//   title:        string;
//   lineItemData: LineDataInterface;
//
//   constructor(public bsModalRef: BsModalRef) {}
// }
