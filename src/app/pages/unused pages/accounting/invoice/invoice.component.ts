// import {Component, OnInit, TemplateRef} from '@angular/core';
// import {ActivatedRoute} from '@angular/router';
// import {AccountingDataInterface} from '../../../interfaces/accounting-data.interface';
// import {InvoiceInterface} from '../../../interfaces/invoice.interface';
// import {SCBOAndSCBOLIInterfaceForInvoice} from '../../../interfaces/sc-bid-form';
// import {BsModalRef, BsModalService} from 'ngx-bootstrap';
// import {FormControl, FormGroup, Validators} from '@angular/forms';
// import {BackendService} from '../../../services/backend.service';
// import {AlertService} from '../../../services/alerts/alert.service';
// import swal from "sweetalert2";
//
// @Component({
//   selector: 'app-invoice',
//   templateUrl: './invoice.component.html',
//   styleUrls: ['./invoice.component.css']
// })
// export class InvoiceComponent implements OnInit {
//
//   accountingData:       AccountingDataInterface;
//   apm:                  InvoiceInterface;
//   scboli:               SCBOAndSCBOLIInterfaceForInvoice;
//   modalRef:             BsModalRef;
//   sendPaymentForm:      FormGroup;
//   submitAttempt =       false;
//
//   projectID;
//   accountingID;
//   constructor
//   (
//     private route: ActivatedRoute,
//     private modalService: BsModalService,
//     private API: BackendService,
//     private alerts: AlertService,
//   )
//   {
//     this.accountingData   = this.route.snapshot.data.data.accounting;
//     this.apm              = this.route.snapshot.data.data.apm;
//     this.scboli           = this.route.snapshot.data.data.scbo;
//
//     this.projectID        = this.route.snapshot.params.id;
//     this.accountingID     = this.route.snapshot.params.accountingID;
//
//
//     this.sendPaymentForm = new FormGroup({
//       'paymentAmount':                    new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(255), Validators.min(1), Validators.max(9999999999)])),
//     });
//   }
//
//   ngOnInit() {
//   }
//
//   onMakePayment(template: TemplateRef<any>) {
//     this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
//   }
//
//   onSendPayment(input) {
//     this.submitAttempt    = false;
//     if (this.sendPaymentForm.invalid) {
//       this.submitAttempt  = true;
//       return false;
//     }
//     swal({
//       title: 'Are you sure you sent $' + input.paymentAmount + ' to ' + this.accountingData[0].bci_company + ' already?',
//       type: 'question',
//       buttonsStyling: false,
//       showConfirmButton: true,
//       confirmButtonText: 'Yes   ',
//       confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//       showCancelButton: true,
//       cancelButtonText: '  Cancel ',
//       cancelButtonClass: 'btn btn-outline-danger btn-lg',
//     }).then(() => {
//         this.API.sendPayment(input, this.projectID, this.accountingID).subscribe(data =>
//         {
//           this.alerts.swalSuccess(data, '', false, '', 2500, false);
//           this.getAccountingData();
//           this.modalRef.hide();
//         },
//           (err) => this.alerts.swalError(err, '', true, 'Ok'));
//       },
//       (dismiss) =>
//       {
//         if (dismiss === 'cancel') {
//         }
//       });
//   }
//
//   confirmOrUnconfirmPayment(pm: InvoiceInterface, status: string ) {
//       const message = status === 'confirm' ? `Are you sure you want to confirm that you have received a payment of $${pm.amountPaid} from the Builder?`
//                                            : `Are you sure you want to mark this payment of $${pm.amountPaid} as not received?`;
//       swal({
//         title: message,
//         type: 'question',
//         buttonsStyling: false,
//         showConfirmButton: true,
//         confirmButtonText: 'Yes   ',
//         confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
//         showCancelButton: true,
//         cancelButtonText: '  Cancel ',
//         cancelButtonClass: 'btn btn-outline-danger btn-lg',
//       }).then(() => {
//         console.log("then");
//           this.API.changePaymentStatus(status, this.projectID, this.accountingID, pm.id).subscribe(data => {
//             this.alerts.swalSuccess(data, '', false, '', 2500);
//             this.getAccountingData();
//           }, (err) => {
//             this.alerts.swalError(err, '', true, 'Ok');
//           });
//         },
//         (dismiss) =>
//         {
//           // dismiss can be 'cancel', 'overlay',
//           // 'close', and 'timer'
//           if (dismiss === 'cancel') {
//             console.log("cancel;");
//           }
//         });
//   }
//
//   getAccountingData() {
//     this.API.getAccountingAndInvoiceForProject(this.projectID, this.accountingID).subscribe(data2 => {
//       this.accountingData     = data2.accounting;
//       this.apm                = data2.apm;
//       this.scboli             = data2.scbo;
//     });
//   }
//
// }
