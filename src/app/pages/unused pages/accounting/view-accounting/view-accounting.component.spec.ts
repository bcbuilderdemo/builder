import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAccountingComponent } from './view-accounting.component';

describe('ViewAccountingComponent', () => {
  let component: ViewAccountingComponent;
  let fixture: ComponentFixture<ViewAccountingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAccountingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAccountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
