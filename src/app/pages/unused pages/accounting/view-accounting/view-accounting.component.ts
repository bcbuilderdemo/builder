// import { Component, OnInit } from '@angular/core';
// import {AuthService} from '../../../services/auth/auth.service';
// import {BackendService} from '../../../services/backend.service';
// import {ScTradesPaginatorInterface} from '../../../interfaces/sctrades-paginator.interface';
// import {AlertService} from '../../../services/alerts/alert.service';
// import {ActivatedRoute} from '@angular/router';
// import {AccountingDataInterface, AccountingInterface} from '../../../interfaces/accounting-data.interface';
// import {GetAccountingDataResolverService} from '../../../services/resolvers/get-accounting-data-resolver/get-accounting-data-resolver.service';
// @Component({
//   selector: 'app-view-accounting',
//   templateUrl: './view-accounting.component.html',
//   styleUrls: ['./view-accounting.component.css']
// })
//
// export class ViewAccountingComponent implements OnInit {
//
//   trades:           ScTradesPaginatorInterface;
//   totalItems:       number;
//   currentPage:      number;
//   projectID;
//   type;
//   title;
//   accounting:     AccountingInterface;
//
//   // viewAll         = false;
//   viewProject     = false;
//   viewReport      = false;
//   maxSize         = 10;
//
//
//   constructor
//   (
//     private auth: AuthService,
//     private API: BackendService,
//     private alerts: AlertService,
//     private route: ActivatedRoute,
//     private accountingService: GetAccountingDataResolverService
//   )
//   {
//     this.type         = route.snapshot.params.type;
//     this.accounting   = route.snapshot.data.data;
//     this.projectID    = route.snapshot.params.id;
//
//     console.log("ac", this.accounting);
//
//     this.totalItems     = this.accounting.last_page;
//     this.currentPage    = this.accounting.current_page;
//
//     if (this.type === 'for-project')
//     {
//       this.viewProject = true;
//       this.title = `View Accounting for Project`;
//     }
//     else if (this.type === 'all') {
//       this.viewAll = true;
//       this.title = "View Accounting for all Projects";
//     }
//
//
//
//     /*if (this.auth.hasPermission('builder')) {
//       if (this.viewProject) {
//         this.accounting = route.snapshot.data.data;
//       }
//       else if (this.viewAll) {
//
//       }
//     }
//
//     if (this.auth.hasPermission('subcontractor')) {
//       if (this.viewProject) {
//         this.accounting = route.snapshot.data.data;
//       }
//       else if (this.viewAll) {
//
//       }
//     }*/
//   }
//
//   ngOnInit() {
//   }
//
//   // TODO: NOT YET TESTED. MAY NEED TO ADD PARAMETERS FOR TYPE AND PROJECT ID?
//   onAccountingPageChanged(event: any): void {
//       const URL = `${this.accounting.path}?page=${event.page}`;
//       this.API.getPaginatorPage(URL)
//         .subscribe(data =>
//           {
//             this.trades = data;
//             this.totalItems = this.trades.last_page;
//             this.currentPage = this.trades.current_page;
//           },
//           (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
//   }
//
//   /*onViewInvoice(data: AccountingDataInterface) {
//     this.accountingService.setAccountingData(data);
//   }*/
//
//   onGetAccounting(type: string) {
//     this.viewReport     = false;
//     if (this.viewAll) {
//       this.API.getAllAccounting(type)
//         .subscribe(data =>
//           {
//             this.accounting     = data;
//             this.totalItems     = this.accounting.last_page;
//             this.currentPage    = this.accounting.current_page;
//             console.log("ongetAcct()", this.accounting);
//           },
//           (err) => this.alerts.swalError('Error', err, true, 'Ok'));
//     }
//     else if (this.viewProject) {
//       this.API.getAccountingForProject(this.projectID, type)
//         .subscribe(data =>
//           {
//             this.accounting     = data;
//             this.totalItems     = this.accounting.last_page;
//             this.currentPage    = this.accounting.current_page;
//           },
//           (err) => this.alerts.swalError('Error', err, true, 'Ok'));
//     }
//   }
//
//   onGetAccountingReport() {
//
//     if (this.viewAll) {
//       this.API.getAccountingReportsForAllProjects().subscribe(data => {
//         this.accounting     = data;
//         this.totalItems     = this.accounting.last_page;
//         this.currentPage    = this.accounting.current_page;
//         this.viewReport     = true;
//         console.log("ongetreport()", this.accounting);
//
//       });
//     }
//     else if (this.viewProject) {
//       this.API.getAccountingReportForProject(this.projectID).subscribe(data => {
//         this.accounting     = data;
//         this.totalItems     = this.accounting.last_page;
//         this.currentPage    = this.accounting.current_page;
//         this.viewReport     = true;
//         console.log("ongetreport()", this.accounting);
//       });
//     }
//   }
//
//
// }
