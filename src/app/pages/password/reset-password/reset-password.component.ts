import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BackendService} from '../../../services/backend.service';
import {AlertService} from '../../../services/alerts/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm:        FormGroup;
  submitAttempt =           false;

  constructor
  (
    private API: BackendService,
    private alertService: AlertService,
    private router: Router
  )
  {
    this.resetPasswordForm = new FormGroup({
      'email':                         new FormControl('', Validators.compose([Validators.email, Validators.required, Validators.minLength(2), Validators.maxLength(50)])),
    });
  }

  ngOnInit() {
  }

  onSubmit(form: any) {

    if (this.resetPasswordForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    this.API.forgotPassword(form)
      .subscribe(message => {
          this.alertService.success('', message);
        },
        (error) => {
        console.log("ERR", error);
        this.alertService.error("Error", error);
        });
  }

}
