import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../interfaces/profile-info.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MasksInterface } from '../../interfaces/masks.interface';
import { GlobalConstants } from '../../constants/global.constants';
import 'rxjs/add/operator/debounceTime';
import { BackendService } from '../../services/backend.service';
import { AlertService } from '../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import swal from "sweetalert2";
import { EqualValidator } from '../../validators/password.validator';

@Component({
    templateUrl: 'photo.component.html',
    styleUrls: ['./photo.component.css']
})

export class SocialProfilePhotoComponent implements OnInit {
    logo: ProfileUploadsInterface;
    profileInfo: ProfileInfoInterface;
    profileUploads = [] as ProfileUploadsInterface[];
    profileStorageURL: string;
    image: ProfileUploadsInterface;
    backgroundImage: ProfileUploadsInterface;
    modalRef: BsModalRef;

    constructor(
        private route: ActivatedRoute,
        private constants: GlobalConstants,
        private API: BackendService,
        private alerts: AlertService,
        private modalService: BsModalService
    ) {
        this.profileInfo = this.route.snapshot.data.data.profileInfo;
        const uploads = this.route.snapshot.data.data.profileUploads;
        this.profileStorageURL = `${this.constants.STORAGEURL}/`;

        for (const upload of uploads) {
            console.log();
            if (parseInt(upload.isLogo) === 0) {
                this.profileUploads.push(upload);
            } else if (parseInt(upload.isLogo) === 1) {
                this.logo = upload;
            }
            else {
                this.backgroundImage = upload;
            }
        }
    }

    ngOnInit() {
    }

    onViewImage(template: TemplateRef<any>, file: ProfileUploadsInterface, index?) {
        if (!parseInt(String(file.isLogo))) {
            console.log("file", file);
            this.image = file;
            this.modalRef = this.modalService.show(template);
        }
        else {
            this.modalRef = this.modalService.show(template);
        }
    }
}
