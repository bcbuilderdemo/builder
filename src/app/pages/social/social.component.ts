import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../interfaces/profile-info.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MasksInterface } from '../../interfaces/masks.interface';
import { GlobalConstants } from '../../constants/global.constants';
import 'rxjs/add/operator/debounceTime';
import { BackendService } from '../../services/backend.service';
import { AlertService } from '../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import swal from "sweetalert2";
import { EqualValidator } from '../../validators/password.validator';

@Component({
    templateUrl: 'social.component.html',
    styleUrls: ['social.component.css']
})

export class SocialProfileComponent implements OnInit {
    logo: ProfileUploadsInterface;
    backgroundImage: ProfileUploadsInterface;
    profileInfo: ProfileInfoInterface;
    profileUploads = [] as ProfileUploadsInterface[];
    profileStorageURL: string;
    newPostInfo: any;
    postsInfo: any[];

    constructor(
        private route: ActivatedRoute,
        private constants: GlobalConstants,
        private API: BackendService,
        private alerts: AlertService,
        private modalService: BsModalService
    ) {
        debugger
        this.profileInfo = this.route.snapshot.data.data.profileInfo;
        this.postsInfo = this.route.snapshot.data.data.postsInfo;
        const uploads = this.route.snapshot.data.data.profileUploads;
        this.profileStorageURL = `${this.constants.STORAGEURL}/`;
        this.newPostInfo = {
            userID: this.profileInfo.id,
            post: ''
        };
        for (const upload of uploads) {
            console.log();
            if (parseInt(upload.isLogo) === 0) {
                this.profileUploads.push(upload);
            } else if (parseInt(upload.isLogo) === 1) {
                this.logo = upload;
            }
            else {
                this.backgroundImage = upload;
            }
        }
    }

    ngOnInit() {
    }

    publishPost() {
        if (this.newPostInfo.post === '') {
            this.alerts.swalError('Please input post', '', true, 'Ok');
            return;
        }
        this.API.publishPost(this.newPostInfo)
            .subscribe((res) => {
                this.alerts.swalSuccess('Successfully Posted', '', false, '', 2500);
                this.postsInfo.unshift(res.post);
            }, (err) => this.alerts.swalError(err, '', true, 'Ok'));
    }
}
