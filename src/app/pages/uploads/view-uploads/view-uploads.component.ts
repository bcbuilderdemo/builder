import { Component, OnInit, ElementRef, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FilesInterface } from '../../../interfaces/files.interface';
import { GlobalConstants } from '../../../constants/global.constants';
import { BackendService } from '../../../services/backend.service';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { AlertService } from '../../../services/alerts/alert.service';

@Component({
  selector: 'app-view-uploads',
  templateUrl: './view-uploads.component.html',
  styleUrls: ['./view-uploads.component.css']
})
export class ViewUploadsComponent implements OnInit {
  projectData: ProjectDataInterface;
  projectFiles: FilesInterface[];
  projectID = null;
  storageURL: string;
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor
    (
    private router: Router,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
    private API: BackendService,
    private alerts: AlertService,
    private projectSerivce: ProjectResolverService
    ) {

  }

  ngOnInit() {
    this.storageURL = this.constants.STORAGEURL;
    this.projectFiles = this.route.snapshot.data['projectFiles'];
    this.projectID = this.route.snapshot.params.id;
    this.projectData = this.projectSerivce.getProjectData();
  }

  onClickFile(file: FilesInterface) {
    window.open(this.API.getProjectFile(file.fileID));
  }

  deleteFile(file, index) {
    this.API.deleteProjectFile(file.fileID)
      .subscribe(res => {
        this.projectFiles.splice(index, 1);
      })
  }

  uploadFile() {
    this.fileInput.nativeElement.click();
  }

  addProjectFile($event) {
    let file = $event.target.files[0];
    const validFileExts = ["jpg", "jpeg", "png", "svg", "bmp", "pdf", "doc", "docx"];
    let validate = true;
    const type = file.name.split('.').pop();
    if (!validFileExts.includes(type)) {
      this.alerts.swalError("You can only upload pdf/doc/img", '', true, 'Ok');
      validate = false;
      return false;
    }
    if (!validate) return;
    const uploadProjectForm = new FormData();
    uploadProjectForm.append('projectFiles[]', file);
    uploadProjectForm.append('projectID', this.projectID);
    
    this.API.uploadProjectFiles(uploadProjectForm)
      .subscribe(data => {
        this.alerts.swalSuccess('New File Successfully uploaded', '', false, '', 2500, false, '', true);
        this.projectFiles.push(data.projectFiles[0]);
      })    
  }

}
