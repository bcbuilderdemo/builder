import { GeoCoder } from '@ngui/map/src/services/geo-coder';
import { ProfileResolverService } from '../../../services/resolvers/profile-resolver/profile-resolver.service';
import { Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalComponent } from '../../../templates/modal/modal.component';
import { NewProjectComponent } from '../../project-pages/new-project/new-project.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { BackendService } from '../../../services/backend.service';
import { GetProjectsInterface, ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { SessionStorageService } from 'ngx-webstorage';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { UserAndCompanyInfo } from '../../../interfaces/user-and-company-info';
import { GlobalConstants } from '../../../constants/global.constants';
import { NotificationsInterface } from '../../../interfaces/notifications.interface';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../../interfaces/profile-info.interface';
import swal from "sweetalert2";
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-maps',
  templateUrl: 'google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})

export class GoogleMapComponent {
  bsModalRef: BsModalRef;
  modalRef: BsModalRef;
  mineProjects: GetProjectsInterface;
  othersProjects: GetProjectsInterface;
  project: ProjectDataInterface = null;
  mySubprojects: any[];
  othersSubProjects: any[];
  hasNots = false;
  hasProjects = false;
  notKeys;
  maxSize = 5;
  totalItems: number;
  currentPage: number;
  storageURL: string;
  profileInfo: ProfileInfoInterface;
  logo: ProfileUploadsInterface;
  image: ProfileUploadsInterface;
  profileUploads = [] as ProfileUploadsInterface[];

  greenIcon = 'http://individual.icons-land.com/IconsPreview/MapMarkers/PNG/Centered/48x48/MapMarker_Marker_Outside_Green.png';
  normalIcon = 'http://individual.icons-land.com/IconsPreview/MapMarkers/PNG/Centered/48x48/MapMarker_Marker_Outside_Red.png';
  public mysubProjectMarkers = [];
  public othersSubProjectsMarkers = [];
  clickedSubProjects = null;
  clickedSubProject = null;
  mapConfig = {
    center: { lat: 49.1087238, lng: -122.7794947 },
    zoom: 11
  };
  config = {
    animated: true,
    class: 'modal-md'
  };

  clickedProjectMarker = null;

  // Array that will hold the addresses of each project so we can loop through these in the .html file
  public mineProjectMarkers = [];
  public othersProjectMarkers = [];

  constructor(
    private viewContainerRef: ViewContainerRef,
    private modalService: BsModalService,
    private API: BackendService,
    private alerts: AlertService,
    private projectService: ProjectResolverService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
  ) {
    // Constructor content here
  }

  ngOnInit() {
    const uploads = this.route.snapshot.data.profile.profileUploads;

    this.storageURL = `${this.constants.STORAGEURL}/`;
    this.mineProjects = this.route.snapshot.data.data.projectInfo;
    this.othersProjects = this.route.snapshot.data.data.othersProjectInfo;
    this.mySubprojects = this.route.snapshot.data.data.mySubprojects;
    this.othersSubProjects = this.route.snapshot.data.data.othersSubprojects;
    this.profileInfo = this.route.snapshot.data.profile.profileInfo;

    Object.values(this.mySubprojects).forEach((project) => {
      let firstSubproject = project[0];
      this.mysubProjectMarkers.push({
        id: firstSubproject.project.id,
        address: firstSubproject.project.address,
        city: firstSubproject.project.city,
        province: firstSubproject.project.province,
        postalCode: firstSubproject.project.postalCode,
        subprojects: project,
        fromRedMarker: false,
        companySlug: firstSubproject.project.builder.company_info.companySlug
      })
    });
    this.mineProjectMarkers = this.mineProjects.data
      .filter((project) => {
        var found = this.mysubProjectMarkers.find(subprojectMarker => {
          return subprojectMarker.id === project.id
        })
        return found === undefined
      })
      .map((project) => {
        project.isOther = false;
        return project;
      });

    Object.values(this.othersSubProjects).forEach((project) => {
      let firstSubproject = project[0];
      this.othersSubProjectsMarkers.push({
        id: firstSubproject.project.id,
        address: firstSubproject.project.address,
        city: firstSubproject.project.city,
        province: firstSubproject.project.province,
        postalCode: firstSubproject.project.postalCode,
        subprojects: project,
        fromRedMarker: true,
        companySlug: firstSubproject.project.builder.company_info.companySlug
      });
    });

    this.othersProjectMarkers = this.othersProjects.data
      .filter((project) => {
        var found = this.othersSubProjectsMarkers.find(subprojectMarker => {
          return subprojectMarker.id === project.id
        })
        return found === undefined
      })
      .map((project) => {
        project.isOther = true;
        return project;
      });
  }

  public displayLabel({ target: marker }, project) {
    this.clickedProjectMarker = null;
    this.clickedSubProjects = project.subprojects;
    this.clickedSubProjects.fromRedMarker = project.fromRedMarker;

    marker.nguiMapComponent.closeInfoWindow('iwForEmptySubProject', marker);
    marker.nguiMapComponent.openInfoWindow('iw', marker);
  }

  public displayEmptySubprojectLabel({ target: marker }, project) {
    this.clickedSubProjects = null;
    this.clickedProjectMarker = project;

    marker.nguiMapComponent.closeInfoWindow('iw', marker);
    marker.nguiMapComponent.openInfoWindow('iwForEmptySubProject', marker);
  }

  showSubproject(subproject, template: TemplateRef<any>) {
    this.clickedSubProject = subproject
    this.bsModalRef = this.modalService.show(template, this.config);
  }

  displayEmptySubProjectInformation(template: TemplateRef<any>) {
    let project = this.clickedProjectMarker;
    this.bsModalRef = this.modalService.show(template, this.config);
  }

  close() {
    this.bsModalRef.hide();
  }
}
