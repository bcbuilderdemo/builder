import { AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalDirective } from "ngx-bootstrap";
import { NgForOf } from "@angular/common";
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { BackendService } from "../../../services/backend.service";
import { NewProjectInterface } from '../../../interfaces/new-project-interface';
import { AlertService } from '../../../services/alerts/alert.service';
import { Router } from '@angular/router';
import { ViewEncapsulation } from '@angular/core';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { GlobalConstants } from '../../../constants/global.constants';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { MasksInterface } from '../../../interfaces/masks.interface';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class NewProjectComponent implements OnInit, AfterViewInit {
  @ViewChild('projectUploadInput') projectUploadInput: ElementRef;

  public config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false
  };


  newProjectForm: FormGroup;
  projectFiles = [];

  masks: MasksInterface;
  regexPatterns: any;

  submitAttempt = false;
  projectTypeInvalid = false;
  zoningInvalid = false;
  buildingTypeInvalid = false;

  constructor
    (
    private API: BackendService,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private alerts: AlertService,
    private router: Router,
    private constants: GlobalConstants,
    private projectService: ProjectResolverService,

  ) {

    this.masks = constants.masks;
    this.regexPatterns = this.constants.regexPatterns;

    console.log(this.regexPatterns.postalCode);

    this.newProjectForm = new FormGroup({
      'projectType': new FormControl(''),
      'buildingType': new FormControl(''),
      'landType': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'address': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'address2': new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(100)])),
      'city': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'postalCode': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(this.regexPatterns.postalCode)])),
      'province': new FormControl('BC'),
      'country': new FormControl('Canada', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'firstName': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])),
      'lastName': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])),
      'buildValue': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'lotSize': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'description': new FormControl(),
      'details': new FormControl(),
      'zoning': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'buildingSize': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'numUnits': new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(100)]))
    });
  }

  ngOnInit() {

  }

  ngAfterViewInit() {

  }
  hide() {
    this.bsModalRef.hide();
  }

  // onFileChange($event) {
  // let file = $event.target.files[0];
  // const mime = file.type;    

  // if (mime != 'application/pdf') {
  //   this.alerts.swalError("You can only upload pdf", '', true, 'Ok');
  //   this.uploadPdf.nativeElement.value = "";
  //   return false;
  // }
  //   this.newProjectForm.controls['uploadPdf'].setValue(file ? file.name : '');
  //   this.pdfFile = file;
  // }

  addProjectFile($event) {
    let files = $event.target.files;
    const validFileExts = ["jpg", "jpeg", "png", "svg", "bmp", "pdf", "doc", "docx"];
    let validate = true;
    // this.projectUploadInput.nativeElement.value = "";
    for (let i = 0; i < files.length; i++) {
      let file = files[i];
      const type = file.name.split('.').pop();
      if (!validFileExts.includes(type)) {
        this.alerts.swalError("You can only upload pdf/doc/img", '', true, 'Ok');
        validate = false;
        return false;
      }
    }
    if (!validate) return;

    this.projectFiles.push.apply(this.projectFiles, files);
  }

  removeProjectFile(index) {
    this.projectFiles.splice(index, 1);
  }

  onSubmit(input: NewProjectInterface) {
    if (this.newProjectForm.value.buildingType === "") {
      this.buildingTypeInvalid = true;
      this.submitAttempt = true;
      return false;
    }
    if (this.newProjectForm.value.zoning === "") {
      this.zoningInvalid = true;
      this.submitAttempt = true;
      return false;
    }
    if (this.newProjectForm.value.projectType === "") {
      this.projectTypeInvalid = true;
      this.submitAttempt = true;
      return false;
    }
    if (this.newProjectForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    let self = this;
    this.API.newProject(input).subscribe(data => {
      console.log('saved');
      if (this.projectFiles.length > 0) {
        const uploadProjectForm = new FormData();
        this.projectFiles.forEach(projectFile => {
          uploadProjectForm.append('projectFiles[]', projectFile);
        })
        uploadProjectForm.append('projectID', data.project.id);
        
        this.API.uploadProjectFiles(uploadProjectForm)
          .subscribe(data => {
            self.alerts.swalSuccess('Project Successfully created', '', false, '', 2500, false, '', true);
            self.bsModalRef.hide();
          })
      } else {
        this.bsModalRef.hide();
        this.alerts.swalSuccess('Project Successfully created', '', false, '', 2500, false, '', true);
      }
    });
  }



}
