import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import 'rxjs/add/operator/pluck';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
import {NotificationsInterface} from '../../../interfaces/notifications.interface';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  projectData:  ProjectDataInterface;
  nots:         NotificationsInterface | any;
  hasNots       = false;
  bidFormData:  any;

  constructor
  (
    private router: Router,
    private route: ActivatedRoute,
    private projectService: ProjectResolverService,
  )
  {
     this.projectData = this.route.snapshot.data['projectData'];
     this.nots        = this.route.snapshot.data.nots.nots;
     this.hasNots      = this.nots;
     this.projectService.setProjectData(this.projectData);

     console.log(this.projectData);

  }

  ngOnInit() {

  }



}
