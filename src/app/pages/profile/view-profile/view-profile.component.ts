import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../../interfaces/profile-info.interface';
import { GlobalConstants } from '../../../constants/global.constants';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {

  profileInfo: ProfileInfoInterface;
  logo: ProfileUploadsInterface;
  image: ProfileUploadsInterface;
  backgroundImage: ProfileUploadsInterface;
  modalRef: BsModalRef;
  profileStorageURL: string;
  profileUploads = [] as ProfileUploadsInterface[];
  companySlug: string;
  postsInfo: any[];

  constructor
    (
    private route: ActivatedRoute,
    private constants: GlobalConstants,
    private modalService: BsModalService,

  ) {
    debugger;
    this.profileInfo = this.route.snapshot.data.data.profileInfo;
    this.postsInfo = this.route.snapshot.data.data.postsInfo;
    const uploads = this.route.snapshot.data.data.profileUploads;
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;
    this.companySlug = this.route.snapshot.params.companySlug;
    for (const upload of uploads) {
      if (parseInt(upload.isLogo) === 0) {
        this.profileUploads.push(upload);
      }
      else if (parseInt(upload.isLogo) === 1) {
        this.logo = upload;
      }
      else {
        this.backgroundImage = upload;
      }
    }
  }

  ngOnInit() {
  }

  onViewImage(template: TemplateRef<any>, file: ProfileUploadsInterface) {
    if (parseInt(String(file.isLogo)) !== 1) {
      this.image = file;
      this.modalRef = this.modalService.show(template);
    }
    else {
      this.modalRef = this.modalService.show(template);
    }
  }

  openURL(URL: string) {
    if (URL) { window.open(URL); }
  }

}