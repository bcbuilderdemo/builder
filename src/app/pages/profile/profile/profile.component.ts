import { Component, ElementRef, OnInit, TemplateRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../../interfaces/profile-info.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MasksInterface } from '../../../interfaces/masks.interface';
import { GlobalConstants } from '../../../constants/global.constants';
import 'rxjs/add/operator/debounceTime';
import { BackendService } from '../../../services/backend.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import swal from "sweetalert2";
import { EqualValidator } from '../../../validators/password.validator';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})

export class ProfileComponent implements OnInit {

  @ViewChild('imageInput') imageInput: ElementRef;

  profileForm: FormGroup;
  changeEmailForm: FormGroup;
  changePasswordForm: FormGroup;
  masks: MasksInterface;
  modalRef: BsModalRef;
  logo: ProfileUploadsInterface;
  backgroundImage: ProfileUploadsInterface;
  image: ProfileUploadsInterface;
  oldFile: ProfileUploadsInterface;
  profileInfo: ProfileInfoInterface;

  profileUploads = [] as ProfileUploadsInterface[];

  profileStorageURL: string;
  submitAttempt = false;
  activeSlideIndex: number;
  regexPatterns: any;


  constructor
    (
    private zone: NgZone,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
    private API: BackendService,
    private alerts: AlertService,
    private modalService: BsModalService
  ) {

    console.log(route.snapshot.data);
    this.activeSlideIndex = 0;
    this.masks = constants.masks;
    this.regexPatterns = this.constants.regexPatterns;
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;


    this.profileForm = new FormGroup({
      'address': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190)])),
      'address2': new FormControl(''),
      'city': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190)])),
      'postalCode': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.postalCode)])),
      'province': new FormControl('BC', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190)])),
      'phoneNumber': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.phoneNumber)])),
      'company': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190)])),
      'contactFirstName': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190)])),
      'contactLastName': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190)])),
      'phoneNumber2': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.phoneNumber)])),
      'twitterURL': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),
      'facebookURL': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),
      'googlePlusURL': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),
      'youtubeURL': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),
      'pinterestURL': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),
      'instagramURL': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),
      'aboutUs': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(2000)])),
      'website': new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(190), Validators.pattern(this.regexPatterns.URL)])),

    });

    this.profileInfo = this.route.snapshot.data.data.profileInfo;
    const uploads = this.route.snapshot.data.data.profileUploads;
    for (const upload of uploads) {
      if (parseInt(upload.isLogo) === 0) {
        this.profileUploads.push(upload);
      }
      else if (parseInt(upload.isLogo) === 1) {
        this.logo = upload;
      }
      else {
        this.backgroundImage = upload;
      }
    }

    this.profileForm.controls['twitterURL'].valueChanges.debounceTime(300).subscribe(data => this.checkURL(this.profileForm.controls['twitterURL']));
    this.profileForm.controls['facebookURL'].valueChanges.debounceTime(300).subscribe(data => this.checkURL(this.profileForm.controls['facebookURL']));
    this.profileForm.controls['googlePlusURL'].valueChanges.debounceTime(300).subscribe(data => this.checkURL(this.profileForm.controls['googlePlusURL']));
    this.profileForm.controls['youtubeURL'].valueChanges.debounceTime(300).subscribe(data => this.checkURL(this.profileForm.controls['youtubeURL']));
    this.profileForm.controls['pinterestURL'].valueChanges.debounceTime(300).subscribe(data => this.checkURL(this.profileForm.controls['pinterestURL']));
    this.profileForm.controls['instagramURL'].valueChanges.debounceTime(300).subscribe(data => this.checkURL(this.profileForm.controls['instagramURL']));
    this.profileForm.controls['website'].valueChanges.debounceTime(300).subscribe(data => this.checkWebsiteURL(this.profileForm.controls['website']));

  }

  ngOnInit() {
    Object.keys(this.profileForm.controls).forEach(key => {
      if (this.profileInfo[key]) {
        this.profileForm.controls[key].patchValue(this.profileInfo[key]);
      }
    });
  }

  onClickUploadFile(file) {
    this.imageInput.nativeElement.click();
    this.oldFile = file;
  }

  onChooseImage(event) {

    const image = event.target.files[0];
    const mime = image.type;
    const type = mime.split('/');
    const validFileExts = ["jpg", "jpeg", "png", "svg", "bmp"];
    this.imageInput.nativeElement.value = "";


    if (type[0] !== 'image') {
      this.alerts.swalError("You can only upload images", '', true, 'Ok');
      // this.imageInput.nativeElement.value = "";
      return false;
    }
    else if (!validFileExts.includes(type[1])) {
      this.alerts.swalError("You can only upload the following formats", '.JPEG, .JPG, .PNG, .SVG, .BMP', true, 'Ok');
      // this.imageInput.nativeElement.value = "";
      return false;
    }
    else if ((image.size / 1024) > 10240) {
      this.alerts.swalError("Image size cannot exceed 10 MB", '', true, 'Ok');
      // this.imageInput.nativeElement.value = "";
      return false;
    }

    const fd = new FormData();
    fd.append('image', image);
    fd.append('fileID', this.oldFile.fileID);
    fd.append('profileID', this.oldFile.profileID);
    this.API.updateProfileImage(fd)
      .subscribe(data => {
        const upload = data.profileUploads;
        if (parseInt(this.oldFile.isLogo) === 0) {
          const index = this.profileUploads.findIndex(f => f.fileID === upload.fileID);
          this.profileUploads[index] = upload;
        } else if (parseInt(this.oldFile.isLogo) === 1) {
          this.zone.run(() => {
            this.logo = upload;
          });
        } else {
          this.backgroundImage = upload;
        }
        this.alerts.swalSuccess(data.message, '', false, '', 2500);
      },
        (err) => this.alerts.swalError(err, '', true, 'Ok')
      );
  }

  onDeleteImage(file: ProfileUploadsInterface) {
    const fd = new FormData();
    fd.append('fileID', file.fileID);
    fd.append('profileID', file.profileID);

    swal({
      title: 'Are you sure you want to delete this image?',
      type: 'question',
      buttonsStyling: false,
      showConfirmButton: true,
      confirmButtonText: 'Yes   ',
      confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
      showCancelButton: true,
      cancelButtonText: '  Cancel ',
      cancelButtonClass: 'btn btn-outline-danger btn-lg',
    }).then(() => {
      this.API.deleteProfileImage(fd)
        .subscribe(data => {
          const upload = data.profileUploads;
          if (upload.isLogo === 0) {
            const index = this.profileUploads.findIndex(f => f.fileID === upload.fileID);
            if (index) {
              this.profileUploads[index] = upload;
            }
          } else if (upload.isLogo === 1) {
            this.logo = upload;
          } else {
            this.backgroundImage = upload;
          }
          this.alerts.swalSuccess(data.message, '', false, '', 2500);
        },
          (err) => this.alerts.swalError(err, '', true, 'Ok')
        );
    },
      (dismiss) => {
        if (dismiss === 'cancel') {
        }
      });

  }

  checkURL(control) {
    const regex = /(http(s?))\:\/\//;
    const str = control.value;
    if (!regex.test(str) && str.length) {
      control.setValue(`https://${str}`);
    }
  }

  checkWebsiteURL(control) {
    const regex = /(http(s?))\:\/\//;
    const str = control.value;
    if (!regex.test(str) && str.length) {
      control.setValue(`http://${str}`);
    }
  }

  onTestURL(URL: string) {
    if (URL) { window.open(URL); }
  }

  onChangeImage(type) {

  }

  /*onViewImage(file, index?) {
    this.activeSlideIndex = index;
    if (file.isLogo) {

    }
    else {

    }
  }*/

  onViewImage(template: TemplateRef<any>, file: ProfileUploadsInterface, index?) {
    if (parseInt(String(file.isLogo)) !== 1) {
      this.image = file;
      this.modalRef = this.modalService.show(template);
    }
    else {
      this.modalRef = this.modalService.show(template);

    }
  }

  onSubmit(form) {
    this.submitAttempt = false;
    if (this.profileForm.invalid) {
      this.submitAttempt = true;
      return false;
    }

    this.API.updateProfile(form)
      .subscribe(msg => this.alerts.swalSuccess(msg, '', false, '', 2500),
        (err) => this.alerts.swalError(err, '', true, 'Ok'));
  }

  onChangeEmail(template: TemplateRef<any>) {
    this.changeEmailForm = new FormGroup({
      'oldEmail': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(50), Validators.email])),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(50), Validators.email])),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(9), Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,20}$/)])),
      'password_confirmation': new FormControl('', Validators.compose([Validators.required, EqualValidator("password")])),
    });

    this.modalRef = this.modalService.show(template);
  }

  onChangePassword(template: TemplateRef<any>) {
    this.changePasswordForm = new FormGroup({
      'email': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(50), Validators.email])),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(9), Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,20}$/)])),
      'password_confirmation': new FormControl('', Validators.compose([Validators.required, EqualValidator("password")])),
      'oldPassword': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
    });

    this.modalRef = this.modalService.show(template);
  }

  onSubmitChangeEmail(input) {
    this.submitAttempt = false;
    if (this.changeEmailForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    this.API.changeEmail(input)
      .subscribe(msg => {
        this.alerts.swalSuccess(msg, '', false, '', 2500);
        this.modalRef.hide();
      },
        (err) => this.alerts.swalError(err, '', true, 'Ok'));
  }

  onSubmitChangePassword(input) {
    this.submitAttempt = false;
    if (this.changePasswordForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    this.API.changePassword(input)
      .subscribe(msg => {
        this.alerts.swalSuccess(msg, '', false, '', 2500);
        this.modalRef.hide();
      },
        (err) => this.alerts.swalError(err, '', true, 'Ok'));
  }
}