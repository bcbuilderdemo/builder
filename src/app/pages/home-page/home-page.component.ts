//zs-dashboard-component
import { Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalComponent } from '../../templates/modal/modal.component';
import { NewProjectComponent } from '../project-pages/new-project/new-project.component';
import { BsModalRef, BsModalService, TabsetComponent } from 'ngx-bootstrap';
import { BackendService } from '../../services/backend.service';
import { GetProjectsInterface, ProjectDataInterface } from '../../interfaces/get-project.interface';
import { SessionStorageService } from 'ngx-webstorage';
import { ProjectResolverService } from '../../services/resolvers/project-resolver/project-resolver.service';
import { AlertService } from '../../services/alerts/alert.service';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserAndCompanyInfo } from '../../interfaces/user-and-company-info';
import { GlobalConstants } from '../../constants/global.constants';
import { NotificationsInterface } from '../../interfaces/notifications.interface';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../interfaces/profile-info.interface';
import swal from "sweetalert2";
import { forEach } from '@angular/router/src/utils/collection';
import { SubcontractorInterface, SubcontractorPaginatorInterface } from '../../interfaces/subcontractor.interface';
import { ScTradesPaginatorInterface } from '../../interfaces/sctrades-paginator.interface';
import {
  BidsInInterface, BidsOutInterface,
  GetBidsByTradeInterface
} from '../../interfaces/get-bids-by-trade.interface';
//import { NewBidComponent } from '../bids/new-bid/new-bid.component';
import { GetScBidsPaginatorInterface } from '../../interfaces/get-sc-bids-paginator.interface';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {

  // @ViewChild('childModal') childModal: ModalComponent;
  // @ViewChild('newProject') newProject: NewProjectComponent;
  bsModalRef: BsModalRef;
  modalRef: BsModalRef;
  projects: GetProjectsInterface;
  userInfo:     UserAndCompanyInfo;
  nots: NotificationsInterface | any;
  hasNots = false;
  hasProjects = false;
  notKeys;
  maxSize = 5;
  totalItems: number;
  currentPage: number;
  storageURL: string;

  profileInfo: ProfileInfoInterface;
  logo: ProfileUploadsInterface;
  image: ProfileUploadsInterface;
  profileUploads = [] as ProfileUploadsInterface[];
  oldFile: ProfileUploadsInterface;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  projectData: ProjectDataInterface;

  isSc: boolean;
  isBuilder: boolean;
  projectID: number;
  bidType: string;
  title: string;
  showWhichTab;
  viewingBidsForProject: boolean;

  type;
  projectsByUser: any;


  public app: any;
  public headerThemes: any;
  public changeHeader: any;
  public sidenavThemes: any;
  public changeSidenav: any;
  public headerSelected: any;
  public sidenavSelected: any;

  @ViewChild('imageInput') imageInput: ElementRef;

  // public NewProjectRef: NewProjectComponent;

  constructor
    (
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    private modalService: BsModalService,
    private API: BackendService,
    private alerts: AlertService,
    private projectService: ProjectResolverService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
  ) {
    this.app = {
      layout: {
        sidePanelOpen: false,
        isMenuOpened: true,
        isMenuCollapsed: false,
        themeConfigOpen: false,
        rtlActived: false
      }
    };
    this.headerThemes = ['header-default', 'header-primary', 'header-info', 'header-success', 'header-danger', 'header-dark'];
    this.changeHeader = changeHeader;

    function changeHeader(headerTheme) {
      this.headerSelected = headerTheme;
    }

    this.sidenavThemes = ['sidenav-default', 'side-nav-dark'];
    this.changeSidenav = changeSidenav;

    function changeSidenav(sidenavTheme) {
      this.sidenavSelected = sidenavTheme;
    }


    if (this.auth.hasPermission('builder')) {
    }
    this.projectID = route.snapshot.params.id;

    this.projects = this.route.snapshot.data.data.projectInfo;
    this.profileInfo = this.route.snapshot.data.profile.profileInfo;
    //console.log(this.profileInfo);
    //console.log(this.projects);

    this.projectsByUser = this.API.getProjectsByID(this.profileInfo.userID).subscribe(
      (data) => { this.projectsByUser = data; }
    );
    console.log(this.projectsByUser);


  }

  ngOnInit() {


    this.storageURL = `${this.constants.STORAGEURL}/`;


    const uploads = this.route.snapshot.data.profile.profileUploads;
    this.nots = this.route.snapshot.data.nots.nots;
    // this.totalItems = this.projects.last_page;
    // this.currentPage = this.projects.current_page;
    console.log(this.route.snapshot.data.data);
    for (const upload of uploads) {
      if (!upload.isLogo) { this.profileUploads.push(upload); }
      else { this.logo = upload; }
    }

    this.hasNots = this.nots;
    this.hasProjects = this.projects.total > 0;


    if (this.nots) {
      this.hasNots = true;

    }
    else {
      this.hasNots = false;
    }

    if (this.auth.hasPermission('builder')) {
      if (!this.auth.checkIfUserHasCreatedAProject()) {
        this.bsModalRef = this.modalService.show(NewProjectComponent, { class: 'modal-dialog' });
      }
    }





    /*Object.entries(this.nots).forEach(([key, value]) => console.log(key + "=>" + value));

    this.notKeys = Object.keys(this.nots);
    console.log(this.notKeys);
    console.log(this.nots);*/
    //debugger;
  }

  openNewProjectModal() {
    this.bsModalRef = this.modalService.show(NewProjectComponent, { class: 'modal-dialog' });
    // this.NewProjectRef.showModal();
  }
  onGotoProject(project: ProjectDataInterface) {
    this.projectService.setProjectData(project);
  }

  // onPageChanged(event: any): void {
  //   const URL = `${this.projects.path}?page=${event.page}`;
  //   this.API.getPaginatorPage(URL)
  //     .subscribe(data => {
  //       this.projects = data;
  //       this.totalItems = this.projects.last_page;
  //       this.currentPage = this.projects.current_page;
  //     },
  //       (err) => { this.alerts.swalError('Error', 'An error has occurred please try again', true, 'Ok'); });
  // }

  onViewImage(template: TemplateRef<any>, file: ProfileUploadsInterface) {
    this.image = file;
    this.modalRef = this.modalService.show(template);
  }

  onEditImage(file) {
    this.imageInput.nativeElement.click();
    this.oldFile = file;
  }

  uploadImage(event) {

    const image = event.target.files[0];
    const mime = image.type;
    const type = mime.split('/');
    const validFileExts = ["jpg", "jpeg", "png", "svg", "bmp"];
    this.imageInput.nativeElement.value = "";

    if (type[0] !== 'image') {
      this.alerts.swalError("You can only upload images", '', true, 'Ok');
      // this.imageInput.nativeElement.value = "";
      return false;
    }
    else if (!validFileExts.includes(type[1])) {
      this.alerts.swalError("You can only upload the following formats", '.JPEG, .JPG, .PNG, .SVG, .BMP', true, 'Ok');
      // this.imageInput.nativeElement.value = "";
      return false;
    }
    else if ((image.size / 1024) > 10240) {
      this.alerts.swalError("Image size cannot exceed 10 MB", '', true, 'Ok');
      // this.imageInput.nativeElement.value = "";
      return false;
    }

    const fd = new FormData();
    fd.append('image', image);
    fd.append('fileID', this.oldFile.fileID);
    fd.append('profileID', this.oldFile.profileID);

    this.API.updateProfileImage(fd)
      .subscribe(data => {
        const upload = data.profileUploads;
        if (!this.oldFile.isLogo) {
          const index = this.profileUploads.findIndex(f => f.fileID === upload.fileID);
          this.profileUploads[index] = upload;
        } else {
          this.logo = upload;
        }

        this.alerts.swalSuccess(data.message, '', false, '', 2500);
      },
        (err) => this.alerts.swalError(err, '', true, 'Ok')
      );
  }

  onDeleteImage(file: ProfileUploadsInterface) {
    const fd = new FormData();
    fd.append('fileID', file.fileID);
    fd.append('profileID', file.profileID);

    swal({
      title: 'Are you sure you want to delete this image?',
      type: 'question',
      buttonsStyling: false,
      showConfirmButton: true,
      confirmButtonText: 'Yes   ',
      confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
      showCancelButton: true,
      cancelButtonText: '  Cancel ',
      cancelButtonClass: 'btn btn-outline-danger btn-lg',
    }).then(() => {
      this.API.deleteProfileImage(fd)
        .subscribe(data => {
          const upload = data.profileUploads;
          const index = this.profileUploads.findIndex(f => f.fileID === upload.fileID);
          this.profileUploads[index] = upload;
          this.alerts.swalSuccess(data.message, '', false, '', 2500);
        },
          (err) => this.alerts.swalError(err, '', true, 'Ok')
        );
    },
      (dismiss) => {
        if (dismiss === 'cancel') {

        }
      });

  }

  openURL(URL: string) {
    if (URL) { window.open(URL); }
  }
}
