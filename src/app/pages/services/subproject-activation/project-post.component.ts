import { Component, OnInit, TemplateRef, Input, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { GlobalConstants } from '../../../constants/global.constants';
import { AuthService } from '../../../services/auth/auth.service';
import { BackendService } from '../../../services/backend.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { MasksInterface } from '../../../interfaces/masks.interface';
import { PopoverModule, PopoverDirective } from 'ngx-bootstrap';
import { ProfileResolverService } from '../../../services/resolvers/profile-resolver/profile-resolver.service';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../../interfaces/profile-info.interface';
import swal from "sweetalert2";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SubprojectDataInterface } from '../../../interfaces/subproject.interface';
import { SubprojectResolverService } from '../../../services/resolvers/subproject-resolver/subproject-resolver.service';


@Component({
  selector: 'invoice-send',
  templateUrl: './project-post.component.html',
  styleUrls: ['./project-post.component.css']
})
export class ProjectPostComponent implements OnInit {

  newRequestForm: FormGroup;

  bsModalRef: BsModalRef;
  subcontractorProfile: any;
  profileStorageURL: string;
  projectData: any;
  scTradeID: number;
  public profileInfo: ProfileInfoInterface = null;
  subcontractorID: any;

  subproject: any;
  filteredSubproject: any;
  requestForm: any;

  hbg_id: any;
  project_id: any;
  id: any;
  is_available: any;
  comments: any;
  is_filled: any;
  start_subproject_date: any;
  bid_close_date: any;
  bsRangeValue: Date[];
  projectFiles = [];
  oldProjectFiles = [];
  config = {
    animated: true,
    class: 'modal-md'
  };

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private API: BackendService,
    private alerts: AlertService,
    private router: Router,
    private constants: GlobalConstants,
    private modalService: BsModalService,
    private profileResolver: ProfileResolverService,
    private subprojectService: SubprojectResolverService,
  ) {
    const snapshot = route.snapshot;
    this.profileInfo = snapshot.data.profile.profileInfo;
    this.subproject = snapshot.data.subprojectService.data;
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;
    this.projectData = snapshot.data.projectData;
    this.scTradeID = snapshot.params.scTradeID;
    this.hbg_id = snapshot.params.hbg;
    this.project_id = this.projectData.id;
    this.id = snapshot.params.subprojectID;
    this.is_available = 1;
    this.is_filled = 0;

    this.filteredSubproject = this.subproject.filter(
      (subproject) => {
        return snapshot.params.subprojectID == subproject.id;
      })
    this.oldProjectFiles = this.filteredSubproject[0].project_files

    let closeExpectedDate = new Date();
    closeExpectedDate.setDate(closeExpectedDate.getDate() + 7);
    this.start_subproject_date = this.filteredSubproject[0].start_subproject_date ? this.filteredSubproject[0].start_subproject_date : this.formatDate(new Date());
    this.bid_close_date = this.filteredSubproject[0].bid_close_date ? this.filteredSubproject[0].bid_close_date : this.formatDate(closeExpectedDate);
    this.bsRangeValue = [new Date(this.start_subproject_date), new Date(this.bid_close_date)];

    this.newRequestForm = new FormGroup({
      'comments': new FormControl(),
      'hbg_id': new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
      'project_id': new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
      'is_available': new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(1)])),
      'id': new FormControl(),
      'is_filled': new FormControl(),
      'start_subproject_date': new FormControl(),
      'bid_close_date': new FormControl()
    });
  }

  ngOnInit() {
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  onPeriodChange(value: Date[]): void {
    if (!value || value.length < 2) return;
    this.start_subproject_date = this.formatDate(value[0]);
    this.bid_close_date = this.formatDate(value[1]);
  }

  onSubmit(input: SubprojectDataInterface) {
    this.API.submitBidRequest(input).subscribe(data => {
      if (this.projectFiles.length > 0) {
        const uploadProjectForm = new FormData();
        this.projectFiles.forEach(projectFile => {
          uploadProjectForm.append('projectFiles[]', projectFile);
        })
        uploadProjectForm.append('projectID', this.project_id);
        uploadProjectForm.append('subprojectID', this.filteredSubproject[0].id);
        this.API.uploadProjectFiles(uploadProjectForm)
          .subscribe(data => {
            this.alerts.swalSuccess('Bid Request Saved Successfully', '', false, '', 2500, false, '', true);
            this.router.navigate(['projects', this.project_id, 'subcontractors', 'view', this.scTradeID, this.hbg_id]);
          })
      } else {
        this.alerts.swalSuccess('Bid Request Saved Successfully', '', false, '', 2500, false, '', true);
        this.router.navigate(['projects', this.project_id, 'subcontractors', 'view', this.scTradeID, this.hbg_id]);
      }
    }, (err) => {
      this.alerts.swalError("There was an error submitting this request!", "Please try again later", true, 'Ok', null, false);

    });

    // this.onRequestNewBid();
  }

  // onRequestNewBid(id: number, subproject_id: number) {
  //   this.bsModalRef = this.modalService.show(RequestBidComponent, { class: 'modal-dialog modal-md' });
  //   this.bsModalRef.content.hbg_id = id;
  //   this.bsModalRef.content.title = "Activate Project";
  //   this.bsModalRef.content.formType = "requestBid";
  //   this.bsModalRef.content.project_id = this.projectData.id;
  //   this.bsModalRef.content.is_available = 1;
  //   this.bsModalRef.content.id = subproject_id;
  //   this.bsModalRef.content.is_filled = 0;
  // }

  uploadFiles(modal: TemplateRef<any>) {
    this.bsModalRef = this.modalService.show(modal, this.config);
  }

  closeModal() {
    this.bsModalRef.hide();
  }

  addProjectFile($event) {
    let files = $event.target.files;
    const validFileExts = ["jpg", "jpeg", "png", "svg", "bmp", "pdf", "doc", "docx"];
    let validate = true;
    for (let i = 0; i < files.length; i++) {
      let file = files[i];
      const type = file.name.split('.').pop();
      if (!validFileExts.includes(type)) {
        this.alerts.swalError("You can only upload pdf/doc/img", '', true, 'Ok');
        validate = false;
        return false;
      }
    }
    if (!validate) return;

    this.projectFiles.push.apply(this.projectFiles, files);
  }

  removeProjectFile(index) {
    this.projectFiles.splice(index, 1);
  }

  viewFile(file) {
    window.open(this.API.getProjectFile(file.fileID));
  }
}
