import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../../services/backend.service';
import { SubcontractorPaginatorInterface } from '../../../interfaces/subcontractor.interface';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { ActivatedRoute } from '@angular/router';
import { GetHBGInterface } from '../../../interfaces/get-hbg.interface';
import { GlobalConstants } from '../../../constants/global.constants';
import { SubprojectDataInterface } from '../../../interfaces/subproject.interface';
import { SubprojectResolverService } from '../../../services/resolvers/subproject-resolver/subproject-resolver.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { GetBidFormInterface } from '../../../interfaces/get-bid-form-interface';
import { RequestBidComponent } from '../../bids/request-bid/request-bid.component';



@Component({
  selector: 'app-contractors',
  templateUrl: './contractors.component.html',
  styleUrls: ['./contractors.component.css']
})
export class ContractorsComponent implements OnInit {

  scs:                SubcontractorPaginatorInterface;
  hbg:                GetHBGInterface;
  projectData:        ProjectDataInterface;
  storageURL:         string;
  subprojects:        SubprojectDataInterface;
  bsModalRef:         BsModalRef;


  constructor
    (
    private API: BackendService,
    private projectService: ProjectResolverService,
    private subprojectService: SubprojectResolverService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private constants: GlobalConstants
    ) {
    this.hbg = this.route.snapshot.data.hbg;
    this.scs = this.route.snapshot.data.scs;
    this.subprojects = this.route.snapshot.data.subprojects;

    //console.log("subprojects\n" + this.subprojects.data);
    //console.log(this.route.snapshot.data);
    //debugger;
  }

  ngOnInit() {
    this.storageURL = `${this.constants.STORAGEURL}/`;
    this.projectData = this.projectService.getProjectData();
  }


  // update(event: any, step) {
  //   let subproject_id = step.id;
  //   if (event.target.checked) {
  //     this.onRequestNewBid(step.hbg_id, subproject_id);
  //   } else {
  //     var off = {
  //       is_available: 0,
  //       id: subproject_id,
  //       hbg_id: step.hbg_id,
  //       project_id: this.projectData.id,
  //       is_filled: 0,
  //     }
  //     this.API.submitBidRequest(off)
  //       .subscribe(data => {
  //       }, (err) => {
  //       });
  //   }
  // }

  // onRequestNewBid(id: number, subproject_id: number) {
  //   this.bsModalRef = this.modalService.show(RequestBidComponent, { class: 'modal-dialog modal-md' });
  //   this.bsModalRef.content.hbg_id = id;
  //   this.bsModalRef.content.title = "Request a Bid";
  //   this.bsModalRef.content.formType = "requestBid";
  //   this.bsModalRef.content.project_id = this.projectData.id;
  //   this.bsModalRef.content.is_available = 1;
  //   this.bsModalRef.content.id = subproject_id;
  //   this.bsModalRef.content.is_filled = 0;
  // }
}
