import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ActivatedRoute } from '@angular/router';
import { SubcontractorPaginatorInterface, SubcontractorInterface } from '../../../interfaces/subcontractor.interface';
import { BidFormInterface, FilesInterface, GetBidFormInterface } from '../../../interfaces/get-bid-form-interface';
import { BackendService } from '../../../services/backend.service';
import { forEach } from '@angular/router/src/utils/collection';
import { AlertService } from '../../../services/alerts/alert.service';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { ProfileResolverService } from '../../../services/resolvers/profile-resolver/profile-resolver.service';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { GlobalConstants } from '../../../constants/global.constants';

import { SubprojectDataInterface } from '../../../interfaces/subproject.interface';
import { SubprojectResolverService } from '../../../services/resolvers/subproject-resolver/subproject-resolver.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  bsModalRef: BsModalRef;
  scs: SubcontractorPaginatorInterface;
  files: FilesInterface;
  projectID: number;
  scTradeID: number;
  projectData: ProjectDataInterface;
  subprojects: Array<any>;
  tradeName: string;
  tradeDesc: string;
  storageURL: string;
  noSCSExist = false;
  subprojectActive: any;
  subp_id: any;
  filteredSubproject: any;
  hbg: any;
  allSelected = false;
  bidExists = false;

  public stepSlider: any = {
    connect: 'lower',
    step: 10,
    pips: { mode: 'steps', density: 10 },
    range: {
      min: 0,
      max: 100
    }
  };

  constructor
    (
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private API: BackendService,
    private alerts: AlertService,
    private subprojectService: SubprojectResolverService,
    private projectService: ProjectResolverService,
    private constants: GlobalConstants,

  ) {
    this.scs = this.route.snapshot.data.data;
    this.projectID = this.route.snapshot.params.id;
    this.scTradeID = this.route.snapshot.params.scTradeID;
    this.subprojects = this.route.snapshot.data.subprojectService.data;
    this.hbg = this.route.snapshot.params.hbg;

    this.filteredSubproject = this.subprojects.filter(
      (subproject) => {
        return subproject.hbg.sctID == this.scTradeID;
      }
    );

    this.subp_id = this.filteredSubproject[0].id;
    if (this.scs.data) {
      this.scs.data.forEach(element => {
        this.API.getBidsForSubprojectFromSubcontractor({
          subprojectID: this.subp_id,
          subcontractorID: element.scUserID
        }).subscribe(res => {
          if (res.bidOut) {
            element.bidExists = true;
          } else {
            element.bidExists = false;
          }
        })
      });
    }

    if (this.filteredSubproject[0].is_available == 1) {
      this.subprojectActive = 1;
    } else {
      this.subprojectActive = 0;
    }

    if (this.scs.tradeName != null) {
      this.noSCSExist = true;
      this.tradeName = this.scs.tradeName;
      this.tradeDesc = this.scs.description;

    }
    else if (this.scs.data[0].tradeName) {
      this.tradeDesc = this.scs.data[0].description;
      this.tradeName = this.scs.data[0].tradeName;

    }

    // console.log(this.scs);


  }

  ngOnInit() {
    this.projectData = this.projectService.getProjectData();
    this.storageURL = `${this.constants.STORAGEURL}/`;
  }

  // onCreateNewBid() {
  //   this.bsModalRef = this.modalService.show(NewBidComponent);
  //   this.bsModalRef.content.scTradeID     = this.scTradeID;
  //   this.bsModalRef.content.title         = "Create a new Bid Form";
  //   this.bsModalRef.content.formType      = "newBid";
  // }
  //
  // onViewBidForm() {
  //   this.bsModalRef = this.modalService.show(NewBidComponent);
  //   this.bsModalRef.content.scTradeID     = this.scTradeID;
  //   //this.bsModalRef.content.bidForm       = this.bidForm;
  //   this.bsModalRef.content.filesArray    = this.files;
  //   this.bsModalRef.content.title         = "Edit your bid form";
  //   this.bsModalRef.content.formType      = "editBid";
  // }
  //
  // onSelectAll() {
  //  this.allSelected = !this.allSelected;
  //   for (let i = 0 ; i < this.scs.data.length; i++) {
  //     if (this.scs.data[i].bboID == null) {
  //       this.scs.data[i].checked = this.allSelected;
  //     }
  //   }
  // }
  //
  // onSCSelect(selected, i) {
  //   this.scs.data[i].checked = selected;
  // }

  // send to all
  // onSubmitSelected() {
  //   const scIDArray = [];
  //   this.scs.data.forEach(item => {
  //     if (item.checked) {
  //       scIDArray.push({'scID': item.scID});
  //     }
  //   });
  //   if ( ! scIDArray.length) {
  //     this.alerts.swalError('You must select at least one Subcontractor to send a bid to', '', true, 'Ok');
  //     return;
  //   }
  //
  //   this.API.sendBidToSelectedSCS(scIDArray, this.projectID, this.scTradeID, this.bidID)
  //          .subscribe(data =>  {
  //            this.alerts.swalSuccess(data, '', false, '', 2500);
  //            this.getBBO();
  //          },
  //    (err) => this.alerts.swalError("Error", err, true, 'Ok')
  //     );
  // }
  //
  // onSendBidToSC(scID) {
  //   console.log(scID);
  //
  //   const scIDArray = [];
  //   scIDArray.push({'scID': scID});
  //
  //   if ( ! scIDArray.length) {
  //     this.alerts.swalError('Error', 'An Error has occurred please try again', true, 'Ok');
  //     return;
  //   }
  //
  //   this.API.sendBidToSelectedSCS(scIDArray, this.projectID, this.scTradeID, this.bidID)
  //     .subscribe(data => {
  //        this.alerts.swalSuccess(data, '', false, '', 2500);
  //        this.getBBO();
  //       },
  //       (err) => this.alerts.swalError("Error", 'An Error has occurred please try again', true, 'Ok')
  //     );
  // }
  //
  onGotoWebsite(URL) {
    if (URL) {
      window.open(URL);
    }
  }
}
