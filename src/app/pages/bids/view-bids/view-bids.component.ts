import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService, TabsetComponent } from 'ngx-bootstrap';
import { GetProjectsInterface, ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { BackendService } from '../../../services/backend.service';
import { SubcontractorInterface, SubcontractorPaginatorInterface } from '../../../interfaces/subcontractor.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { GetAllBidsByProjectResolverService } from '../../../services/resolvers/get-all-bids-by-project/get-all-bids-by-project-resolver.service';
import { ScTradesPaginatorInterface } from '../../../interfaces/sctrades-paginator.interface';
import { AlertService } from '../../../services/alerts/alert.service';
import {
  BidsInInterface, BidsOutInterface,
  GetBidsByTradeInterface
} from '../../../interfaces/get-bids-by-trade.interface';
//import { NewBidComponent } from '../new-bid/new-bid.component';
import { AuthService } from '../../../services/auth/auth.service';
import { GetScBidsPaginatorInterface } from '../../../interfaces/get-sc-bids-paginator.interface';
import { GlobalConstants } from '../../../constants/global.constants';
import { Observable } from 'rxjs/Observable';
import { GetAllBidsByProject } from '../../../interfaces/get-all-bids-by-project.interface';

import { GetBidsBySubproject } from '../../../interfaces/get-bids-by-subproject.interface';
import swal from "sweetalert2";

@Component({
  selector: 'app-view-bids',
  templateUrl: './view-bids.component.html',
  styleUrls: ['./view-bids.component.css']
})
export class ViewBidsComponent implements OnInit {

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  bsModalRef: BsModalRef;

  gabbp: GetAllBidsByProject[][];
  filteredList: any[];

  title: string;

  constructor
    (
    private router: Router,
    private route: ActivatedRoute,
    private API: BackendService,
    private alerts: AlertService,
    private modalService: BsModalService,
    private projectService: ProjectResolverService,
    private auth: AuthService,
    private constants: GlobalConstants,
    private getAllBids: GetAllBidsByProjectResolverService,

  ) {
    this.gabbp = Object.values(this.route.snapshot.data.getAllBids.bids);
    this.title = "All";
    debugger;
    this.filteredList = [];
    this.gabbp.forEach((sp) => {
      sp.forEach((bid) => {
        this.filteredList.push(bid);
      })
    });
    debugger;
  }

  ngOnInit() {
    //this.storageURL = `${this.constants.STORAGEURL}/`;
  }

  onChangeSCT(spIndex: any) {
    this.filteredList = [];
    this.title = "All";
    if (spIndex === 'All') {
      this.gabbp.forEach((sp) => {
        sp.forEach((bid) => {
          this.filteredList.push(bid);
        })
      })

    } else {
      this.filteredList = this.gabbp[spIndex];
      //console.log(this.filteredList);
      this.title = this.filteredList[0].subproject.hbg.stepName;
    }
  }

  onGotoWebsite(URL) {
    if (URL) {
      window.open(URL);
    }
  }

  accept(bid) {
    bid.status = 'accepted';

    this.API.updateBid(bid).subscribe(() => {
      swal("Success", "Accepted", 'success');
    });
  }

  renegotiate(bid) {
    bid.status = 'renegotiate';
    this.API.updateBid(bid).subscribe(() => {
      swal("Success", "Renegotiated", 'success');

    });
  }
}
