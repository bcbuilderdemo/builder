import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {BackendService} from '../../../services/backend.service';
import {SubprojectDataInterface} from '../../../interfaces/subproject.interface';
import {ModalComponent} from '../../../templates/modal/modal.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {AlertService} from '../../../services/alerts/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionStorageService} from 'ngx-webstorage';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import swal from 'sweetalert2';
import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
import {FilesInterface} from '../../../interfaces/files.interface';
import {AuthService} from '../../../services/auth/auth.service';


@Component({
  selector: 'requet-new-bid',
  templateUrl: './request-bid.component.html',
  styleUrls: ['./request-bid.component.css']
})

export class RequestBidComponent implements OnInit {
  // NewProjectComponent: any;

  @ViewChild('childModal') childModal: ModalComponent;
  @ViewChild(ModalDirective) public newBidModal: ModalDirective;

  newRequestForm: FormGroup;

  message:      string;
  project:      ProjectDataInterface;

  requestForm:    any;
  hbg_id:         any;
  formType:       any;
  title:          any;
  is_available:   any;
  project_id:     any;
  id:             any;
  is_filled:      any;

  files: Array<File> = [];
  filesArray = [];

  currentNumUploads     = 0;
  readonly MAX_UPLOADS  = 10;

  constructor
  (
    private API:                  BackendService,
    public bsModalRef:            BsModalRef,
    private modalService:         BsModalService,
    private alerts:               AlertService,
    private route:                ActivatedRoute,
    private session:              SessionStorageService,
    private projectService:       ProjectResolverService,
    private auth:                 AuthService,
    private router:               Router,

  ) {

    this.project = this.session.retrieve('project');

    console.log("f2", this.filesArray);
    this.newRequestForm = new FormGroup({
      'description':        new FormControl(),
      'hbg_id':             new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
      'project_id':         new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(250)])),
      'is_available':       new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(1)])),
      'id':                 new FormControl(),
      'is_filled':          new FormControl(),
    });


  }

      ngOnInit()
      {

      }


  onSubmit(input: SubprojectDataInterface) {
    //debugger;
    this.API.submitBidRequest(input).subscribe(data =>
    {
      this.bsModalRef.hide();
    }, (err) =>
    {
      this.alerts.swalError("An error has occurred", "Please try again", true, 'Ok', null, false);

    });
  }


}
