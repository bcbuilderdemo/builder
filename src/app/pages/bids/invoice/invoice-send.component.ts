import { Component, OnInit, TemplateRef, Input, ViewChild, QueryList, ViewChildren, ElementRef } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { GlobalConstants } from '../../../constants/global.constants';
import { AuthService } from '../../../services/auth/auth.service';
import { BackendService } from '../../../services/backend.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { MasksInterface } from '../../../interfaces/masks.interface';
import { PopoverModule, PopoverDirective } from 'ngx-bootstrap';
import { ProfileResolverService } from '../../../services/resolvers/profile-resolver/profile-resolver.service';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../../interfaces/profile-info.interface';
import swal from "sweetalert2";

@Component({
  selector: 'invoice-send',
  templateUrl: './invoice-send.component.html',
  styleUrls: ['./invoice-send.component.css']
})
export class InvoiceSendComponent implements OnInit {
  @ViewChild('projectUploadInput') projectUploadInput: ElementRef;

  bsModalRef: BsModalRef;
  bid: any;
  bidInfo: any;
  projectID: any;
  subprojectID: number;
  subcontractorProfile: any;
  profileStorageURL: string;
  allSubprojects: any;
  projectData: any;
  scTradeID: number;
  hbgID: number;
  public profileInfo: ProfileInfoInterface = null;
  subcontractorID: any;
  bidComments: string;
  projectFiles = [];

  config = {
    animated: true,
    class: 'modal-md'
  };

  masks: MasksInterface;
  regexPatterns: any;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private API: BackendService,
    private alerts: AlertService,
    private router: Router,
    private constants: GlobalConstants,
    private modalService: BsModalService,
    private profileResolver: ProfileResolverService
  ) {

    const snapshot = route.snapshot;
    this.projectID = snapshot.params.id;
    this.subprojectID = snapshot.params.subprojectID;
    this.scTradeID = snapshot.params.scTradeID;
    this.profileInfo = this.route.snapshot.data.profileResolver;
    this.subcontractorID = snapshot.params.subcontractorID;
    this.allSubprojects = this.route.snapshot.data.allSubprojects;
    this.hbgID = this.route.snapshot.params.hbg;
    this.bidComments = '';
    this.bidInfo = this.allSubprojects.data.filter((subproject) => {
      if (subproject.id == this.subprojectID) {
        this.bidComments = subproject.comments;
        return true;
      }
      return false;
    });

    this.bid = {
      id: null,
      comments: this.bidComments,
      amount: 0,
      scTradeID: this.scTradeID,
      subprojectID: this.subprojectID,
      is_bid_out: 1,
      subcontractorID: this.subcontractorID,
    };
    if (this.route.snapshot.data.bidInfo.bidOut) {
      this.bid = this.route.snapshot.data.bidInfo.bidOut
      this.projectFiles = this.bid.project_files;
    }
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;
    this.projectData = snapshot.data.projectData;
    this.masks = constants.masks;
    this.regexPatterns = this.constants.regexPatterns;
  }

  ngOnInit() {
  }

  submit() {
    let params = {
      bid: this.bid,
      project: this.projectData,
    }

    this.API.setBidsLineItems(params).subscribe((res) => {
      if (this.projectFiles.length > 0) {
        const uploadProjectForm = new FormData();
        this.projectFiles.forEach(projectFile => {
          uploadProjectForm.append('projectFiles[]', projectFile);
        })
        uploadProjectForm.append('projectID', this.projectID);
        uploadProjectForm.append('bidID', res.bid.id);
        this.API.uploadProjectFiles(uploadProjectForm)
          .subscribe(data => {
            swal("Success", "Invoice successfuly submitted", 'success');
            this.router.navigate(['/projects', this.projectID, 'subcontractors', 'view', this.scTradeID, this.hbgID]);
          })
      } else {
        swal("Success", "Invoice successfuly submitted", 'success');
        this.router.navigate(['/projects', this.projectID, 'subcontractors', 'view', this.scTradeID, this.hbgID]);
      }
    });
  }

  uploadFiles(modal: TemplateRef<any>) {
    this.bsModalRef = this.modalService.show(modal, this.config);
  }

  closeModal()
  {
    this.bsModalRef.hide();
  }

  addProjectFile($event) {
    let files = $event.target.files;
    const validFileExts = ["jpg", "jpeg", "png", "svg", "bmp", "pdf", "doc", "docx"];
    let validate = true;
    for (let i = 0; i < files.length; i++) {
      let file = files[i];
      const type = file.name.split('.').pop();
      if (!validFileExts.includes(type)) {
        this.alerts.swalError("You can only upload pdf/doc/img", '', true, 'Ok');
        validate = false;
        return false;
      }
    }
    if (!validate) return;

    this.projectFiles.push.apply(this.projectFiles, files);
  }

  removeProjectFile(index) {
    this.projectFiles.splice(index, 1);
  }

  viewFile(file) {
    window.open(this.API.getProjectFile(file.fileID));
  }
}
