import { Component, OnInit, TemplateRef, Input, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { GlobalConstants } from '../../../constants/global.constants';
import { AuthService } from '../../../services/auth/auth.service';
import { BackendService } from '../../../services/backend.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { MasksInterface } from '../../../interfaces/masks.interface';
import swal from "sweetalert2";


@Component({
    templateUrl: './view-invoice.component.html',
    styleUrls: ['./view-invoice.component.css'],

})

export class ViewInvoiceComponent implements OnInit {
    bidsLineItems: any;
    bid: any;
    projectID: any;
    subprojectID: any;
    builderProfile: any;
    subcontractorProfile: any;
    profileStorageURL: string;
    projectData: any;
    editIndex = null;

    constructor(
        private route: ActivatedRoute,
        private auth: AuthService,
        private API: BackendService,
        private alerts: AlertService,
        private router: Router,
        private constants: GlobalConstants,
        private modalService: BsModalService,
        private projectService: ProjectResolverService,
    ) {
        const snapshot = route.snapshot;
        this.projectID = snapshot.params.id;
        this.subprojectID = snapshot.params.subprojectID;
        this.bidsLineItems = snapshot.data.bidInfo.bidLineItems;
        this.bid = snapshot.data.bidInfo.bid;
        this.subcontractorProfile = this.bid.subcontractor;
        this.builderProfile = snapshot.data.profile.profileInfo;
        this.profileStorageURL = `${this.constants.STORAGEURL}/`;
    }

    ngOnInit() {
        this.projectData = this.projectService.getProjectData();
    }

    ngAfterViewInit() {
    }

    accept() {
        this.bid.status = 'accepted';

        this.API.updateBid(this.bid).subscribe(() => {
            swal("Success", "Accepted", 'success');
        });
    }

    renegotiate() {
        this.bid.status = 'renegotiate';

        this.API.updateBid(this.bid).subscribe(() => {
            swal("Success", "Renegotiated", 'success');
        });
    }

    get total() {
        let total = 0;
        this.bidsLineItems.forEach(bidsLineItem => {
            total += bidsLineItem.quantity * bidsLineItem.price;
        });
        this.bid.amount = total;
        return total;
    }
}
