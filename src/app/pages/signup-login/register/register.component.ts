import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {RegisterInterface} from '../../../interfaces/register-interface';
import {BackendService} from "../../../services/backend.service";
import {AlertService} from '../../../services/alerts/alert.service';
import {Router} from '@angular/router';
import {EqualValidator} from '../../../validators/password.validator';
import {SubcontractorInterface} from '../../../interfaces/subcontractor.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm:       FormGroup;
  submitAttempt   =   false;
  tradeRequired   =   false;
  masks: any;

  scs: SubcontractorInterface;

  constructor
  (
    private   API: BackendService,
    private   alertService: AlertService,
    private   router: Router,
  ) {

    this.masks = {
      phoneNumber: [/[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
      phoneNumber2: [/[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
      postalCode: [/[A-Za-z]/, /\d/, /[A-Za-z]/, ' ', /\d/, /[A-Za-z]/, /\d/]
    };

    this.registerForm = new FormGroup({
      'email':                         new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(255), Validators.email])),
      'password':                      new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(255), Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,20}$/)])),
      'password_confirmation':         new FormControl('', Validators.compose([Validators.required, EqualValidator("password")])),
      'userType':                      new FormControl('', Validators.compose([Validators.required])),
      'trade':                         new FormControl('NA'),
      'address':                       new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])),
      'address2':                      new FormControl(''),
      'city':                          new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])),
      'postalCode':                    new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250), Validators.pattern(/^[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]/)])),
      'province':                      new FormControl('BC', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])),
      'phoneNumber':                   new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250), Validators.pattern(/^[1-9]\d\d \d\d\d \d\d\d\d/)])),
      'company':                       new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])),
      'contactFirstName':              new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])),
      'contactLastName':               new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])),
      'phoneNumber2':                  new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(250), Validators.pattern(/^[1-9]\d\d \d\d\d \d\d\d\d/)])),
    });
  }

  ngOnInit() {
    console.log("on init register");
    this.API.getSubContractors().subscribe((data) => {
      // console.log("register trades", data);
      this.scs = data;
    });
  }

  onUserTypeChange(value) {
    console.log(value);
  }

  onSubmit(uploadForm: any) {
    this.submitAttempt = false;
    this.tradeRequired = false;

    if (this.registerForm.value.userType === 'subcontractor') {
      if ( ! this.registerForm.value.trade) {
        this.tradeRequired = true;
      }
    }

    if (this.registerForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    this.API.register(uploadForm.value).subscribe((data) =>
      {
        this.alertService.success(data, 'Registration Successful!\nPlease Wait for one of our representative to contacts', true);
        this.alertService.swalSuccess('', data, false, '', 3000, true, '/login');
      },
      (error) => {
          if    (error.status === 422)
          {
            this.alertService.swalError('', error, true, 'Ok', null,  false, 'login');
          }
          // else {  this.alertService.swalError('', error, true, 'Ok', null,  false, 'login'); }
          else {
            this.alertService.error('ERROR', error, false);
            window.scrollTo(0, 0);
          }
        });
  }

}
