import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { BackendService } from "../../../services/backend.service";
import { AlertService } from '../../../services/alerts/alert.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitAttempt = false;
  errMessage = null;

  constructor
    (
    private auth: AuthService,
    private alertService: AlertService,
    private router: Router
    ) {
    this.loginForm = new FormGroup({
      'userType': new FormControl(''),
      'email': new FormControl('', Validators.compose([Validators.email, Validators.required, Validators.minLength(2), Validators.maxLength(50)])),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(20), Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,20}$/)])),
    });
    this.loginForm.patchValue({
      'userType': "builder"
    });
  }

  ngOnInit() {
    this.auth.authenticationNotifier().subscribe(authd => {
      if (authd) { this.auth.authNotifier.next(false); }
    });
  }

  onSubmit(form: any) {
    if (this.loginForm.invalid) {
      this.submitAttempt = true;
      this.errMessage = "Your email or password is incorrect";
      return false;
    }
    this.auth.login(form.value)
      .subscribe(data => {
        this.auth.setToken(data);
        this.router.navigate(['/dashboard']);
      }, (err) => {
        this.errMessage = err;
      });
  }

}
