import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import { AlertService } from './services/alerts/alert.service';
import { DataService } from './templates/topbar/topbar.component';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  public app: any;
  bodyBorder = true;


  constructor(private auth: AuthService,
    private router: Router,
    private _alert: AlertService,
    private storage: LocalStorageService,
    private dataService: DataService
  ) {
    this.app = {
      layout: {
        isMenuCollapsed: false,
      }
    };
  }

  ngOnInit() {

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        const path = event.url;
        this.bodyBorder = (path === '/login' || path === '/register' || path === '/password/forgot');
      }
    });


    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    });

    this.storage.observe('token')
      .subscribe((newValue) => {
        if (!newValue) {
          this.router.navigate(['/login']);
        }
        console.log(newValue);
      })

    this.auth.authenticationNotifier().subscribe(authed => {
      if (!authed) {
        this.router.navigate(['/login']);
      } else {
      }
    });

    this.dataService.itemAdded$.subscribe(() => this.onGetSidebarInfo());
  }

  private onGetSidebarInfo(): void {
    // do something with added item
    this.app.layout.isMenuCollapsed = !this.app.layout.isMenuCollapsed;
  }
}
